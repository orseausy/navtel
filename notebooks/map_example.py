#!/home/$USER/anaconda3/bin python
# Import public modules
import cmocean
import os
from os import chdir, path
import matplotlib.font_manager as fm
import matplotlib.pyplot as plt
import numpy as np
import subprocess

# Import personal modules
chdir("/home/orseausy/opentelemac/v8p1r0/scripts/python3/")
from data_manip.extraction.telemac_file import TelemacFile
from postel.plot2d import *

# -- File we are going to use
path_archives = "/home/orseausy/Git/navtel/sources/TELBOT/model/archives"
file_name = path.join(path_archives, "t2d_navtel_19032020.slf")
bnd_file = path.join(path_archives[:-8], "BoundaryConditions.cli")

# -- Initalisaing Telemac file reader
res = TelemacFile(file_name, bnd_file=bnd_file)
nodes = res.npoin2

# -- Getting values
predicted_period = np.arange(4320,5184)
salinity = np.zeros((len(predicted_period), nodes), dtype=np.float64)
for count, value in enumerate(predicted_period):
    salinity[count] = res.get_data_value('SALINITY', value)

# -- Average over the period of prediction
mean_salinity = np.mean(salinity, axis=0)

# -- Figure
# Font
font_path = "/usr/share/fonts/truetype/msttcorefonts/Arial.ttf"
font = fm.FontProperties(fname=font_path, size=10)
#
fig, ax = plt.subplots(1, 1, figsize=(18 / 2.54, 15 / 2.54))
ax.set_aspect('equal')
# Plotting mesh
# plot2d_triangle_mesh(ax, res.tri, color='k', linewidth=0.1)
# Plotting scalar map
plot2d_scalar_map(fig, ax, res.tri, mean_salinity, data_name="salinity [psu]", vmin=0, vmax=35, nv=5, cmap_name=cmocean.cm.haline)
# Setting x, y limits
xlim = [340000, 430000]
ylim = [6420000, 6550000]
ax.set_xlim(xlim[0], xlim[1])
ax.set_ylim(ylim[0], ylim[1])
# Defining labels
ax.set_title("Mean salinity for the predicted period").set_fontproperties(font)
ax.set_xlabel('X-coordinate (m)').set_fontproperties(font)
ax.set_ylabel('Y-coordinate (m)').set_fontproperties(font)
for label in (ax.get_xticklabels() + ax.get_yticklabels()):
    label.set_fontproperties(font)
# Showing the plot
fig.tight_layout()
plt.savefig("/home/orseausy/Bureau/fig05.pdf", dpi=1200)
plt.close(fig)

