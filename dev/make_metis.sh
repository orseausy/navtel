#!/bin/bash

cd ../sources/TELBOT/opentelemac/v7p2r2/optionals/metis-5.1.0

make distclean
make config prefix=$(pwd)
make
make install

cd ../../../../../../dev/
