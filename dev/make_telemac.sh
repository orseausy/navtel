#!/bin/bash

cd ../sources/TELBOT/opentelemac/v7p2r2/

chmod u+x scripts/python27/*.py

# Configure the Opentelemac path
REGEX_BEFORE="(export HOMETEL=).*"
REGEX_AFTER="\1$(pwd)"
sed -i -r -e "s|(export HOMETEL=).*|\1$(pwd)|g" configs/pysource.sh
cat configs/pysource.sh

# On parse la configuration des chemins nécessaires à Telemac
source configs/pysource.sh

# Configuration vers la librairie Metis
REGEX_BEFORE="(libs_all:  .* ).*(libmetis\.a)"
REGEX_AFTER="\1$(pwd)/optionals/metis-5\.1\.0/build/Linux-x86_64/libmetis/\2"
sed -i -r -e "s|$REGEX_BEFORE|$REGEX_AFTER|g" configs/systel.cfg
cat configs/systel.cfg

# Configuration de Telemac
config.py
# Compilation
compileTELEMAC.py --clean

# Configuration du nombre de processeurs utilisés pour le calcul
cd ../../model

REGEX_BEFORE="(PARALLEL PROCESSORS=).*"
REGEX_AFTER="\1$(nproc --all)"
sed -i -r -e "s|$REGEX_BEFORE|$REGEX_AFTER|g" t2d_navtel.cas
cat t2d_navtel.cas

cd ../../../dev/
