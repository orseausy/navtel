#!/bin/bash
# Paths
export HOMENAVTEL=$(dirname $(realpath $0) | rev | cut -d'/' -f2- | rev)
export HOMETELBOT=$HOMENAVTEL/sources/TELBOT
export HOMET2D=$HOMETELBOT/opentelemac/v7p2r2
# Source virtual env for Python and Opentelemac
source $HOME/anaconda3/bin/activate
source activate py27
source $HOMET2D/configs/pysource.sh
# Run TELBOT
cd $HOMETELBOT
python $HOMETELBOT/telbot.py
sleep 60
# Run the simulation
python $HOMET2D/scripts/python27/runcode.py telemac2d -s $HOMETELBOT/model/t2d_navtel.cas
# Once the simulation is ended
# Move older .slf files
find $HOMETELBOT/model -type f -name '*.slf' ! -name geom.slf -exec mv {} $HOMETELBOT/model/archives/ \;
# Delete old files
find $HOMETELBOT/model -type f -name '*.CL' ! -name HarmonicConstants.CL -exec rm -f {} \; 
find $HOMETELBOT/model -type f -name '*.zip' -mtime -1 -exec rm -f {} \;
find $HOMETELBOT/model -type f -name '*.sortie' -mtime -1 -exec rm -f {} \;
# Delete files older than 2 days
find $HOMETELBOT/model/archives/ -type f -name '*.slf' -mtime +2 -exec rm -f {} \;
