# NavTEL
NavTEL is a decision support tool dedicated to assist port
authorities for ship routing and underkeel clearance management. The tool lies
on the TELEMAC-MASCARET modeling system (TMS) use for free-surface flows and was build with a
modular structure which make it easily adjusted depending port configuration.
The core of NavTEL is composed of two main modules called TELBOT and NAVIRE
The former prepares and launch simulations, while the latter provides possible
routes and computes the underkeel clearance with squat prediction at selected
locations of the channel.
TELBOT uses TELEMAC-2D and SISYPHE modules of TMS. TELEMAC-2D is dedicated to predict water levels,
velocity components and longitudinal salinity gradients. It also accounts bed
friction and the influence of meteorological conditions (atmospheric pressure
and wind). SISYPHE is dedicated to sediment transport (bed- and
suspended-load) and bed evolution. It also accounts processes specific to
cohesive sediments like self-weight consolidation. TELBOT is also composed by a set of
Python and bash scripts used to prepare and launch simulations.
In NAVIRE, route planning lies on the prediction of water levels obtained from
the astronomical tide and storm surge predictions computed by the TELBOT module.
The tidal window and routes were computed depending the longitudinal variation
of ship speed. Passing hours were already determined
empirically by the Port of Bordeaux, but for other port configuration, these
values can be established based on the Automatic Identification System (AIS)
tracking system and for different hydrological conditions.

## Getting started
A getting started tutorial is provided to learn how to use NavTEL (see file
examples/tutorial.md). An extensive manual containing more details on the installation of
NavTEL is also available (see folder documentation/manual.md).

## Installation and prerequisites
NavTEL is written in Python 2.7. For an easy installation, we recommended to use
Anaconda, which includes Python and all required packages. For more details on
its installation, please refer to the manual.

NavTEL lies on the TELEMAC-MASCARET modeling system used in the field of free
surface flows and available at www.opentelemac.org. The TELEMAC-MASCARET system is
written in Fortran (standard Fortran 2003). For its installation, please refer
to the online guides depending your system (Linux or Windows).

NavTEL also used following external Python scripts:
- pputils by Pat Prodanovic: to read and to extract simulation outputs (https://github.com/pprodano/pputils).
- detect_peaks.py by Marcos Duarte: to detect high and low waters (https://github.com/demotu).

Running NavTEL requires no or very little knowledge of Python. However, using it
for another port configuration implies to have basic knowledge in numerical
modeling, particularly for TELEMAC-MASCARET features.

## Authors
- Sylvain Orseau - Main developer, responsible for the NavTEL.v1 core
  developement and general module packages (GitLab profile).
- Guillaume Poissonnier - developer, responsible of the installation procedure (Gitlab profile)
- Nicolas Huybrechts - Contributor.
- Pablo Tassi - Contributor.

## Ackowledgments
The development of NavTEL was initiated by the Port of Bordaux through the
"Gironde XL 3D" project that is aimed at improve underkeel clearance management
and at better understand hydrodynamics and fine sediment stransport within the Gironde Estuary.

## Contributing
Contributions are welcome! Feel free to contact me through GitLab for possible
contribution topics or for additioal code-related information.

