# Tutorial

## What is NavTEL?
The NavTEL decision support tool determines near real-time ship route and underkeel clearance by predicting water levels, current velocities and water density along an estuarine navigation channel. Predictions are peformed with the TELEMAC-MASCARET system, a set of software for numerical modelling of free surface flows. Because of its modular structure, the tool can be easily adjusted to a different port configuration. The kernel of NavTEL is composed by 2 modules named NAVIRE and TELBOT. To ensure portability, NavTEL can be steered from the command line and does not include a graphical user interface. A description of each module of NavTEL is presented thereafter.

The TELBOT module is composed by a set of Python subroutines and bash scripts used to prepare and launch simulations. The first step consists in creating the TELEMAC-2D (hydrodynamics) and SISYPHE (sediment transport) steering files containing numerical and physical information such as the time-step, the friction coefficient and the eddy viscosity parameterization, as well as the input/output filenames. These filenames referred to the mesh (binary file), the boundary conditions (ASCII file), result files (binary file) and optional data files (ASCII format). The second step is to create optional data files containing river discharge and storm surge values.

The NAVIRE module is composed of Python scripts used to post-process numerical model results, to find the optimal route and to determine the underkeel clearance with the static or the dynamic draft. The communication between the port authority and ships is based on email exchanges. The first step consists to query received emails at the address specified by the user. In this way, the user can choose different requests (emails) for the specified simulation. Emails must contain criteria for navigation (date, hour, destinations and routes) and for the ship (type, length, width, static draft and speed). Once the required specifications are automatically extracted from the body of the email, NavTEL postprocesses the numerical model results. The tool extracts the hydro-sedimentary variables, e.g.~water depth, water surface elevation, flow velocity, salinity, and mass-concentration at a given interval (set by default equal to 5 minutes for the current version of NavTEL) of the simulation over the computational domain.

Depending on the shipping route, e.g. landward or seaward, NAVIRE will determine the passing time at each location and extract the hydro-sedimentary variables. With the chart datum and the static draft information, a first estimation of the underkeel clearance at each stations is provided. Optional variables can be used in the model to improve the determination of underkeel clearances, for example by considering the water density to predict the squat.

<div align="center">
  <img src="../documentation/navtel_flowchart_half.png" width="680">
</div>


## How to use NavTEL?

### Prerequisites
Before to run NavTEL, ensure to have all requiring files. These files are listed below and should be set for a unique estuarine navigation channel. For more information on the format of these files, see manual.md in the folder ``navtel/documentation`` and the application for the Gironde Estuary at ``navtel/examples/Gironde/GPMB``.

- CriticalLocations.csv: contains information on selected locations such as geographical coordinates or corresponding nodes.
- Distance.csv: provides distance separating selected locations.
- Terminals.csv: contains the name of terminals as well as corresponding nodes.
- Timings.csv: provides passing hours at selected locations. For the Gironde Estuary, it consists of a table containing time lags (in minutes) between the passing hour at a location and the hour of the high tide at the referent terminal. These values change depending the destination, the ship speed, the tidal range and the route type (landward or seaward).

### Run a numerical simulation
NavTEL lies on the TELEMAC-MASCARET system to predict hydrodynamics and sediment transport in the estuary. Numerical simulations are performed for a period of 18 days including a spin-up period of 15 days and a forecast period of 3 days from the date of the request. The spin-up period allows to distribute salinity and suspended-sediments depending hydrological conditions to reach a state with the correct salt intrusion length and the right location of the turbidity maximum zone.

Supplement data file can be provided to consider time-varying river discharge or storm surge. For the Gironde estuary, river discharges are downloaded on the national website https://www.vigicrues.gouv.fr/ in xml format at the Pessac and La Réole stations located at fluvial boundaries. The same procedure is applied to obtain storm surge values near the Cordouan station. The source of dataset can be changed by the user, but should respect the file format. 

Once all required files are created, simulations are performed and numerical results are stored in a binary filename SELAFIN (slf format). To launch a simulation, two ways can be followed: Automatically or manually.
#### Automatically with TELBOT
This method is recommended if your want to determine ship routes on a daily basis. It consists to set a crontab file which will launch daily the numerical model, as follows:
~~~~
0 6 * * * bash <path>/navtel/dev/model_launcher.sh
| | | | |
| | | | +-- day of the week (0-6)
| | | +------- month (1-12)
| | +------------ day of the month (1-31) 
| +----------------- hour (0-23)
+---------------------- minute (0-59)
~~~~

Model results are kept up to 3 days before deletion and are available to be used by the NAVIRE module to determine the ship route and underkeel clearances.
#### Manually
This method is recommended if you want to punctually determine ship route. It consists to launch the numerical simulation with the following command:
~~~~
python runcode.py telemac2d -s <path>/navtel/sources/TELBOT/model/t2d_navtel.cas
~~~~

### Run NavTEL
As explained above, the communication between the port authority and ships is based on email exchanges. User should set a specific . Once the required specifications are extracted from the email, NavTEL extracts the hydro-sedimentary variables from the numerical model result. Keep in mind that NavTEL reads the numerical result of the current date. If you want to run NavTEL for past simulations, change the result filename in the script ``navtel/sources/navtel.py``.

To run NavTEL, simply execute the following Python script:
~~~~
python <path>/navtel/sources/navtel.py
~~~~

NavTEL will ask you which request you want to process. Select one and wait a few minutes. Once the computation of the underkeel clearance is performed, NAVIRE generates a route report in pdf format including passing time, water level, water density, and underkeel clearance at selected locations. It also provides (i) all possible routes and corresponding maximum allowable draft with a 5-min interval in a html file named "Routes", and (ii) a graphical output showing the temporal variation of the allowable draft along the navigation channel and the safest traject (see figure below).

<div align="center">
  <img src="../documentation/gironde_example.png" width="680">
</div>

