#!/home/$USER/anaconda3/envs/py27/bin/python
# -*- coding: utf-8 -*-
# ***********************************************************
# Project: GIRONDE XL
# Authors: Sylvain Orseau
# history: Sylvain Orseau (Cerema)
#           15/01/2020, #V0P1
# ***********************************************************
"""
@author: Sylvain Orseau (Cerema) - sylvain.orseau@tutanota.com
Help:
--------------------------------------------------------------------------------
* NavTel V0P1
  help :
    ... -h
--------------------------------------------------------------------------------
"""
################################################################################
# Import public modules
import calendar
import datetime as dt
import matplotlib.font_manager as fm
from matplotlib.pyplot import axvline
from matplotlib import pyplot as plt
import numpy as np
import os
import pandas as pd
from scipy.interpolate import interp1d

# Import private modules
from NAVIRE.conversion import ConversionLevel
from NAVIRE.html_table import HTMLTable
from NAVIRE.port_configuration import Configuration
from NAVIRE.mail import Mail
from NAVIRE import report_routier
from NAVIRE.squat import Squat
from NAVIRE.telres_read import Selafin, TideAnalysis, Density
from NAVIRE.timings import Timings

# Path file
path_file = os.getcwd()
print path_file
path_archives = os.path.join(path_file + "/TELBOT/model/archives")
path_data = os.path.join(path_file[:-8] + "/examples/Gironde/GPMB/data")

# NavTEL version
version = "0.1"


# Private Functions
# -- High Tide Finding
def nearest(items, pivot):
    return min(enumerate(items), key=lambda value: abs(value[1] - pivot))


# -- Get the last sunday of March
current_year = dt.date.today().year
cal = calendar.Calendar(0)
march = cal.monthdatescalendar(current_year, 3)
october = cal.monthdatescalendar(current_year, 10)
last_sunday_march = max(week[-1] for week in march)
last_sunday_march = dt.datetime(last_sunday_march.year, last_sunday_march.month, last_sunday_march.day)
last_sunday_october = max(week[-1] for week in october)
last_sunday_october = dt.datetime(last_sunday_october.year, last_sunday_october.month, last_sunday_october.day)

################################################################################
# -- Used Files
filename_locations = "CriticalLocations.csv"
filename_distance = "Distance.csv"
filename_terminals = "Terminals.csv"
filename_safety_margin = "SafetyMargins.csv"
filename_timings = "Timings.csv"

# -- Information of the selected request
mail = Mail()
mail.check()
mail.extract(mail.body)
tide_of_interest = dt.datetime.strptime(mail.date + mail.hour, "%m/%d/%Y%H:%M")
print "Date:", mail.date
print "Time:", mail.hour
print "Route Type:", mail.route
print "Port:", mail.harbor
print "Draft:", mail.draft
print "Tide of Interest:", tide_of_interest

# -- Receipt confirmation of a request
mail.send_confirmation(version)

# -- Result files
now = dt.datetime.now()
filename_slf_hydro = "/home/orseausy/Git/navtel/sources/TELBOT/model/archives/t2d_navtel_19102020.slf"
filename_slf_sis = "/home/orseausy/Git/navtel/sources/TELBOT/model/archives/sis_navtel_19102020.slf"
# filename_slf_hydro = os.path.join(path_archives, "t2d_navtel_" + now.strftime("%d%m%Y") + ".slf")
# filename_slf_sis = os.path.join(path_archives, "sis_navtel_" + now.strftime("%d%m%Y") + ".slf")
slf_hydro = Selafin(filename_slf_hydro)
slf_sis = Selafin(filename_slf_sis)


# ***********************************************************
#                   PORT CONFIGURATION
# ***********************************************************
# -- Configure time and start time depending daylight saving time
os.chdir(path_data)
config = Configuration(mail.route)
config.start_time = slf_hydro.date[0]
if last_sunday_march < config.start_time < last_sunday_october:
    config.start_time = config.start_time + dt.timedelta(hours=2)
else:
    config.start_time = config.start_time + dt.timedelta(hours=1)

for row, date in enumerate(slf_hydro.date):
    if last_sunday_march < date < last_sunday_october:
        slf_hydro.date[row] = date + dt.timedelta(hours=2)
    else:
        slf_hydro.date[row] = date + dt.timedelta(hours=1)

# -- Loading of terminals configuration
terminals = config.Terminals(filename_terminals)
terminals.load(mail.harbor)

# -- Loading of critical locations configuration
critical_locations = config.CriticalLocations(filename_locations)
critical_locations.load(config.route_id)
critical_locations.filter(terminals.terminal_id)
critical_locations.Z_ign = critical_locations.convert_depth("ign")
critical_locations.write_file("seuils_locIGN69.csv", critical_locations.x, critical_locations.y,
                              critical_locations.Z_ign)
critical_locations.load_distance(filename_distance)

# -- Loading of safety margins configuration
safety_margins = config.SafetyMargin(filename_safety_margin)
safety_margins.load(config.route_id, terminals.terminal_id)
for count, val in enumerate(critical_locations.type_marker):
    safety_margins.values = np.insert(safety_margins.values, val, np.nan)


# ***********************************************************
#                   TIDE CHARACTERISTICS
# ***********************************************************
os.chdir(path_archives)
tide = TideAnalysis(filename_slf_hydro, terminals.terminal_referent_node)
tide.find_peaks()
tide.tidal_range_computation()
tide.high_water_time = [config.start_time + dt.timedelta(seconds=x) for x in tide.high_water_time_t2d]
tide.low_water_time = [config.start_time + dt.timedelta(seconds=x) for x in tide.low_water_time_t2d]


# ***********************************************************
#                   DATA EXTRACTION
# ***********************************************************
# -- Numerical outputs extraction at critical locations
slf_hydro.extract_at_multiple_nodes(critical_locations.nodes)
slf_sis.extract_at_multiple_nodes(critical_locations.nodes)


# ***********************************************************
#                   PASSING HOURS
# ***********************************************************
os.chdir(path_data)
# -- Passing hours loading
values_extended = np.zeros((tide.tide_nb - 28, len(critical_locations.type)), dtype=float)
critical_locations.passing_hours = list()
for i in range(28, tide.tide_nb):
    # Influence of the tidal range
    if tide.tidal_coefficient[i] >= 70:
        tide.tidal_range_id = 3
    else:
        tide.tidal_range_id = 1
    print tide.tidal_range_id
    # Passing hours loading
    timings = Timings(filename_timings)
    timings.load(config.route_id, terminals.terminal_id, tide.tidal_range_id, mail.speed)
    timings.duration = np.diff(timings.values)
    critical_locations.speed = critical_locations.distance_markers / abs(timings.duration)
    # Computation of passing hours if substations exist for each tide of the predicted period
    k = 0
    # If route type is seaward
    if config.route_id == 3:
        values_extended[i - 28, -1] = timings.values[-1]
        for count, val in enumerate(critical_locations.distance):
            if "Marker" in critical_locations.type[count]:
                values_extended[i - 28, count] = timings.values[k]
                k = k + 1
            else:
                values_extended[i - 28, count] = timings.values[k - 1] + val / critical_locations.speed[k - 1]
    # If route type is landward
    else:
        values_extended[i - 28, 0] = timings.values[0]
        for count, val in enumerate(critical_locations.distance):
            if "Marker" in critical_locations.type[count + 1]:
                values_extended[i - 28, count + 1] = timings.values[k + 1]
                k = k + 1
            else:
                values_extended[i - 28, count + 1] = timings.values[k] + val / critical_locations.speed[k]
    critical_locations.passing_hours.append([tide.high_water_time_t2d[i] + time * 60
                                             for time in values_extended[i - 28]])

# Creation of passing hours in TELEMAC-2D time (seconds) and real time (date and hours)
timings.values_extended = values_extended.astype(int)
critical_locations.passing_hours = critical_locations.passing_hours
del values_extended

# -- Compute advised passing hours for the requested tide
tide_of_interest_t2d = (tide_of_interest - config.start_time).days * 24 * 3600 \
                       + (tide_of_interest - config.start_time).seconds
nearest_tide_t2d = nearest(tide.high_water_time_t2d, tide_of_interest_t2d)
nearest_tide = config.start_time + dt.timedelta(seconds=nearest_tide_t2d[1])
critical_locations.passing_hours_advised_t2d = critical_locations.passing_hours[nearest_tide_t2d[0] - 28]
critical_locations.passing_hours_advised = [config.start_time + dt.timedelta(seconds=x)
                                            for x in critical_locations.passing_hours_advised_t2d]


# ***********************************************************
#    Variables Extraction
# ***********************************************************
variables = dict()

# -- TELEMAC-2D variables
for key in slf_hydro.variable_names:
    variables[key] = np.empty([len(critical_locations.nodes),
                               len(slf_hydro.time_prediction)])
    for i in range(len(critical_locations.nodes)):
        variables[key][i] = np.asarray(slf_hydro.variables
                                       ["node_" + str(i)]
                                       [key]
                                       [slf_hydro.index_prediction:len(slf_hydro.time)])

# -- SISYPHE variables
for key in slf_sis.variable_names:
    variables[key] = np.empty([len(critical_locations.nodes),
                               len(slf_hydro.time_prediction)])
    for i in range(len(critical_locations.nodes)):
        variables[key][i] = np.asarray(slf_sis.variables
                                       ["node_" + str(i)]
                                       [key]
                                       [slf_hydro.index_prediction:len(slf_hydro.time)])

# -- Compute velocity
variables['VELOCITY'] = np.sqrt(np.power(variables['VELOCITY_U'], 2) + np.power(variables['VELOCITY_V'], 2))

# -- Compute draft
variables['DRAFT'] = np.empty([len(critical_locations.nodes), len(slf_hydro.time_prediction)])
for i in range(len(critical_locations.nodes)):
    variables['DRAFT'] = np.abs(critical_locations.Z_ign[i]) + variables['FREE_SURFACE'][i] - safety_margins.values[i]

# -- Compute density
tmp = Density(variables['SALINITY'], variables['CONC_MAS_CL2'])
tmp.compute()
variables['DENSITY'] = tmp.extrapolate()


# ***********************************************************
#    Allowable drafts and UKCs computation
# ***********************************************************
varnames = ['FREE_SURFACE', 'VELOCITY', 'SALINITY', 'CONC_MAS_CL2', 'DENSITY']
for key in varnames:
    variables[key + '_GPMB'] = np.empty([critical_locations.number])
    for m, n in enumerate(critical_locations.passing_hours_advised_t2d):
        x = slf_hydro.time_prediction
        y = variables[key][m]
        f = interp1d(x, y)
        variables[key + '_GPMB'][m] = f(critical_locations.passing_hours_advised_t2d[m])
    variables[key + '_GPMB'][critical_locations.type_marker] = np.nan

# -- Compute allowable drafts
variables['ALLOWABLE_DRAFT_GPMB'] = np.abs(critical_locations.Z_ign) + variables[
    'FREE_SURFACE_GPMB'] - safety_margins.values
variables['MAXIMUM_ALLOWABLE_DRAFT_GPMB'] = np.nanmin(variables['ALLOWABLE_DRAFT_GPMB'])

# -- Compute underkeel clearances
variables['UNDERKEEL_CLEARANCE_GPMB'] = np.abs(critical_locations.Z_ign) + variables['FREE_SURFACE_GPMB'] - mail.draft

# Status
if variables['UNDERKEEL_CLEARANCE_GPMB'].size == 0:
    status = "FAILED"
else:
    status = "COMPLETED"


# ***********************************************************
#    SQUAT
# ***********************************************************
squat = Squat(mail.length,
              mail.breadth,
              mail.draft,
              mail.block_coefficient,
              mail.speed,
              variables['FREE_SURFACE_GPMB'] + np.abs(critical_locations.Z_ign),
              200,
              variables['VELOCITY_GPMB'])
squat.compute(3, density=False)


# ***********************************************************
#    TRAJECTS
# ***********************************************************
# -- Compute trajects
k = 0
variables['TRAJECTS'] = np.empty([len(slf_hydro.time_prediction[100:-100]),
                                  critical_locations.number]).astype(int)
for count, value in enumerate(slf_hydro.time_prediction[100:-100]):
    if value in tide.high_water_time_t2d:
        k = k + 1
    variables['TRAJECTS'][count] = value + np.asarray(timings.values_extended[k]) * 60

# Convert to datetime and str
variables['TRAJECTS_TIME'] = [[config.start_time + dt.timedelta(seconds=x) for x in step.tolist()]
                              for step in variables['TRAJECTS']]
variables['TRAJECTS_HTML'] = [[x.strftime("%m/%d %H:%M") for x in step]
                              for step in variables['TRAJECTS_TIME']]

# -- Data extraction
for key in varnames:
    variables[key + '_TRAJECTS'] = np.empty([critical_locations.number, len(variables['TRAJECTS'])])
    for m, n in enumerate(critical_locations.nodes):
        x = slf_hydro.time_prediction
        y = variables[key][m]
        f = interp1d(x, y)
        variables[key + '_TRAJECTS'][m] = np.asarray([f(value) for value in variables['TRAJECTS'][:, m]])

# Compute allowable drafts for each traject
variables['ALLOWABLE_DRAFT_TRAJECTS'] = np.empty([critical_locations.number, len(variables['TRAJECTS'])])
for m, n in enumerate(critical_locations.nodes):
    variables['ALLOWABLE_DRAFT_TRAJECTS'][m] = np.abs(critical_locations.Z_ign[m]) + variables['FREE_SURFACE_TRAJECTS'][
        m] - safety_margins.values[m]

# Find maximum allowable draft each time step
variables['MAXIMUM_ALLOWABLE_DRAFT'] = np.nanmin(variables['ALLOWABLE_DRAFT_TRAJECTS'], axis=0)
maximum_allowable_draft_location = np.nanargmin(variables['ALLOWABLE_DRAFT_TRAJECTS'], axis=0)


# ***********************************************************
#    HTML FILE
# ***********************************************************

# -- Dataframe creation
# Find indexes of allowable trajects
idx = np.where(variables['MAXIMUM_ALLOWABLE_DRAFT'] >= mail.draft + np.nanmax(squat.medium_values))
df = pd.DataFrame(np.array(variables['TRAJECTS_HTML'])[idx[0]], columns=critical_locations.stations)
df.insert(0, "UKC", variables['MAXIMUM_ALLOWABLE_DRAFT'][idx[0]].round(decimals=2))

# -- HTML creation
html = df.to_html(border=0)
html_table = HTMLTable(html)
trajects_table = html_table.generate()


# ***********************************************************
#    REPORTS
# ***********************************************************
h1 = [["Date:", nearest_tide.strftime("%d/%m/%y")],
      ["Parcours:", mail.harbor],
      ["Type:", mail.route],
      ["Vitesse calculée:", mail.speed]]
h2 = [["Tirant d'eau du navire:", mail.draft, " ",
       "Pleine mer Bordeaux:", nearest_tide.strftime("%Hh%M")],
      ["Tirant d'eau maximum admissible:", np.nanmin(variables['MAXIMUM_ALLOWABLE_DRAFT_GPMB']).round(decimals=2), " ",
       "Coefficient de la marée:", tide.tidal_coefficient[nearest_tide_t2d[0]]]]

# -----
# Ship route and underkeel clearance report
conversion = ConversionLevel()
d1 = {"Hs/Quille": variables['UNDERKEEL_CLEARANCE_GPMB'].round(decimals=1),
      "Hauteur": conversion.conversion_zh(critical_locations.x, critical_locations.y,
                                          variables['FREE_SURFACE_GPMB']).round(decimals=2),
      "Cote": np.abs(conversion.conversion_zh(critical_locations.x, critical_locations.y,
                                              critical_locations.Z_ign).round(decimals=1)),
      "Delta": [""] * len(critical_locations.passing_hours_advised),
      "Heure réelle": [""] * len(critical_locations.passing_hours_advised),
      "Heure prévue": [time.strftime("      %Hh%M") if "Threshold" in critical_locations.type[count] else
                       time.strftime("%Hh%M") for count, time in enumerate(critical_locations.passing_hours_advised)],
      "Jour": [time.strftime("%d/%m") for time in critical_locations.passing_hours_advised],
      "Site": ["        " + name if "Threshold" in critical_locations.type[count] else
               name for count, name in enumerate(critical_locations.stations)],
      "PK": critical_locations.pk}

# -----
cols = ["PK", "Site", "Jour", "Heure prévue", "Heure réelle", "Delta", "Cote", "Hauteur", "Hs/Quille"]
dfR1 = pd.DataFrame(data=d1, columns=cols)
dfR1 = dfR1.replace(np.nan, "", regex=True)
doc1 = report_routier.Document("route")
doc1.construct_pdf("route_report_gpmb.pdf", h1, h2, dfR1.values.tolist())


# Mail confirmation
# ##################################################################
status_external_forcings = "COMPLETED"
outputs = [status,
           status_external_forcings,
           filename_slf_hydro,
           np.nanmin(variables['ALLOWABLE_DRAFT_GPMB']).round(decimals=2)]
mail.send(outputs, "route_report_gpmb.pdf")
