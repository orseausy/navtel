#!/home/$USER/anaconda3/envs/py27/bin python
# -*- coding: utf-8 -*-
# ***********************************************************
# Project: GIRONDE XL
# Authors: Sylvain Orseau
# history: Sylvain Orseau (Cerema)
#           15/01/2020, #V0P1
# ***********************************************************
"""
Help:
--------------------------------------------------------------------------------
* NavTel V0P1
  help :
    ... -h
--------------------------------------------------------------------------------
"""
__version__ = "0.1"
__author__ = "Sylvain Orseau (sylvain.orseau@tutanota.com)"
__date__ = "05/01/2020"


################################################################################
# Import public modules
import calendar
import datetime as dt
import matplotlib.font_manager as fm
from matplotlib.pyplot import axvline
from matplotlib import pyplot as plt
import numpy as np
import os
import pandas as pd
from scipy.interpolate import interp1d

# Import private modules
from NAVIRE.conversion import ConversionLevel
from NAVIRE.html_table import HTMLTable
from NAVIRE.port_configuration import Configuration
from NAVIRE.mail import Mail
from NAVIRE import report
from NAVIRE.squat import Squat
from NAVIRE.telres_read import Selafin, TideAnalysis, Density
from NAVIRE.timings import Timings

# Path file
path_file = os.getcwd()
path_archives = os.path.join(path_file + "/TELBOT/model/archives")
path_data = os.path.join(path_file[:-8] + "/examples/Gironde/GPMB/data")

# NavTEL version
version = "0.1"


# Private Functions
def let_user_pick(options):
    print("Please choose a request:")
    for idx, element in enumerate(options):
        print("{}) {}".format(idx + 1, element))
    i = input("Enter number: ")
    try:
        if 0 < i <= len(options):
            return i
    except:
        pass
    return None


def nearest(items, pivot):
    return min(enumerate(items), key=lambda value: abs(value[1] - pivot))


# -- Get the last sunday of March
current_year = dt.date.today().year
cal = calendar.Calendar(0)
march = cal.monthdatescalendar(current_year, 3)
october = cal.monthdatescalendar(current_year, 10)
last_sunday_march = max(week[-1] for week in march)
last_sunday_march = dt.datetime(last_sunday_march.year, last_sunday_march.month, last_sunday_march.day)
last_sunday_october = max(week[-1] for week in october)
last_sunday_october = dt.datetime(last_sunday_october.year, last_sunday_october.month, last_sunday_october.day)

################################################################################

# ***********************************************************
#                  REQUEST SELECTION 
# ***********************************************************
# -- Used Files
filename_locations = "CriticalLocations.csv"
filename_distance = "Distance.csv"
filename_terminals = "Terminals.csv"
filename_safety_margin = "SafetyMargins.csv"
filename_timings = "Timings.csv"

# -- Information of the selected request
mail = Mail()
mail.parse()
request_id = let_user_pick(mail.request)
request_body = mail.body[request_id - 1]
mail.extract(request_body)
tide_of_interest = dt.datetime.strptime(mail.date + mail.hour, "%m/%d/%Y%H:%M")
print "Date:", mail.date
print "Time:", mail.hour
print "Route Type:", mail.route
print "Port:", mail.harbor
print "Draft:", mail.draft
print "Block coefficient: ", mail.block_coefficient
print "Tide of Interest:", tide_of_interest

# -- Receipt confirmation of a request
mail.send_confirmation(version)

# -- Result files
now = dt.datetime.now()
filename_slf_hydro = os.path.join(path_archives, "t2d_navtel_" + now.strftime("%d%m%Y") + ".slf")
filename_slf_sis = os.path.join(path_archives, "sis_navtel_" + now.strftime("%d%m%Y") + ".slf")
slf_hydro = Selafin(filename_slf_hydro)
slf_sis = Selafin(filename_slf_sis)


# ***********************************************************
#                   PORT CONFIGURATION
# ***********************************************************
# -- Configure time and start time depending daylight saving time
os.chdir(path_data)
config = Configuration(mail.route)
config.start_time = slf_hydro.date[0]
if last_sunday_march < config.start_time < last_sunday_october:
    config.start_time = config.start_time + dt.timedelta(hours=2)
else:
    config.start_time = config.start_time + dt.timedelta(hours=1)

for row, date in enumerate(slf_hydro.date):
    if last_sunday_march < date < last_sunday_october:
        slf_hydro.date[row] = date + dt.timedelta(hours=2)
    else:
        slf_hydro.date[row] = date + dt.timedelta(hours=1)

# -- Loading of terminals configuration
terminals = config.Terminals(filename_terminals)
terminals.load(mail.harbor)

# -- Loading of critical locations configuration
critical_locations = config.CriticalLocations(filename_locations)
critical_locations.load(config.route_id)
critical_locations.filter(terminals.terminal_id)
critical_locations.Z_ign = critical_locations.convert_depth("ign")
critical_locations.write_file("seuils_locIGN69.csv", critical_locations.x, critical_locations.y,
                              critical_locations.Z_ign)
critical_locations.load_distance(filename_distance)

# -- Loading of safety margins configuration
safety_margins = config.SafetyMargin(filename_safety_margin)
safety_margins.load(config.route_id, terminals.terminal_id)
for count, val in enumerate(critical_locations.type_marker):
    safety_margins.values = np.insert(safety_margins.values, val, np.nan)


# ***********************************************************
#                   TIDE CHARACTERISTICS
# ***********************************************************
os.chdir(path_archives)
tide = TideAnalysis(filename_slf_hydro, terminals.terminal_referent_node)
tide.find_peaks()
tide.tidal_range_computation()
tide.high_water_time = [config.start_time + dt.timedelta(seconds=x) for x in tide.high_water_time_t2d]
tide.low_water_time = [config.start_time + dt.timedelta(seconds=x) for x in tide.low_water_time_t2d]


# ***********************************************************
#                   DATA EXTRACTION
# ***********************************************************
slf_hydro.extract_at_multiple_nodes(critical_locations.nodes)
slf_sis.extract_at_multiple_nodes(critical_locations.nodes)


# ***********************************************************
#                   PASSING HOURS
# ***********************************************************
os.chdir(path_data)
# -- Passing hours loading
values_extended = np.zeros((tide.tide_nb - 28, len(critical_locations.type)), dtype=float)
critical_locations.passing_hours = list()
for i in range(28, tide.tide_nb):
    # Influence of the tidal range
    if tide.tidal_coefficient[i] >= 70:
        tide.tidal_range_id = 3
    else:
        tide.tidal_range_id = 1
    # Passing hours loading
    timings = Timings(filename_timings)
    timings.load(config.route_id, terminals.terminal_id, tide.tidal_range_id, mail.speed)
    timings.duration = np.diff(timings.values)
    critical_locations.speed = critical_locations.distance_markers / abs(timings.duration)
    # Computation of passing hours if substations exist for each tide of the predicted period
    k = 0
    # If route type is seaward
    if config.route_id == 3:
        values_extended[i - 28, -1] = timings.values[-1]
        for count, val in enumerate(critical_locations.distance):
            if "Marker" in critical_locations.type[count]:
                values_extended[i - 28, count] = timings.values[k]
                k = k + 1
            else:
                values_extended[i - 28, count] = timings.values[k - 1] + val / critical_locations.speed[k - 1]
    # If route type is landward
    else:
        values_extended[i - 28, 0] = timings.values[0]
        for count, val in enumerate(critical_locations.distance):
            if "Marker" in critical_locations.type[count + 1]:
                values_extended[i - 28, count + 1] = timings.values[k + 1]
                k = k + 1
            else:
                values_extended[i - 28, count + 1] = timings.values[k] + val / critical_locations.speed[k]
    critical_locations.passing_hours.append([tide.high_water_time_t2d[i] + time * 60
                                             for time in values_extended[i - 28]])
# Creation of passing hours in TELEMAC-2D time (seconds) and real time (date and hours)
timings.values_extended = values_extended.astype(int)
critical_locations.passing_hours = critical_locations.passing_hours
del values_extended

# -- Compute advised passing hours for the requested tide
tide_of_interest_t2d = (tide_of_interest - config.start_time).days * 24 * 3600 \
                       + (tide_of_interest - config.start_time).seconds
nearest_tide_t2d = nearest(tide.high_water_time_t2d, tide_of_interest_t2d)
nearest_tide = config.start_time + dt.timedelta(seconds=nearest_tide_t2d[1])
critical_locations.passing_hours_advised_t2d = critical_locations.passing_hours[nearest_tide_t2d[0] - 28]
critical_locations.passing_hours_advised = [config.start_time + dt.timedelta(seconds=x)
                                            for x in critical_locations.passing_hours_advised_t2d]


# ***********************************************************
#    Variables Extraction
# ***********************************************************
variables = dict()

# -- TELEMAC-2D variables
for key in slf_hydro.variable_names:
    variables[key] = np.empty([len(critical_locations.nodes),
                               len(slf_hydro.time_prediction)])
    for i in range(len(critical_locations.nodes)):
        variables[key][i] = np.asarray(slf_hydro.variables
                                       ["node_" + str(i)]
                                       [key]
                                       [slf_hydro.index_prediction:len(slf_hydro.time)])

# -- SISYPHE variables
for key in slf_sis.variable_names:
    variables[key] = np.empty([len(critical_locations.nodes),
                               len(slf_hydro.time_prediction)])
    for i in range(len(critical_locations.nodes)):
        variables[key][i] = np.asarray(slf_sis.variables
                                       ["node_" + str(i)]
                                       [key]
                                       [slf_hydro.index_prediction:len(slf_hydro.time)])

# -- Compute velocity
variables['VELOCITY'] = np.sqrt(np.power(variables['VELOCITY_U'], 2) + np.power(variables['VELOCITY_V'], 2))

# -- Compute draft
variables['DRAFT'] = np.empty([len(critical_locations.nodes), len(slf_hydro.time_prediction)])
for i in range(len(critical_locations.nodes)):
    variables['DRAFT'] = np.abs(critical_locations.Z_ign[i]) + variables['FREE_SURFACE'][i] - safety_margins.values[i]

# -- Compute density
tmp = Density(variables['SALINITY'], variables['CONC_MAS_CL2'])
tmp.compute()
variables['DENSITY'] = tmp.extrapolate()


# ***********************************************************
#    SQUAT
# ***********************************************************
squat = Squat(mail.length,
              mail.breadth,
              mail.draft,
              mail.block_coefficient,
              mail.speed,
              variables["FREE_SURFACE"] + np.abs(critical_locations.Z_ign),
              200,
              variables["VELOCITY"])
squat.compute(3, density=False)


# ***********************************************************
#    TRAJECTS
# ***********************************************************
# -- Compute trajects
k = 0
variables['TRAJECTS'] = np.empty([len(slf_hydro.time_prediction[100:-100]), critical_locations.number]).astype(int)
for count, value in enumerate(slf_hydro.time_prediction[100:-100]):
    if value in tide.high_water_time_t2d:
        k = k + 1
    variables['TRAJECTS'][count] = value + np.asarray(timings.values_extended[k]) * 60
    
# Convert to datetime and str
variables['TRAJECTS_TIME'] = [[config.start_time + dt.timedelta(seconds=x) for x in step.tolist()]
                              for step in variables['TRAJECTS']]
variables['TRAJECTS_HTML'] = [[x.strftime("%m/%d %H:%M") for x in step]
                              for step in variables['TRAJECTS_TIME']]

# -- Data extraction
varnames = ['FREE_SURFACE', 'VELOCITY', 'SALINITY', 'CONC_MAS_CL2', 'DENSITY']
for key in varnames:
    variables[key + '_TRAJECTS'] = np.empty([critical_locations.number, len(variables['TRAJECTS'])])
    for m, n in enumerate(critical_locations.nodes):
        x = slf_hydro.time_prediction
        y = variables[key][m]
        f = interp1d(x, y)
        variables[key + '_TRAJECTS'][m] = np.asarray([f(value) for value in variables['TRAJECTS'][:, m]])

# Compute allowable drafts for each traject        
variables['ALLOWABLE_DRAFT_TRAJECTS'] = np.empty([critical_locations.number, len(variables['TRAJECTS'])])
for m, n in enumerate(critical_locations.nodes):
    variables['ALLOWABLE_DRAFT_TRAJECTS'][m] = np.abs(critical_locations.Z_ign[m]) + variables['FREE_SURFACE_TRAJECTS'][m] - safety_margins.values[m]

# Find maximum allowable draft each time step
variables['MAXIMUM_ALLOWABLE_DRAFT'] = np.nanmin(variables['ALLOWABLE_DRAFT_TRAJECTS'], axis=0)
maximum_allowable_draft_location = np.nanargmin(variables['ALLOWABLE_DRAFT_TRAJECTS'], axis=0)


# ***********************************************************
#    SAFEST TRAJECT
# ***********************************************************
# -- Find indexes of allowable trajects
idx_02 = np.where(variables['MAXIMUM_ALLOWABLE_DRAFT'] >= mail.draft + np.nanmax(squat.medium_values))

# -- Interpolation
time_interp = np.arange(variables['TRAJECTS'][0, 0], variables['TRAJECTS'][-1, -1], 1).astype(int)
variables['ALLOWABLE_DRAFT_INTERP'] = np.empty([len(critical_locations.type_threshold), len(time_interp)])
for count, value in enumerate(critical_locations.type_threshold):
    x = variables['TRAJECTS'][:, value]
    y = variables['ALLOWABLE_DRAFT_TRAJECTS'][value, :]
    f = interp1d(x, y, bounds_error=False)
    variables['ALLOWABLE_DRAFT_INTERP'][count, :] = f(time_interp)

# -- Find the traject nearest to the tide of interest
idx_poi = nearest(np.array(variables['TRAJECTS_TIME'])[idx_02[0]][:, -1], tide_of_interest)
#
idx_03 = np.where(np.diff(idx_02)[0] > 1)
idx_03 = idx_03[0] + 1
idx_03 = np.insert(idx_03, 0, 0)
idx_03 = np.insert(idx_03, len(idx_03), len(idx_02[0]))
#
nearest_tide_safest = idx_poi[1]
# varnames = ['FREE_SURFACE', 'VELOCITY', 'SALINITY', 'CONC_MAS_CL2', 'DENSITY']
# for key in varnames:
for count, value in enumerate(idx_03[1:]):
    if idx_03[count] <= idx_poi[0] < value:
        index = np.argmax(variables['MAXIMUM_ALLOWABLE_DRAFT'][idx_02[0][idx_03[count]:value]])
        #
        variables['ALLOWABLE_DRAFT_SAFEST'] = variables['ALLOWABLE_DRAFT_TRAJECTS'][:, idx_02[0][idx_03[count] + index]]
        variables['MAXIMUM_ALLOWABLE_DRAFT_SAFEST'] = variables['MAXIMUM_ALLOWABLE_DRAFT'][idx_02[0][idx_03[count] + index]]
        maximum_allowable_draft_location_safest = maximum_allowable_draft_location[idx_02[0][idx_03[count] + index]]
        variables['TRAJECTS_SAFEST'] = variables['TRAJECTS'][idx_02[0][idx_03[count] + index]]
        variables['TRAJECTS_TIME_SAFEST'] = variables['TRAJECTS_TIME'][idx_02[0][idx_03[count] + index]]
        # Variables
        for key in varnames:
            variables[key + '_SAFEST'] = variables[key + '_TRAJECTS'][:, idx_02[0][idx_03[count] + index]]
        variables[key + '_SAFEST'][critical_locations.type_marker] = np.nan
    else:
        pass

# Compute UKC
variables['UKC_SAFEST'] = np.abs(critical_locations.Z_ign) + variables['FREE_SURFACE_SAFEST'] - (mail.draft + squat.medium_values)

# -- Status
if variables['UKC_SAFEST'].size == 0:
    status_navtel = "FAILED"
else:
    status_navtel = "COMPLETED"


# ***********************************************************
#    HTML FILE
# ***********************************************************

# Dataframe creation
df = pd.DataFrame(np.array(variables['TRAJECTS_HTML'])[idx_02[0]], columns=critical_locations.stations)
df.insert(0, "Maximum Allowable Draft", variables['MAXIMUM_ALLOWABLE_DRAFT'][idx_02[0]].round(decimals=2))

# HTML creation
html = df.to_html(border=0)
html_table = HTMLTable(html)
trajects_table = html_table.generate()


# ***********************************************************r
#    FIGURES
# ***********************************************************
figure = Figures(config, critical_locations, mail, variables)

# Figure 06
# figure.fig06(time_interp,
#             np.arange(0, len(critical_locations.type_threshold)),
#             allowable_draft_interp)

# Figure 07
# figure.fig07()


# ***********************************************************
#    REPORTS
# ***********************************************************
header = [["Date", nearest_tide.strftime("%d/%m/%y"), "Static draft:", mail.draft],
          ["Destination:", mail.harbor, "High tide at referent harbor:", nearest_tide_safest.strftime("%Hh%M")],
          ["Route:", mail.route, "Maximum allowable draft:", variables['MAXIMUM_ALLOWABLE_DRAFT_SAFEST'].round(decimals=2)],
          ["Speed:", mail.speed, "Tidal coefficient:", tide.tidal_coefficient[nearest_tide_t2d[0]]]]

# -- Ship route and underkeel clearance report
conversion = ConversionLevel()
data_1 = {"Squat": squat.medium_values.round(decimals=1),
          "Density": variables['DENSITY_SAFEST'].round(decimals=1),
          "UKC": variables['UNDERKEEL_CLEARANCE_SAFEST'].round(decimals=1),
          "Water Level": conversion.conversion_zh(critical_locations.x,
                                                  critical_locations.y,
                                                  variables['FREE_SURFACE_SAFEST']).round(decimals=2),
          "Z": np.abs(conversion.conversion_zh(critical_locations.x,
                                               critical_locations.y,
                                               critical_locations.Z_ign).round(decimals=1)),
          "Hour": [time.strftime("      %Hh%M") if "Threshold" in critical_locations.type[count]
                   else time.strftime("%Hh%M") for count, time in enumerate(variables['TRAJECTS_TIME_SAFEST'])],
          "Date": [time.strftime("%d/%m") for time in variables['TRAJECTS_TIME_SAFEST']],
          "Critical Locations": ["        " + name if "Threshold" in critical_locations.type[count]
                                 else name for count, name in enumerate(critical_locations.stations)],
          "KP": critical_locations.pk}
# --
cols = ["KP", "Critical Locations", "Date", "Hour", "Z", "Water Level", "UKC", "Density", "Squat"]
# --
dfR1 = pd.DataFrame(data=data_1, columns=cols)
dfR1 = dfR1.replace(np.nan, "", regex=True)
# --
doc1 = report.Document("route")
doc1.construct_pdf("route_report2.pdf", header, dfR1.values.tolist())


# Mail confirmation
# ##################################################################
status_external_forcings = "COMPLETED"
outputs = [status_navtel, status_external_forcings, filename_slf_hydro, np.nanmin(draft_advised).round(decimals=2)]
# mail.send(outputs, "route_report.pdf", "optimized_route_report.pdf", "navigation_report.pdf")
print " "
print " "
print "===========================> My Work is Done! <==========================="
print "Ship route and corresponding underkeel clearances were sent by e-mail!"
print " "
print "========================================================================="
print "                         END OF THE PROCESSING                           "
print "========================================================================="
