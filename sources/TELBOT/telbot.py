#!/home/$USER/anaconda3/envs/py27/bin python
# -*- coding: utf-8 -*-
"""
@author: Sylvain Orseau (sylvain.orseau@tutanota.com)

"""
################################################################################
# Import modules
import csv
from shom import DataShom
import datetime as dt
import numpy as np
import pandas as pd
import os
from hubeau import HubEau


# Private functions
def nearest(items, pivot):
    return min(enumerate(items), key=lambda x: abs(x[1] - pivot))


# Paths
path_home = os.path.expanduser('~')
path_telbot = os.getcwd()


################################################################################
class SurgeFile:
    def __init__(self, time, url):
        self.start_simulation = time
        self.url = url
        # ---
        data = DataShom(self.url)
        data.extract()
        data.load_surge()
        self.status = data.status
        i_1, self.start_time = nearest(data.surge_time, dt.datetime.now())
        self.start_time_t2d = int((self.start_time - self.start_simulation).total_seconds())
        self.values = np.array(data.surge_values[i_1:]).round(decimals=4)
        self.duration = np.diff(data.surge_time[i_1:]) / np.timedelta64(1, 's')
        self.time_t2d = np.r_[self.start_time_t2d, self.duration].cumsum()
        self.data = None
        self.filename = str()
        self.type = None

    def write(self, filename):
        self.filename = filename
        raw = [(time, self.values[i]) for i, time in enumerate(self.time_t2d)]
        self.type = [("Time", int), ("Storm Surge", float)]
        self.data = np.array(raw, dtype=self.type)
        csv_file = open(self.filename, "w")
        with csv_file:
            writer = csv.writer(csv_file, delimiter=" ")
            writer.writerows(self.data)


class RiverDischargeFile:
    def __init__(self, time, code_station):
        self.start_simulation = time
        self.code_station = code_station
        data = HubEau(self.code_station)
        res = data.load()
        data.parse(res)
        self.status = data.status
        self.end_simulation = self.start_simulation + dt.timedelta(days=18)
        i_1, self.start_time = nearest(data.time_daily, self.start_simulation)
        i_2, self.end_time = nearest(data.time_daily, self.end_simulation)
        i_3, self.prescribed_time = nearest(data.time_daily, self.end_time - dt.timedelta(days=3))
        time_prevision = pd.date_range(self.end_time, self.end_simulation)[1:]
        time_simulation = pd.Series.append(data.time_daily[i_1:], time_prevision.to_series(), ignore_index=True)
        self.step = np.diff(time_simulation).astype('timedelta64[s]')
        self.time_t2d = np.r_[0, self.step].cumsum()
        self.prescribed_value = np.nanmean(data.river_discharge_daily[i_3:i_2+1]).astype(int)
        self.values_t2d = pd.Series.append(data.river_discharge_daily[i_1:],
                                           pd.Series(self.prescribed_value).repeat(len(time_prevision)))


def write_discharge_file(filename, time, q_garonne, q_dordogne):
    filename = filename
    time_t2d = time.astype("timedelta64[s]").astype(int)
    dordogne = q_dordogne
    garonne = q_garonne
    sea_level = np.zeros(len(dordogne), dtype=int)
    velocity_u = np.zeros(len(dordogne), dtype=int)
    velocity_v = np.zeros(len(dordogne), dtype=int)
    header = [["#"],
              ["#"],
              ["T", "SL(1)", "U(1)", "V(1)", "Q(2)", "Q(3)"],
              ["s", "m", "m/s", "m/s", "m3/s", "m3/s"]]
    # ---
    csv_file = open(filename, "w+")
    with csv_file:
        writer = csv.writer(csv_file, delimiter=" ")
        writer.writerows(header)
        for (T, SL, U, V, garonne, dordogne) in zip(time_t2d, sea_level, velocity_u, velocity_v, garonne, dordogne):
            writer.writerow([T, SL, U, V, garonne, dordogne])


################################################################################
os.chdir("model")
# Writing of steering files
os.system(path_home + "/anaconda3/bin/python " + path_telbot + "/steerings.py")
# Find start time of the simulation
today = dt.datetime.today()
start_simulation = dt.datetime(today.year, today.month, today.day, 8) - dt.timedelta(days=15)
# URLs
url_cordouan = "https://services.data.shom.fr/oceano/render/text?duration=4&delta-date=0&spot=CORDOUAN&utc=1&lang=en"
code_dordogne = "P555001001" # Station Pessac-sur-Dordogne
code_garonne = "O919001001"  # Station La Reole
dordogne = RiverDischargeFile(start_simulation, code_dordogne)
# garonne = RiverDischargeFile(start_simulation, url_garonne)
garonne = [int(value / .35 - value) for value in dordogne.values_t2d]
if "FAILED" in (dordogne.status):
    pass
else:
    write_discharge_file("q_navtel.txt", dordogne.time_t2d, garonne, dordogne.values_t2d)

# -- Storm Surges
surge_file = SurgeFile(start_simulation, url_cordouan)
surge_file.write("surge.txt")

# -- Status
if "FAILED" in (dordogne.status, surge_file.status):
    status_external_forcings = "FAILED"
else:
    status_external_forcings = "COMPLETED"
