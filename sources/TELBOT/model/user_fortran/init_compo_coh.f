
!                    *************************
                     SUBROUTINE INIT_COMPO_COH
!                    *************************
!
     &(ES,CONC_VASE,CONC,NPOIN,NOMBLAY,NSICLA,AVAIL,AVA0)
!
!***********************************************************************
! SISYPHE   V6P2                                   21/07/2011
!***********************************************************************
!
!brief    INITIAL FRACTION DISTRIBUTION, STRATIFICATION,
!+                VARIATION IN SPACE.
!
!warning  USER SUBROUTINE; MUST BE CODED BY THE USER
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE BIEF
      USE INTERFACE_SISYPHE, EX_INIT_COMPO_COH=> INIT_COMPO_COH
      USE DECLARATIONS_SISYPHE, ONLY : NLAYMAX,SIS_FILES,SISGEO,MESH
	  USE DECLARATIONS_TELEMAC2D, ONLY : X,Y,zf,T2D_FILES,T2DFO1
!
      USE DECLARATIONS_SPECIAL
      IMPLICIT NONE
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER, INTENT(IN)              :: NPOIN,NOMBLAY,NSICLA
      DOUBLE PRECISION, INTENT(INOUT)  :: ES(NPOIN,NOMBLAY)
      DOUBLE PRECISION, INTENT(IN)     :: CONC_VASE(NOMBLAY)
      DOUBLE PRECISION,  INTENT(INOUT) :: CONC(NPOIN,NOMBLAY)
      DOUBLE PRECISION, INTENT(INOUT)  :: AVAIL(NPOIN,NOMBLAY,NSICLA)
      DOUBLE PRECISION, INTENT(IN)     :: AVA0(NSICLA)
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      DOUBLE PRECISION  EPAI_VASE(NLAYMAX),EPAI_SABLE(NLAYMAX)
      DOUBLE PRECISION TM1(NPOIN)
!	     TYPE(BIEF_OBJ) :: SEDMIX
        CHARACTER*16 NAME
!		 CHARACTER*16 NAME2
      REAL, ALLOCATABLE :: W(:)
      INTEGER I,J,ERR
c      INTEGER I,J
            LOGICAL OK
	  
!	     CALL BIEF_ALLVEC(1,SEDMIX,'SEDMIX  ',2,1,2,MESH)
!        ALLOCATE(W(NPOIN),STAT=ERR)
       NAME='TM               ' 
!	   NAME2='EPAI           ' 
!       CALL FIND_IN_SEL(SEDMIX,NAME,SIS_FILES(SISGEO)%LU,'SERAFIN ',W
!     *	   ,OK)
!       DEALLOCATE(W)
           CALL READ_BIN_2D
     &     (TM1,NAME,0.d0,SIS_FILES(SISGEO)%LU,
     &	        'SERAFIN ',NPOIN,OK,OK)
!	       CALL READ_BIN_2D
!     &     (EPAI1,NAME2,0.d0,SIS_FILES(SISGEO)%LU,
!     &	        'SERAFIN ',NPOIN,OK,OK)
!-----------------------------------------------------------------------

!			EPAI_VASE(1)=0.25D0
!			EPAI_VASE(2)=0.25D0
!			EPAI_VASE(3)=0.5D0
!			EPAI_VASE(4)=1.D0
!			EPAI_VASE(5)=1.D0
!			EPAI_VASE(6)=0.5D0
!			EPAI_VASE(7)=0.5d0
!			EPAI_VASE(8)=0.5
!			EPAI_VASE(9)=0.25
!			EPAI_VASE(10)=0.25
			EPAI_VASE(1)=0.05D0
			EPAI_VASE(2)=0.1D0
			EPAI_VASE(3)=0.2D0
			EPAI_VASE(4)=1.5D0
			EPAI_VASE(5)=4.5D0
          DO I=1,NPOIN
			 DO J= 1,NOMBLAY	
			    AVAIL(I,J,1)= 0.
              AVAIL(I,J,2)= 1.
			  ENDDO

			if (Y(I)-X(I).ge.6098000.d0) then
!       maritime 
			DO J= 1,NOMBLAY	       
		     AVAIL(I,J,1)= 1.
              AVAIL(I,J,2)= 0.
				ENDDO
			
			elseif (X(I).lt.417927) then
!		central
!             DO J= 1,5	
!			  AVAIL(I,J,1)= 0.5
 !             AVAIL(I,J,2)= 1
!			  ENDDO
			  DO J= 1,NOMBLAY	
			  AVAIL(I,J,1)= 0.
              AVAIL(I,J,2)= 1.
			  ENDDO
			elseif (X(I).ge.417927.and.Y(I).ge.6441733) then
         
			    DO J= 1,NOMBLAY	
			  AVAIL(I,J,1)= 0.
              AVAIL(I,J,2)= 1.
			    ENDDO
			elseif (X(I).ge.421600.and.Y(I).ge.6419710
     &  .and.X(I).lt.450000)		then
!       Dordogne aval	 
			 DO J= 1,NOMBLAY	
			  AVAIL(I,J,1)= 0.
              AVAIL(I,J,2)= 1.
			    ENDDO
			elseif (X(I).ge.450000.and.Y(I).lt.6426000
     &  .and.Y(I).gt.6414000) then
!          Dordogne amont	 
			DO J= 1,NOMBLAY	       
		   AVAIL(I,J,1)= 0.
              AVAIL(I,J,2)= 1.
				ENDDO
			elseif (X(I).ge.417927.and.X(I).lt.421600
     &  .and.Y(I).lt.6441600) then
!        Garonne aval	 
         DO J= 1,NOMBLAY	
			  AVAIL(I,J,1)= 0.
              AVAIL(I,J,2)= 1.
			    ENDDO
			elseif (X(I).gt.421600.and.Y(I).gt.6392000
     &  .and.Y(I).lt.6430000) then
              DO J= 1,NOMBLAY	
			  AVAIL(I,J,1)= 0.
              AVAIL(I,J,2)= 1.
			    ENDDO
			else
!       Garonne amont		
        DO J= 1,NOMBLAY	       
		   AVAIL(I,J,1)= 0.
              AVAIL(I,J,2)= 1.
				ENDDO
			endif


      ENDDO

!
!-----------------------------------------------------------------------
!
!     INITIALISING THE NUMBER OF LAYERS
!
!         DO I=1,NPOIN
!	           DO J= 1,NOMBLAY	
!			  AVAIL(I,J,1)= 0.25
 !             AVAIL(I,J,2)= 0.75
!			  ENDDO
!	        if (Y(I)-X(I).ge.10000.d0) then
!				DO J= 1,NOMBLAY	       
!		   AVAIL(I,J,1)= 0.98
!              AVAIL(I,J,2)= 0.02
!				ENDDO
		       
!				ENDIF
!		    ENDDO
 
!		   DO 

  !    DO J= 1,NOMBLAY
!        EPAI_VASE(J) = 0.1D0
   !     IF(NSICLA.GT.1) THEN
   !       EPAI_SABLE(J) = AVA0(1)/AVA0(2)*EPAI_VASE(J)
   !     ENDIF
    !  ENDDO
!-----------------------------------------------------------------------
!
!     INITIALISING OF LAYER THICKNESS AND CONC
!

!     BY DEFAULT : UNIFORM BED COMPOSITION (KEY WORDS)
!     V6P3: IT WILL BE POSSIBLE TO HAVE A SPATIAL DISTRIBUTION OF THE BED CONC
!     V6P2: SO FAR THE MUD CONC IS CONSTANT PER LAYER
!     si mixte: calculer aussi les AVAI!
!
      DO I=1,NPOIN
        DO J= 1,NOMBLAY
!             
!          print *, CONC_VASE(J)
          CONC(I,J) = CONC_VASE(J)
		  
          ES(I,J)   = EPAI_VASE(J)
		  ENDDO
!		  DO J= 1,NOMBLAY
!
           ES(I,1)= 0. ! 75g/l
            ES(I,2)= 0. ! 93g/l
            ES(I,3)= 0. ! 111g/l
            ES(I,4)= 0.! 129g/l
            ES(I,5)= 0. ! 147g/l
            ES(I,6)= 0.! 165g/l
            ES(I,7)= 0. ! 183g/l
		    ES(I,8)= 0.! 201g/l
		    ES(I,9)= 0. ! 219g/l
		    ES(I,10)= 0.25 ! 237g/l
		    ES(I,11)= 0.25 ! 255g/l
		    ES(I,12)= 0.25 ! 273g/l
		    ES(I,13)= 0.25! 291g/l
		    ES(I,14)= 0.25 ! 309g/l
		    ES(I,15)= 0.25 ! 327g/l
            ES(I,16)= 0.25 ! 363g/l
            ES(I,17)= 0.25 ! 399g/l
		    ES(I,18)= 0.25 ! 435g/l
		    ES(I,19)= 0.25 ! 471g/l
!		  DO J= 1,NOMBLAY
!
			if (TM1(I).lt.-50) then
		   
!          CONC(I,J) = 0.11
!          ES(I,1)   = 0.1
!		  ES(I,2)=0.1
!		    ES(I,3)   = 0.1
!		  ES(I,4)=0.1
!		  ES(I,5)=0.1
!		   ES(I,10)= 0.! 237g/l
!		   ES(I,11)= 0.! 237g/l
           DO J= 1,NOMBLAY
		  		     AVAIL(I,J,1)= 0.
              AVAIL(I,J,2)= 1.!
			   ENDDO
!			     AVAIL(I,2,1)= 0.
!              AVAIL(I,2,2)= 1.
!			     AVAIL(I,3,1)= 0.
!              AVAIL(I,3,2)= 1.
!			     AVAIL(I,4,1)= 0.
!              AVAIL(I,4,2)= 1.
!			     AVAIL(I,5,1)= 0.
!              AVAIL(I,5,2)= 1.
		  
		  
		  endif
			
         if (TM1(I).gt.200) then
		   
!          CONC(I,J) = 0.11
          ES(I,1)   = 0.1
		  ES(I,2)=0.1
		    ES(I,3)   = 0.1
		  ES(I,4)=0.1
		  ES(I,5)=0.1
		   ES(I,10)= 0.! 237g/l
		   ES(I,11)= 0.! 237g/l
!		  		     AVAIL(I,1,1)= 0.
!              AVAIL(I,1,2)= 1.!
!			     AVAIL(I,2,1)= 0.
!              AVAIL(I,2,2)= 1.
!			     AVAIL(I,3,1)= 0.
!              AVAIL(I,3,2)= 1.
!			     AVAIL(I,4,1)= 0.
!              AVAIL(I,4,2)= 1.
!			     AVAIL(I,5,1)= 0.
!              AVAIL(I,5,2)= 1.
		  
		  
		  endif
!		    AVAIL(I,1,1)= 0.
!              AVAIL(I,1,2)= 1.
 		    if (Y(I)-X(I).ge.6098000.d0) then
            ES(I,1)= 0.
			ES(I,2)= 0.
		    ES(I,3)= 0.
            ES(I,4)= 0.
            ES(I,5)= 0.
            ES(I,6)= 0.
            ES(I,7)= 0.
		    ES(I,8)= 0.
		    ES(I,9)= 0.
		    ES(I,10)= 0.
		    ES(I,11)= 0.
		    ES(I,12)= 0.
		    ES(I,13)= 0.
		    ES(I,14)= 0.
		    ES(I,15)= 0.
		    ES(I,16)= 0.
		    ES(I,17)= 0.
		    ES(I,18)= 0.
		    ES(I,19)= 0.
        elseif (x(i).gt.438000) then
            ES(I,1)= 0
			ES(I,2)= 0.
		    ES(I,3)= 0.
            ES(I,4)= 0.
            ES(I,5)= 0.
            ES(I,6)= 0.
            ES(I,7)= 0.
		    ES(I,8)= 0.
		    ES(I,9)=0.
		  ES(I,10)=0.
		  ES(I,11)   = 0.
		  ES(I,12)=0.
		    ES(I,13)   = 0.
		  ES(I,14)=0.
		  ES(I,15)=0.
		     ES(I,16)   = 0.
		 ES(I,17)=0.
		    ES(I,18)   = 0.
		  ES(I,19)=0
!		  ES(I,10)=0.
        endif
		  
		  
		 ES(I,20)=5-ES(I,19)-ES(I,18)-ES(I,17)-ES(I,16)-ES(I,15)
     &		-ES(I,14)-ES(I,13)-ES(I,12)-ES(I,11)-ES(I,10)-ES(I,9)
     &       -ES(I,8)-ES(I,7)-ES(I,6)-ES(I,5)-ES(I,4)
     &	 -ES(I,3)-ES(I,2)-ES(I,1)
			

           
	
 		    
			
!		   print *, J,EPAI_VASE(J)
!
!          IF(NSICLA.GT.1) THEN
!            ES(I,J)= ES(I,J) + EPAI_SABLE(J)
!            IF(ES(I,J).GE.1.D-6) THEN
! Class 1 is for sand, class 2 is mud
!              AVAIL(I,J,1)= EPAI_SABLE(J)/ES(I,J)
!              AVAIL(I,J,2)= EPAI_VASE(J)/ES(I,J)
!            ELSE
!              AVAIL(I,J,1)= AVA0(1)
!              AVAIL(I,J,2)= AVA0(2)
!            ENDIF
!          ENDIF
!
!        ENDDO
      ENDDO
!
!-----------------------------------------------------------------------
!
      RETURN
      END
