C                       *****************
                        SUBROUTINE STRCHE
C                       *****************
C
C***********************************************************************
C  BIEF VERSION 5.2           01/10/96    J-M HERVOUET (LNH) 30 87 80 18
C
C***********************************************************************
C
C      FONCTION : CALCUL DU COEFFICIENT DE FROTTEMENT SUR LE FOND.
C                 SI IL  EST VARIABLE EN ESPACE.
C
C      CE SOUS-PROGRAMME EST SIMPLEMENT UN MODELE.
C      IL DOIT ETRE REMPLI PAR L'UTILISATEUR.
C
C-----------------------------------------------------------------------
C
C      FUNCTION: SETTING THE FRICTION COEFFICIENT IF IT IS VARIABLE
C                IN SPACE.
C
C      ONLY AN EXAMPLE IS GIVEN HERE, MUST BE IMPLEMENTED BY THE USER.
C      COMMENTS CEX MUST BE REMOVED TO IMPLEMENT THE EXAMPLE.
C
C-----------------------------------------------------------------------
C                             ARGUMENTS
C .________________.____.______________________________________________.
C |      NOM       |MODE|                   ROLE                       |
C |________________|____|______________________________________________|
C |    CF          |<-- |  COEFFICIENT DE FROTTEMENT                   |
C |    X,Y         | -->|  COORDONNEE DU MAILLAGE .                    |
C |    NPOIN       | -->|  NOMBRE DE POINTS DU MAILLAGE                |
C |    PRIVE       | -->|  TABLEAU DE TRAVAIL DEFINI DANS PRINCI       |
C |    ZF          | -->|  COTE DU FOND                                |
C |    KFROT       | -->|  LOI DE FROTTEMENT (LINEAIRE,CHEZY,STRICKLER)|
C |    FFON        | -->|  COEFFICIENT DE FROTTEMENT ASSOCIE A LA LOI  |
C |    MESH        | -->|  BLOC DES ENTIERS DU MAILLAGE.
C |________________|____|______________________________________________|
C MODE : -->(DONNEE NON MODIFIEE), <--(RESULTAT), <-->(DONNEE MODIFIEE)
C
C-----------------------------------------------------------------------
C
C  APPELE PAR : PREDAT
C
C  SOUS-PROGRAMME APPELE : OV
C
C**********************************************************************
C
      USE BIEF
C
C     DECLARATIONS MUST BE ADAPTED TO EVERY CODE
C     EXAMPLE OF TELEMAC2D HERE
C
      USE DECLARATIONS_TELEMAC2D
      USE DECLARATIONS_TELEMAC
      USE DECLARATIONS_SPECIAL
      IMPLICIT NONE
   


C
      INTEGER I
	   INTEGER NFO2
	  DOUBLE PRECISION  KF1,KF2,KF3,KF4,KF5,KF6,KF7,KF8,KF9
C      INTEGER, INTENT(IN) :: NPOIN2
C      DOUBLE PRECISION, DIMENSION(NPOIN2), INTENT(INOUT) :: ZF
C      DOUBLE PRECISION, DIMENSION(NPOIN2), INTENT(IN) :: X,Y 
C      TYPE (BIEF_MESH), INTENT(INOUT) :: MESH2D
C
C-----------------------------------------------------------------------
C
C
C
! 9K		 run 10
!		 		 					     KF1=47.5
!	      KF2=100
!	     KF3=100
!	     KF4=55
!	     KF5=60
!	     KF6=46
!		  KF7=60
!	     KF8=50
!	     KF9=40
		 
!		   9 K run 11
!		 					     KF1=50
!	      KF2=120
!	     KF3=120
!	     KF4=50
!	     KF5=60
!	     KF6=46
!		  KF7=50
!	     KF8=50
!	     KF9=40
		 
!		      9K run 12 //pi run14		 
! config run 12
        KF1=45
        KF2=90
        KF3=90
        KF4=50
        KF5=93
        KF6=46
        KF7=50
        KF8=45
        KF9=40
		 
! config run 13		 
!        KF1=45
!        KF2=90
!        KF3=90
!        KF4=45
!        KF5=93
!        KF6=46
!        KF7=50
!        KF8=45
!        KF9=40
		 
		 
      DO I=1,NPOIN

        if (Y(I)-X(I).ge.6098000.d0) then
!       maritime
        CHESTR%R(I) = KF1
        elseif (Y(I).gt.6475000) then
!		central
        CHESTR%R(I) = KF2
		elseif (Y(I).gt.6455000) then
!		central
        CHESTR%R(I) = KF3
		elseif (Y(I).gt.6438000) then
!		central
        CHESTR%R(I) = KF4
                elseif (X(I).ge.421600.and.Y(I).ge.6419710
     &  .and.X(I).lt.450000)		then
!       Dordogne aval	 
        CHESTR%R(I) = KF5
        elseif (X(I).ge.450000.and.Y(I).lt.6426000
     &  .and.Y(I).gt.6414000) then
!          Dordogne amont	 
        CHESTR%R(I) = KF6
        elseif (X(I).ge.417927.and.X(I).lt.426000
     &  .and.Y(I).lt.6441600) then
!        Garonne aval	 
        CHESTR%R(I) = KF7
		elseif (X(I).gt.426000.and.Y(I).gt.6392000) then
!     &  .and.Y(I).lt.6430000) then
        CHESTR%R(I) = KF8
        else
!       Garonne amont		
        CHESTR%R(I) = KF9
        endif


      ENDDO
C
C
C-----------------------------------------------------------------------
C
C
C-----------------------------------------------------------------------
C
C
CEX   INTEGER I
C
C-----------------------------------------------------------------------
C
C     HERE A CONSTANT VALUE OF FRICTION IS GIVEN
C
C      DO I=1,NPOIN2
C         CF%R(I) = 60.D0
C      END DO
C
C-----------------------------------------------------------------------
C
C     COMMENTS HERE MAY BE CHANGED
C
C      IF(LNG.EQ.1) THEN
C        WRITE(LU,*) 'STRCHE (BIEF) : PAS DE MODIFICATION DU FROTTEMENT'
C        WRITE(LU,*)
C      ENDIF
C      IF(LNG.EQ.2) THEN
C        WRITE(LU,*) 'STRCHE (BIEF): NO MODIFICATION OF FRICTION'
C        WRITE(LU,*)
C      ENDIF
C
C-----------------------------------------------------------------------
C
      RETURN
      END          
