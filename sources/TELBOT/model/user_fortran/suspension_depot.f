!                    ***************************
                     SUBROUTINE SUSPENSION_DEPOT
!                    ***************************
!
     &(TOB,HN, NPOIN, HMIN,XWC,VITCD,ZERO,KARMAN,
     & FDM,FD90,XMVE,T1,T2,ZREF,FLUDPT,DEBUG,SEDCO,CSTAEQ)
!
!***********************************************************************
! SISYPHE   V7P2
!***********************************************************************
!
!brief    COMPUTES THE FLUX OF DEPOSITION AND EROSION.
!
!note     T1: TOB
!note  TO DO:  REPLACE USTAR WITH TOB
!
!history  J-M HERVOUET + C VILLARET
!+        31/07/2008
!+        V5P9
!+
!
!history  N.DURAND (HRW), S.E.BOURBAN (HRW)
!+        13/07/2010
!+        V6P0
!+   Translation of French comments within the FORTRAN sources into
!+   English comments
!
!history  N.DURAND (HRW), S.E.BOURBAN (HRW)
!+        21/08/2010
!+        V6P0
!+   Creation of DOXYGEN tags for automated documentation and
!+   cross-referencing of the FORTRAN sources
!
!history  C.VILLARET (EDF-LNHE), P.TASSI (EDF-LNHE)
!+        19/07/2011
!+        V6P1
!+   Name of variables
!
!history  J-M HERVOUET & P.TASSI (EDF LAB, LNHE)
!+        31/05/2016
!+        V7P2
!+   Cleaner and safer formulas in the IF(SEDCO) case.
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!| DEBUG          |-->| FLAG FOR DEBUGGING
!| FLUDPT         |<->| IMPLICIT DEPOSITION FLUX
!| HMIN           |-->| MINIMUM VALUE OF WATER DEPTH
!| HN             |-->| WATER DEPTH
!| KARMAN         |-->| VON KARMAN CONSTANT
!| NPOIN          |-->| NUMBER OF POINTS
!| SEDCO          |-->| LOGICAL, SEDIMENT COHESIVE OR NOT
!| T1             |<->| WORK BIEF_OBJ STRUCTURE
!| T2             |<->| WORK BIEF_OBJ STRUCTURE
!| TOB            |-->| BED SHEAR STRESS (TOTAL FRICTION)
!| VITCD          |-->| CRITICAL SHEAR VELOCITY FOR MUD DEPOSITION
!| XMVE           |-->| FLUID DENSITY
!| XWC            |-->| SETTLING VELOCITIES
!| ZERO           |-->| ZERO
!| ZREF           |<->| REFERENCE ELEVATION
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE DECLARATIONS_SISYPHE, ONLY : SET_LAG,CS,U2D,V2D,MIXTE
      USE INTERFACE_SISYPHE,EX_SUSPENSION_DEPOT => SUSPENSION_DEPOT
      USE BIEF
      USE DECLARATIONS_SPECIAL
      IMPLICIT NONE
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      TYPE (BIEF_OBJ),  INTENT(IN)    ::  HN,TOB,CSTAEQ
      INTEGER,          INTENT(IN)    ::  NPOIN,DEBUG
      LOGICAL,          INTENT(IN)    :: SEDCO
      DOUBLE PRECISION, INTENT(IN)    ::  HMIN
      DOUBLE PRECISION, INTENT(IN)    :: FDM,FD90,XWC
      DOUBLE PRECISION, INTENT(IN)    :: VITCD
      DOUBLE PRECISION, INTENT(IN)    :: ZERO, KARMAN,XMVE
      TYPE (BIEF_OBJ),  INTENT(INOUT) :: T1,T2
      TYPE (BIEF_OBJ),  INTENT(IN)    :: ZREF
      TYPE (BIEF_OBJ),  INTENT(INOUT) :: FLUDPT
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER :: I
      DOUBLE PRECISION :: AUX
	   DOUBLE PRECISION :: ws,wm,mcs,mcm,rfs,rfm,velo
	  DOUBLE PRECISION ::  vitcdp2,  vitcdp1,ustar,ps,pm
!	   modif NH	  
	     integer mixmth
! MIXMTH=1 method V7P2 and older
!  MIXMTH=2 KUL method 
!
!======================================================================!
!                               PROGRAM                                !
!======================================================================!
!
!     ****************************************************
!     THE TOTAL FRICTION VELOCITY    --> USTAR (T1)
!     HAS BEEN REPLACED BY USTARP (SKIN FRICTION VELOCITY)
!     FOR EROSION FLUX FROM V6P0 ON
!     ****************************************************
!
     
      CALL OS('X=CY    ', X=T1, Y=TOB, C=1.D0/XMVE)
!     TOB assumed >=0, otherwise mistake elsewhere...
      CALL OS('X=SQR(Y)', X=T1, Y=T1)
!
         mixmth=1
		 
        

        if ( mixmth.eq.1) then
         IF(SEDCO) THEN
!
!       *********************************************************
!       IA - FORMULATION FOR COHESIVE SEDIMENTS (WITHOUT BEDLOAD)
!       *********************************************************
!
!       COMPUTES THE PROBABILITY FOR DEPOSITION
!
          DO I = 1, NPOIN
!         HERE T1 >=0, so case VITCD=0.D0 excluded by the test
          IF(T1%R(I).LT.VITCD) THEN
            AUX = 1.D0-(T1%R(I)/VITCD)**2
          ELSE
            AUX = 0.D0
          ENDIF
!         COMPUTES THE IMPLICIT PART OF THE DEPOSITION FLUX
          FLUDPT%R(I)= XWC*AUX
!	  print *,'dep', I,XWC,AUX
        ENDDO
!       UNIFORM SEDIMENT ALONG THE VERTICAL
        CALL CPSTVC(TOB,T2)
        CALL OS('X=C     ', X=T2, C=1.D0)
!
!       **********************************************************
!       IB - FORMULATION FOR NON-COHESIVE SEDIMENTS (WITH BEDLOAD)
!       **********************************************************
!
      ELSE
!
!       *******************************************************
!       COMPUTES THE RATIO BETWEEN NEAR BED CONC. AND MEAN CONC
!                                  -->  T2    (TO KEEP )
!       *******************************************************
!
!       DMK Modification 06/05/2011
        IF(.NOT.(SET_LAG)) THEN
          IF(DEBUG > 0) WRITE(LU,*) 'SUSPENSION_ROUSE'
          CALL SUSPENSION_ROUSE(T1,HN,NPOIN,
     &                        KARMAN,HMIN,ZERO,XWC,ZREF,T2)
          IF(DEBUG > 0) WRITE(LU,*) 'END SUSPENSION_ROUSE'
        ELSE
          IF(DEBUG > 0) WRITE(LU,*) 'SUSPENSION_BETAFACTOR'
          CALL SUSPENSION_MILES(HN,NPOIN,KARMAN,HMIN,ZERO,
     &                  FDM,FD90,XWC,ZREF,T2)
          IF(DEBUG > 0) WRITE(LU,*) 'END SUSPENSION_BETAFACTOR'
        ENDIF
!       End of DMK mod
!
!       **************************************************
!       COMPUTES THE DEPOSITION FLUX --> FLUDPT = XWC * T2
!       **************************************************
!
        CALL OS('X=CY    ', X=FLUDPT, Y=T2, C=XWC)
!
      ENDIF
	  elseif ( mixmth.eq.2) then
			 ws=1.d-2
			 wm=1.d-3
	     do i = 1, npoin
		 velo=max(dsqrt(u2d%r(i)**2+v2d%r(i)**2),1.d-08)
		 ustar=t1%r(i)
		     if (mixte)	 then
		         mcs=cs%adr(1)%p%r(i) *2650.d0
                 mcm=cs%adr(2)%p%r(i) *2650.d0
				  elseif (sedco) then
				  mcs=0.d0!*2650.d0
                 mcm=cs%adr(1)%p%r(i) *2650.d0
                 else
				  mcs=cs%adr(1)%p%r(i) *2650.d0
                 mcm=0.d0 !*1600.d0
				  endif
!		         mcs=cs%adr(1)%p%r(i) !*2650.d0
!        mcm=cs%adr(2)%p%r(i) !*1600.d0

	    rfs=0.25*(ustar/ws)**2*0.01d0/(1.d0+(ustar/ws)**2*0.01d0)
		   CALL VITCHU_VARIABLE( wm ,mcm )	
        rfm=0.25*(ustar/wm)**2*0.01d0/(1.d0+(ustar/wm)**2*0.01d0)
         
		  if(sedco) then
 !           CALL VITCHU_VARIABLE( wm ,mcm )
          vitcdp2=(1.d0-1025.d0/1600.d0)*9.81d0*hn%r(i)/rfm*wm*mcm/velo
		   
		   
		    if (vitcdp2.gt.1.d-08 .and. (mcs+mcm).gt.1.d-8) then
          pm = max(1.d0-(mcm/(mcs+mcm)*tob%r(i))/vitcdp2,zero)
            else
          pm = 0.d0
            endif
		
			fludpt%r(i)=wm*pm *mcm/2560.d0
			    
		  
         else
		 
		  vitcdp1=(1.d0-1025.d0/2650.d0)*9.81d0*hn%r(i)/rfs*ws*mcs/velo
        if (vitcdp1.gt.1.d-08 .and. (mcs+mcm).gt.1.d-8) then
          ps = max(1.d0-(mcs/(mcs+mcm)*tob%r(i))/vitcdp1,zero)
        else
          ps = 0.d0
        endif
		
        fludpt%r(i)=ws*ps*mcs/2560.d0
          endif
!		    endif
		    enddo
		  
         endif
        
!        FLUDPT_VASE%R(I)=WM*PM !*CS%ADR(2)%P%R(I)
!
!======================================================================!
!======================================================================!
!
      RETURN
      END

