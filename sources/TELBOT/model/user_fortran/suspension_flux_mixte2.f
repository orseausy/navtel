
!                   ********************************
                    SUBROUTINE SUSPENSION_FLUX_MIXTE2
!                   ********************************
!
     &(TAUP,HN,FDM,NPOIN,CHARR,XMVE,XMVS,VCE,GRAV,HMIN,XWC,
     & ZERO,PARTHENIADES,FLUER_SABLE,FLUER_VASE,ZREF,
     & AC,CSTAEQ,QSC,ICQ,DEBUG,AVAIL,NSICLA,ES,
     & TOCE_VASE,TOCE_SABLE,NOMBLAY,DT,TOCE_MIXTE,MS_SABLE,MS_VASE,CONC)
!
!***********************************************************************
! SISYPHE   V7P0                                   21/07/2011
!***********************************************************************
!
!brief    COMPUTES THE FLUX OF DEPOSITION AND EROSION.
!
!history  C. VILLARET, JMH
!+        2008
!+
!+
!
!history  N.DURAND (HRW), S.E.BOURBAN (HRW)
!+        13/07/2010
!+        V6P0
!+   Translation of French comments within the FORTRAN sources into
!+   English comments
!
!history  N.DURAND (HRW), S.E.BOURBAN (HRW)
!+        21/08/2010
!+        V6P0
!+   Creation of DOXYGEN tags for automated documentation and
!+   cross-referencing of the FORTRAN sources
!
!history  C. VILLARET (EDF-LNHE)
!+        20/03/2011
!+        V6P1
!+  Change of arguments FDM insteam of ACLADM
!+   KARMAN suppressed
!+   Added TOCE _ SABLE + VCE
!
!history  C.VILLARET (EDF-LNHE), P.TASSI (EDF-LNHE)
!+        19/07/2011
!+        V6P1
!+   Name of variables
!+
!
!history  J-M HERVOUET (EDFLAB, LNHE)
!+        28/04/2014
!+        V7P0
!+   Possible divisions by 0 secured. Formatting.
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!| AC             |<->| CRITICAL SHIELDS PARAMETER
!| FDM            |-->| DIAMETER DM FOR EACH CLASS
!| AVAIL          |<->| VOLUME PERCENT OF EACH CLASS
!| CHARR          |-->| BEDLOAD
!| CS             |<->| CONCENTRATION AT TIME N
!| CSTAEQ         |<->| EQUILIBRIUM CONCENTRATION
!| DEBUG          |-->| FLAG FOR DEBUGGING
!| DT             |-->| TIME STEP
!| ES             |<->| LAYER THICKNESSES AS DOUBLE PRECISION
!| FLUER_SABLE    |<->| EROSION FLUX FOR MIXED SEDIMENTS
!| FLUER_VASE     |<->| EROSION FLUX FOR MIXED SEDIMENTS
!| GRAV           |-->| ACCELERATION OF GRAVITY
!| HMIN           |-->| MINIMUM VALUE OF WATER DEPTH
!| HN             |-->| WATER DEPTH
!| ICQ            |-->| REFERENCE CONCENTRATION FORMULA
!| MS_SABLE       |<->| MASS OF SAND PER LAYER (KG/M2)
!| MS_VASE        |<->| MASS OF MUD PER LAYER (KG/M2)
!| NOMBLAY        |-->| NUMBER OF LAYERS FOR CONSOLIDATION
!| NPOIN          |-->| NUMBER OF POINTS
!| NSICLA         |-->| NUMBER OF SIZE CLASSES FOR BED MATERIALS
!| PARTHENIADES   |-->| CONSTANT OF THE KRONE AND PARTHENIADES EROSION LAW (KG/M2/S)
!| QSC            |<->| BEDLOAD TRANSPORT RATE
!| TAUP           |-->| CRITICAL SHEAR STRESS
!| TOCE_MIXTE     |<->| CRITICAL SHEAR STRESS FOR MIXED SEDIMENTS
!| TOCE_SABLE     |<->| CRITICAL SHEAR STRESS FOR SAND
!| TOCE_VASE      |<->| CRITICAL EROSION SHEAR STRESS OF THE MUD PER LAYER (N/M2)
!| VCE            |-->| FLOW VISCOSITY
!| XMVE           |-->| FLUID DENSITY
!| XMVS           |-->| WATER DENSITY
!| XWC            |-->| SETTLING VELOCITIES
!| ZERO           |-->| ZERO
!| ZREF           |<->| REFERENCE ELEVATION
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE INTERFACE_SISYPHE, EX_FLUX_MIXTE=>SUSPENSION_FLUX_MIXTE
      USE BIEF
      USE DECLARATIONS_SISYPHE, ONLY : NLAYMAX
	     USE DECLARATIONS_TELEMAC2D, ONLY : X,Y
!
      USE DECLARATIONS_SPECIAL
      IMPLICIT NONE
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      TYPE (BIEF_OBJ),  INTENT(IN)     :: TAUP,HN
      INTEGER,          INTENT(IN)     :: NPOIN,DEBUG,NSICLA
      INTEGER,          INTENT(IN)     :: NOMBLAY
      LOGICAL,          INTENT(IN)     :: CHARR
      DOUBLE PRECISION, INTENT(IN)     :: XMVE, XMVS, VCE,GRAV, HMIN
      DOUBLE PRECISION, INTENT(IN)     :: XWC
      DOUBLE PRECISION, INTENT(IN)     :: ZERO, PARTHENIADES
      TYPE (BIEF_OBJ),  INTENT(IN)     :: ZREF
      DOUBLE PRECISION, INTENT(INOUT)  :: AVAIL(NPOIN,NOMBLAY,NSICLA)
      DOUBLE PRECISION, INTENT(INOUT)  :: AC
      DOUBLE PRECISION, INTENT(INOUT)  :: ES(NPOIN,NOMBLAY)
      TYPE (BIEF_OBJ),  INTENT(INOUT)  :: CSTAEQ
      TYPE (BIEF_OBJ),  INTENT(INOUT)  :: FLUER_SABLE,FLUER_VASE
      DOUBLE PRECISION, INTENT(INOUT)  :: MS_SABLE(NPOIN,NOMBLAY)
      DOUBLE PRECISION, INTENT(INOUT)  :: MS_VASE(NPOIN,NOMBLAY)
      DOUBLE PRECISION, INTENT(INOUT)  :: TOCE_MIXTE(NPOIN,NOMBLAY)
      DOUBLE PRECISION, INTENT(IN)     :: DT, FDM
      TYPE(BIEF_OBJ),   INTENT(IN)     ::  QSC
      INTEGER,          INTENT (IN)    :: ICQ
      DOUBLE PRECISION, INTENT(IN)     :: TOCE_VASE(NOMBLAY)
      DOUBLE PRECISION, INTENT(IN)     :: TOCE_SABLE
	  DOUBLE PRECISION, INTENT(INOUT) :: CONC(NPOIN,NOMBLAY)
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER I, J
      DOUBLE PRECISION FLUERSABLE(NOMBLAY),FLUERVASE(NOMBLAY)
	 DOUBLE PRECISION FLUER_LOC(NLAYMAX)
!
      DOUBLE PRECISION QE_MOY,TEMPS,QER_VASE,QER_SABLE
      DOUBLE PRECISION F2,DETER
! modif NH	  
	     integer mixmthe
		  double precision e0s, e0v, as, av
		  double precision e0m, am
! MIXMTH=1 method V7P2 and older
! MIXMTH=2 KUL method 
		 		 
!
!======================================================================!
!======================================================================!
!                               PROGRAM                                !
!======================================================================!
!======================================================================!
!
!     DOES THE EROSION COMPUTATION ONLY ONCE (SAND FOR EXAMPLE
!     BECAUSE THE COMPUTED FLUX IS A GLOBAL FLUX COMMON TO THE 2 SEDIMENTS)
!     COMPUTES THE THEORETICAL FLUX OF EROSION FOR EACH (SEDIMENT INFINITELY
!     AVAILABLE IN EACH LAYER)
!
!     COMPUTES THE CRITICAL STRESS FOR EACH LAYER AS A FUNCTION
!     OF THE PROPORTION OF MUD
!
!     modif nh
!             print *,'err', TOCE_SABLE
         mixmthe=2
	        e0s = 0.0005d0/2650.d0
!	 0.55*(C/1000)**3		
 !     e0v = 0.55
         e0v = 0.002d0/2650.d0
!        e0v =0.25E-13
      as = 0.5d0
      av = 1.0d0

      DO J=1,NOMBLAY
        DO I=1,NPOIN
!         WRITE(LU,*)'I=',I,' J=',J,' (MS_VASE(I, J)=',MS_VASE(I,J)
!         WRITE(LU,*)'I=',I,' J=',J,' (MS_SABLE(I, J)=',MS_SABLE(I,J)
          DETER=MS_VASE(I,J) + MS_SABLE(I,J)
!		    if ((I.EQ.5781).AND.(J.EQ.1)) then
!				print *, 'mass1',J,  DETER,MS_VASE(I,J),MS_SABLE(I,J)
!				endif
          IF(DETER.GT.1.D-20) THEN
            F2=MS_VASE(I, J)/DETER
          ELSE
            F2=0.5D0
          ENDIF
!         F2= MS_VASE(I, J)/(MS_VASE(I, J) + MS_SABLE(I, J))
          if (mixmthe.eq.1) then         
		    IF(F2.LE.0.3D0) THEN
            TOCE_MIXTE(I,J)=TOCE_SABLE
			   if ((X(I).gt.452000).or.
     &			 (Y(I).LT.6400000)) then
	            TOCE_MIXTE(I,20)=100
				  endif
		    ELSEIF(F2.GE.0.5D0)THEN
            TOCE_MIXTE(I,J)=TOCE_VASE(J)
			   if ((X(I).gt.452000).or.
     &			 (Y(I).LT.6400000)) then
	            TOCE_MIXTE(I,20)=100
				 endif
            ELSE
            TOCE_MIXTE(I,J)=TOCE_SABLE +
     &     (F2-0.3D0)*(TOCE_VASE(J)-TOCE_SABLE)/(0.5D0-0.3D0)
	       	     if ((X(I).gt.452000).or.
     &			 (Y(I).lt.6400000)) then
	            TOCE_MIXTE(I,20)=100
				  endif
            ENDIF
		    elseif (mixmthe.eq.2) then
              if(f2.le.0.3d0)then
             toce_mixte(i,j)=toce_sable+0.5d0*f2
			   if ((X(I).gt.452000).or.
     &			 (Y(I).LT.6400000)) then
	            TOCE_MIXTE(I,20)=100
				  endif
			   elseif(f2.ge.0.5d0)then
             toce_mixte(i,j)=toce_vase(j)
			    if ((X(I).gt.452000).or.
     &			 (Y(I).LT.6400000)) then
	            TOCE_MIXTE(I,20)=100
				  endif
               elseif((f2.ge.0.3d0).and.(f2.le.0.5d0)) then
             toce_mixte(i,j)=(toce_sable+0.5d0*0.3d0)/(0.5d0-0.3d0)
     &       *(0.5d0-f2)+(f2-0.3d0)*toce_vase(j)/(0.5d0-0.3d0)
	           if ((X(I).gt.452000).or.
     &			 (Y(I).lt.6400000)) then
	            TOCE_MIXTE(I,20)=100
				  endif
               endif
			   else
		     endif
!             if ((I.EQ.6538).AND.(J.EQ.1)) then
!			   print *,'tau',f2,J,toce_mixte(i,j),taup%r(i)
!			   endif 
              
		
        ENDDO
		
      ENDDO
!
      IF(ICQ.EQ.1) THEN
        IF(DEBUG > 0) WRITE(LU,*) 'SUSPENSION_FREDSOE'
        CALL SUSPENSION_FREDSOE(FDM,TAUP,NPOIN,
     &                           GRAV,XMVE,XMVS,ZERO,AC,CSTAEQ)
        IF(DEBUG > 0) WRITE(LU,*) 'END SUSPENSION_FREDSOE'
!
        DO I=1,NPOIN
          CSTAEQ%R(I)=CSTAEQ%R(I)*AVAIL(I,1,1)
        ENDDO
!
      ELSEIF(ICQ.EQ.2) THEN
!
        IF(DEBUG > 0) WRITE(LU,*) 'SUSPENSION_BIJKER'
        CALL SUSPENSION_BIJKER(TAUP,HN,NPOIN,CHARR,QSC,ZREF,
     &                         ZERO,HMIN,CSTAEQ,XMVE)
        IF(DEBUG > 0) WRITE(LU,*) 'END SUSPENSION_BIJKER'
!
      ELSEIF(ICQ.EQ.3) THEN
        IF(DEBUG > 0) WRITE(LU,*) 'SUSPENSION_VANRIJN'
        CALL SUSPENSION_VANRIJN(FDM,TAUP,NPOIN,
     &                          GRAV,XMVE,XMVS,VCE,
     &                          ZERO,AC,CSTAEQ,ZREF)
        IF(DEBUG > 0) WRITE(LU,*) 'END SUSPENSION_VANRIJN'
        DO I=1,NPOIN
          CSTAEQ%R(I)=CSTAEQ%R(I)*AVAIL(I,1,1)
        ENDDO
!
      ENDIF
!
      DO I=1,NPOIN
!
        DO J=1,NOMBLAY
!
!         COMPUTES FLUER_SABLE_VASE AS A FUNCTION OF THE PROPORTION OF MUD
!
          DETER=MS_VASE(I,J) + MS_SABLE(I,J)
		    if ((I.EQ.5781).AND.(J.EQ.1)) then
				endif
          IF(DETER.GT.1.D-20) THEN
            F2=MS_VASE(I, J)/DETER
          ELSE
            F2=0.5D0
          ENDIF
		    if (mixmthe.eq.1) then   
		  
!         F2= MS_VASE(I, J)/(MS_VASE(I, J) + MS_SABLE(I, J))
              IF(F2.LE.0.3D0) THEN
!           PROPORTION OF MUD < 30%, FLUXES ARE SIMILAR TO THOSE FOR SAND ONLY
                IF(TAUP%R(I).GT.TOCE_MIXTE(I,J))THEN
				
              FLUER_LOC(J)=CSTAEQ%R(I)*XWC
			    if (I.eq.3746) then
				endif
!			    FLUER_LOC(J)=PARTHENIADES*(TAUP%R(I)/TOCE_MIXTE(I,J)-1.D0)**as
                ELSE
              FLUER_LOC(J)=0.D0
               ENDIF
             ELSEIF(F2.GE.0.5D0) THEN
!           PROPORTION OF MUD > 50%, FLUXES ARE SIMILAR TO THOSE FOR MUD ONLY
                IF(TAUP%R(I).GT.TOCE_MIXTE(I,J))THEN
              FLUER_LOC(J)=PARTHENIADES*
     &              ((TAUP%R(I)/TOCE_MIXTE(I,J))-1.D0)**as
                ELSE
              FLUER_LOC(J)=0.D0
               ENDIF
            ELSE
!           PROPORTION OF MUD >30% AND <50%, INTERPOLATES THE FLUXES
!           AND CRITICAL SHEAR STRESS
              IF(TAUP%R(I).GT.TOCE_MIXTE(I,J)) THEN
              FLUERSABLE (j)=CSTAEQ%R(I)*XWC
			  if (I.eq.9282) then
				endif
!              FLUERSABLE (j)=PARTHENIADES*
!     &			 (TAUP%R(I)/TOCE_MIXTE(I,J)-1.D0)**as
              FLUERVASE(j)=PARTHENIADES*
     &			  (TAUP%R(I)/TOCE_MIXTE(I,J)-1.D0)**as
              ELSE
              FLUERSABLE(j)=0.D0
              FLUERVASE(j)=0.D0
              ENDIF
              FLUER_LOC(J)=(F2-0.3D0)/
     &          (0.5D0-0.3D0)*(FLUERVASE(j)-FLUERSABLE(j))+FLUERSABLE(j)
             ENDIF
		    elseif (mixmthe.eq.2) then
		      if(f2.le.0.3d0)then
!-------------proportion of mud < 30%
              if(taup%r(i).gt.toce_mixte(i,j))then
			  
                  FLUERSABLE(j) = (1.d0-f2)*e0s*
     &                (taup%r(i)/toce_mixte(i,j)-1.d0)**as
                  fluervase(j)=f2*e0s
     &                     *(taup%r(i)/toce_mixte(i,j)-1.d0)**as
            else
               fluersable(j)=0.d0
               fluervase(j)=0.d0
            endif
            fluer_loc(j)=(fluersable(j)+fluervase(j))
!-------------proportion of mud > 50%
! modih nh e0v*(conc(i,j)/1000.)**3.0
          elseif(f2.ge.0.5d0)then
            if(taup%r(i).gt.toce_mixte(i,j))then
               fluersable(j)=(1.d0-f2)*e0v
     &                      *(taup%r(i)/toce_mixte(i,j)-1.d0)**av
               fluervase(j)=e0v
     &                     *(taup%r(i)/toce_mixte(i,j)-1.d0)**av
!	          if (I.EQ.86014) then
		    
!		   endif
!	       
            else
               fluersable(j)=0.d0
               fluervase(j)=0.d0
            endif
            fluer_loc(j)=(fluersable(j)+fluervase(j))
!-------------proportion of 30% < mud < 50%
          else
            if(taup%r(i).gt.toce_mixte(i,j))then
               e0m=e0s+(f2-0.3d0)*(e0v-e0s)/(0.5d0-0.3d0)
               am=as+(f2-0.3d0)*(av-as)/(0.5d0-0.3d0)
               fluersable(j)=(1.d0-f2)*e0m
     &                      *(taup%r(i)/toce_mixte(i,j)-1.d0)**am
               fluervase(j)=f2*e0m
     &                     *(taup%r(i)/toce_mixte(i,j)-1.d0)**am
            else
               fluersable(j)=0.d0
               fluervase(j)=0.d0
            endif
            fluer_loc(j)=(fluersable(j)+fluervase(j))
          endif
		    endif
             if ((I.EQ.6538).AND.(J.EQ.1)) then
			   print *,'2',f2,J,fluer_loc (j),fluersable(j),fluervase(j)
			   endif 
        ENDDO
!
!       COMPUTES THE EROSION DEPTH ZER_MOY AND ERODED MASSES
!
        QER_VASE = 0.D0
        QER_SABLE = 0.D0
!
        TEMPS= DT
!
        DO J= 1, NOMBLAY
		    if (mixmthe.eq.1) then
              IF(ES(I,J).GE.1.D-6) THEN
!           COMPUTES THE MASS POTENTIALLY ERODABLE IN LAYER J (KG/M2)
            QE_MOY= FLUER_LOC(J) *XMVS * TEMPS
               IF(QE_MOY.LT.MS_SABLE(I,J)+MS_VASE(I,J)) THEN
              QER_VASE = QER_VASE
     &     +  QE_MOY*MS_VASE(I,J)/(MS_VASE(I,J)+MS_SABLE(I,J))
              QER_SABLE = QER_SABLE
     &     +  QE_MOY*MS_SABLE(I,J)/(MS_VASE(I,J)+MS_SABLE(I,J))
              GO TO 10
                ELSE
              QER_VASE = QER_VASE + MS_VASE(I,J)
              QER_SABLE = QER_SABLE + MS_SABLE(I,J)
              TEMPS= TEMPS -
     &        (MS_SABLE(I,J)+MS_VASE(I,J))/FLUER_LOC(J)/XMVS
               ENDIF
             ENDIF
		    elseif (mixmthe.eq.2) then
		      if(es(i,j).ge.1.d-6) then
!
! computes the mass potentially erodable in layer j (kg/m2)
!
              qe_moy= (fluersable(j)*xmvs+fluervase(j)*2650.d0)*temps
!
              if(qe_moy.lt.(ms_sable(i,j)
     &            +ms_vase(i,j))) then
!
                  qer_vase = qer_vase
     &                  + fluervase(j)*1600.d0*temps
                  qer_sable = qer_sable
     &                  + fluersable(j)*xmvs*temps
!
                  go to 10
!
              else
!
                  qer_vase = qer_vase + ms_vase(i,j)
                  qer_sable = qer_sable + ms_sable(i,j)
                  temps= temps -(ms_sable(i,j)+ms_vase(i,j))
     &                   /(fluersable(j)*xmvs+fluervase(j)*2650.d0)
              endif
              endif
			   endif

        ENDDO
!        IF(LNG.EQ.1) THEN
!          WRITE(LU,*) 'ATTENTION TOUTES LES COUCHES SONT VIDES'
!        ENDIF
 !       IF(LNG.EQ.2) THEN
!          WRITE(LU,*) 'BEWARE, ALL LAYERS ARE EMPTY'
!        ENDIF
!        CALL PLANTE(1)
!       STOP
10      CONTINUE
!
!       Q_VASE REPRESENTS THE SURFACE MASS OF MUD
!       TO BE ERODED TO REACH ZER_MOY
!       Q_SABLE REPRESENTS THE SURFACE MASS OF SAND
!       TO BE ERODED TO REACH ZER_MOY
!
        FLUER_VASE%R(I)  = QER_VASE /(DT*XMVS)
        FLUER_SABLE%R(I) = QER_SABLE/(DT*XMVS)
!
      ENDDO
!
!-----------------------------------------------------------------------
!
      RETURN
      END
