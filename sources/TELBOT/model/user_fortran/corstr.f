!                    *****************
                     SUBROUTINE CORSTR
!                    *****************
!
!
!***********************************************************************
! TELEMAC2D   V6P1                                   21/08/2010
!***********************************************************************
!
!brief    CORRECTS THE FRICTION COEFFICIENT ON THE BOTTOM
!+                WHEN IT IS VARIABLE IN TIME.
!
!warning  USER SUBROUTINE; MUST BE CODED BY THE USER; THIS IS MERELY AN EXAMPLE
!code
!+2D   DO I = 1 , NPOIN
!+2D     IF(AT.GT.1200.D0) THEN
!+2D       CHESTR%R(I) = 40.D0
!+2D     ELSE
!+2D       CHESTR%R(I) = 60.D0
!+2D     ENDIF
!+2D   ENDDO
!
!history  J-M HERVOUET (LNHE)
!+        17/08/1994
!+        V5P6
!+
!
!history  N.DURAND (HRW), S.E.BOURBAN (HRW)
!+        13/07/2010
!+        V6P0
!+   Translation of French comments within the FORTRAN sources into
!+   English comments
!
!history  N.DURAND (HRW), S.E.BOURBAN (HRW)
!+        21/08/2010
!+        V6P0
!+   Creation of DOXYGEN tags for automated documentation and
!+   cross-referencing of the FORTRAN sources
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE BIEF
!
!     C2D: EXAMPLE FOR TELEMAC-2D
!     C3D: EXAMPLE FOR TELEMAC-3D
!
      USE DECLARATIONS_TELEMAC2D
!3D   USE DECLARATIONS_TELEMAC3D
!
      USE DECLARATIONS_TELEMAC
      USE DECLARATIONS_SPECIAL
      IMPLICIT NONE
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      DOUBLE PRECISION   :: COEF
      DOUBLE PRECISION   :: KF1,KF2,KF3,KF4,KF5,KF6,KF7,KF8,KF9
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!      
      INTEGER :: lskip
      INTEGER :: I,J,L,io
       DOUBLE PRECISION , DIMENSION(:), ALLOCATABLE :: TIME,SL,m,n,Q1,Q2
       DOUBLE PRECISION , DIMENSION(:), ALLOCATABLE :: Q_TOTAL
      DOUBLE PRECISION  :: TIME_tmp,SL_tmp,m_tmp
      DOUBLE PRECISION  :: n_tmp,Q1_tmp,Q2_tmp
      DOUBLE PRECISION  :: Q_INTERP,Q1_INTERP,Q2_INTERP
       DOUBLE PRECISION  :: ELAPSED_TIME
!3D   INTEGER I
!
!-----------------------------------------------------------------------
      !--- File opening
      ! WRITE(*,*) 'SO: FILE NAME du DEBIT:', T2D_FILES(T2DIMP)%NAME
      ELAPSED_TIME = LT*DT
      ! WRITE(*,*) 'SO: ET = ', ELAPSED_TIME
      OPEN(10,FILE='/home/orseausy/Git/navtel/sources/TELBOT'//
     & '/model/q_navtel.txt',
     & STATUS='old',ACTION='read',POSITION='asis') 
      ALLOCATE( TIME(0),SL(0),m(0),n(0),Q1(0),Q2(0) )
      !--- Skip first 4 lines
      DO lskip = 1, 4
        READ(10,*)
      END DO
      L = 0
      DO
        READ(10,*,iostat=io) TIME_tmp,SL_tmp,m_tmp,
     &       n_tmp,Q1_tmp,Q2_tmp
        TIME = [TIME, TIME_tmp]
        SL = [SL, SL_tmp]
        m = [m, m_tmp]
        n = [n, n_tmp]
        Q1 = [Q1, Q1_tmp]
        Q2 = [Q2, Q2_tmp]
        IF(io/=0) EXIT
        L = L+1
      END DO
      Q_TOTAL = Q1 + Q2
      ! WRITE(*,*) 'SO: SIZE = ', L
      ! WRITE(*,*) 'SO: TIME = ', TIME
      ! WRITE(*,*) 'SO: TOTAL RIVER DISCHARGE = ', Q_TOTAL
      CLOSE(10)
!-----------------------------------------------------------------------     
      !--- Interpolation
      DO J = 1, L 
        IF(ELAPSED_TIME.GE.TIME(J).AND.ELAPSED_TIME.LT.TIME(J+1)) THEN
          IF(Q_TOTAL(J+1)-Q_TOTAL(J).GT.1.D-6) THEN
            COEF = (ELAPSED_TIME-TIME(J)) / (TIME(J+1)-TIME(J))
          ELSE
            COEF = 0.D0
          ENDIF
          Q_INTERP = Q_TOTAL(J)+COEF*(Q_TOTAL(J+1)-Q_TOTAl(J))
          Q1_INTERP = Q1(J)+COEF*(Q1(J+1)-Q1(J))
          Q2_INTERP = Q2(J)+COEF*(Q2(J+1)-Q2(J))
        ENDIF
      ENDDO
!
!-----------------------------------------------------------------------  
      !--- Computation of Strickler coefficients
      DO I = 1, NPOIN
        ! ZONE K1 
        IF (Y(I)-X(I).GE.6098000.d0) THEN
          CHESTR%R(I) = -2.32926E-6*Q_INTERP**2+8.56043E-3*
     &  Q_INTERP+47.8881
        ! ZONE K2
        ELSEIF (Y(I).GT.6475000) THEN
          CHESTR%R(I) = -0.00001*Q_INTERP**2+0.03068+64.15048
        ! ZONE K3
        ELSEIF (Y(I).GT.6455000) THEN
          CHESTR%R(I) = 0.00234*Q2_INTERP**2-1.04218+212.07088
        ! ZONE K4
        ELSEIF (Y(I).GT.6438000) THEN
          CHESTR%R(I) = 8.62415E-6*Q2_INTERP**3-5.05151E-3*
     &  Q2_INTERP**2+8.58473E-1*Q2_INTERP-11.3565
        ! ZONE K5
        ELSEIF (X(I).GE.421600.and.Y(I).GE.6419710
     &  .and.X(I).LT.450000) THEN 
          CHESTR%R(I) = 608.62619*Q1_INTERP**-0.34416
        ! ZONE K6
        ELSEIF (X(I).GE.450000.and.Y(I).LT.6426000
     &  .and.Y(I).GT.6414000) THEN 
          CHESTR%R(I) = 3523.14498*Q1_INTERP**-0.71578
        ! ZONE K7
        ELSEIF (X(I).GE.417927.and.X(I).LT.426000
     &  .and.Y(I).LT.6441600) THEN 
          CHESTR%R(I) = -0.00018*Q1_INTERP**2+0.22677*
     &  Q1_INTERP-12.40622
        ELSE
        ENDIF
        ! THRESHOLDS
        IF (CHESTR%R(I).LT.30) THEN
          CHESTR%R(I) = 30
        ELSEIF (CHESTR%R(I).GT.200) THEN
          CHESTR%R(I) = 200
        ENDIF
      ENDDO
      !
      !DO I = 1, NPOIN
        !IF (Y(I)-X(I).GE.6098000.d0) THEN
          !CHESTR%R(I) = 1500*SUM(DEBIT)**(-0.44)
        !ELSEIF (Y(I).GT.6475000) THEN
          !CHESTR%R(I) = 12.83*log(SUM(DEBIT))+9.60
        !ELSE
          !CHESTR%R(I) = 45
        !ENDIF
      !ENDDO
!
!-----------------------------------------------------------------------
!
!3D   DO I = 1 , NPOIN2
!3D     IF(AT.GT.1200.D0) THEN
!3D       RUGOF%R(I) = 40.D0
!3D     ELSE
!3D       RUGOF%R(I) = 60.D0
!3D     ENDIF
!3D   ENDDO
!
!-----------------------------------------------------------------------
!
      RETURN
      END
