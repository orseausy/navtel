!                    *********************************
                     SUBROUTINE SUSPENSION_EROSION_COH
!                    *********************************
!
     &(TAUP,NPOIN,XMVS,PARTHENIADES,ZERO,
     & FLUER,TOCE_VASE,NOMBLAY,DT,MS_VASE)
!
!***********************************************************************
! SISYPHE   V6P2                                   21/07/2011
!***********************************************************************
!
!brief    COMPUTES THE FLUX OF DEPOSITION AND EROSION
!+                ACCOUNTING FOR THE VERTICAL STRUCTURE.
!+
!+            !! NEW SUBROUTINE !!
!
!history  C. VILLARET
!+        31/07/2008
!+        V6P0
!+
!
!history  N.DURAND (HRW), S.E.BOURBAN (HRW)
!+        13/07/2010
!+        V6P0
!+   Translation of French comments within the FORTRAN sources into
!+   English comments
!
!history  N.DURAND (HRW), S.E.BOURBAN (HRW)
!+        21/08/2010
!+        V6P0
!+   Creation of DOXYGEN tags for automated documentation and
!+   cross-referencing of the FORTRAN sources
!
!history  C. VILLARET
!+        22/08/2012
!+        V6P2
!+        Added a number of improvements and modifications
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!| DT             |-->| TIME STEP
!| FLUER          |<->| EROSION RATE
!| MS_VASE        |<->| MASS OF MUD PER LAYER (not modified here)
!| NOMBLAY        |-->| NUMBER OF LAYERS OF THE CONSOLIDATION MODEL
!| NPOIN          |-->| NUMBER OF POINTS
!| PARTHENIADES   |-->| PARTHENIADES CONSTANT (M/S)
!| TAUP           |-->| SKIN FRICTION
!| TOCE_VASE      |-->| CRITICAL BED SHEAR STRESS OF THE MUDPER LAYER
!| XMVS           |-->| DENSITY OF SOLID
!| ZERO           |-->| ZERO
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE INTERFACE_SISYPHE, EX_SUSPENSION_EROSION_COH=>
     &                          SUSPENSION_EROSION_COH
      USE BIEF
      USE DECLARATIONS_SISYPHE, ONLY : NLAYMAX,CONC
	    USE DECLARATIONS_TELEMAC2D, ONLY : X,Y
!
      USE DECLARATIONS_SPECIAL
      IMPLICIT NONE
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER,          INTENT(IN)    :: NOMBLAY
      INTEGER,          INTENT(IN)    :: NPOIN
      DOUBLE PRECISION, INTENT(IN)    :: XMVS
      DOUBLE PRECISION, INTENT(IN)    :: ZERO,PARTHENIADES
      DOUBLE PRECISION,  INTENT(INOUT) :: MS_VASE(NPOIN,NOMBLAY)
      DOUBLE PRECISION, INTENT(IN)     :: TOCE_VASE(NOMBLAY), DT
      TYPE (BIEF_OBJ),  INTENT(INOUT) :: FLUER
      TYPE (BIEF_OBJ),  INTENT(IN)    :: TAUP
!
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER :: I, J
      DOUBLE PRECISION :: AUX, CES,TOCE_VASE2(NPOIN,NOMBLAY)
      DOUBLE PRECISION :: FLUER_LOC(NLAYMAX),MER_VASE,TEMPS
!======================================================================!
!======================================================================!
!                               PROGRAM                                !
!======================================================================!
!======================================================================!
! supprimer VITCE = SQRT (TOCE_VASE(1)), ES
! supprimer XMVE, TASS, DEBUG, GRAV
! rjouter NLAYAX
      ! *************************************************  !
      ! IA - FORMULATION FOR COHESIVE SEDIMENTS            !
      !      (WITHOUT CONSOLIDATION: UNIFORM SEDIMENT BED) !                                   !
      ! ******************************************* *****  !
       CES=0.002
	    	
      IF(NOMBLAY.EQ.1) THEN
        DO I = 1, NPOIN

          IF(TAUP%R(I).GT.TOCE_VASE(1)) THEN
!            AUX = MAX(((USTARP/VITCE)**2 - 1.D0),0.D0)
             AUX=((TAUP%R(I)/MAX(TOCE_VASE(1),1.D-08))-1.D0)
          ELSE
            AUX = 0.D0
          ENDIF
 !         FLUER%R(I) = PARTHENIADES*AUX
 !           CES=(CONC_VASE(1))-75)*(0.003-0.0002)/(507-75)+2.D-4
            FLUER%R(I) = CES*AUX/2650
        ENDDO
      ELSE
      ! **************************************************** !
      ! IB - FORMULATION FOR COHESIVE SEDIMENTS  + CONSOLIDATION !
      !      (WITH BEDLOAD)                                  !
      ! **************************************************** !
!      BEWARE: HERE PARTHENIADES IS IN M/S
        DO I=1,NPOIN
! Calcul des Flux de masse/couche  en Kg/m2/s
          DO J=1,NOMBLAY
		    TOCE_VASE2(I,J)=TOCE_VASE(J)
			   if (J.EQ.20) then
		      if (Y(I).LT.6400000) then
	           TOCE_VASE2(I,J)=100
				endif
		  
		      if (X(I).gt.454000) then
	           TOCE_VASE2(I,J)=100
				endif
			   endif 
            IF(TAUP%R(I).GT.TOCE_VASE2(I,J))THEN
!			    CES=(CONC_VASE(K)-75)*(0.003-0.0002)/(507-75)+2.D-4
!            FLUER%R(I) = CES*AUX/2650
              FLUER_LOC(J)=CES*
     &              *((TAUP%R(I)/MAX(TOCE_VASE2(I,J),1.D-08))-1.D0)
	     
            ELSE
              FLUER_LOC(J)=0.D0
            ENDIF
          ENDDO
!
! MER_VASE: total mass to be potentially eroded
!
          MER_VASE = 0.D0
          TEMPS= DT
!
          DO J= 1, NOMBLAY
            IF(MS_VASE(I,J).GE.1.D-8) THEN
!             COMPUTES THE MASS POTENTIALLY ERODED IN LAYER J (KG/M2)
!              QE_COUCHE = FLUER_LOC(J) *XMVS * TEMPS
              IF(FLUER_LOC(J)*TEMPS.LT.MS_VASE(I,J)) THEN
!			   if (I.EQ.23264) then
!		       print *,J, TAUP%R(I),TOCE_VASE(J), FLUER_LOC(J),MS_VASE(I,J)
!		         endif
                MER_VASE = MER_VASE  + FLUER_LOC(J)*TEMPS
                GO TO 10
              ELSE
                MER_VASE = MER_VASE + MS_VASE(I,J)
!				if (I.EQ.23264) then
!		       print *,'2',J, TAUP%R(I)/TOCE_VASE(J),FLUER_LOC(J),MS_VASE(I,J)
!		         endif
                TEMPS= TEMPS-MS_VASE(I,J)/FLUER_LOC(J)
                TEMPS=MAX(TEMPS,0.D0)
              ENDIF
            ENDIF
          ENDDO
!
!          IF(LNG.EQ.1) THEN
!            WRITE(LU,*) 'ATTENTION TOUTES LES COUCHES SONT VIDES'
!          ENDIF
!          IF(LNG.EQ.2) THEN
!            WRITE(LU,*) 'BEWARE, ALL LAYERS EMPTY'
!          ENDIF
!          CALL PLANTE(1)
!          STOP
10        CONTINUE
!

          FLUER%R(I) = MER_VASE/DT/XMVS
!
        ENDDO
      ENDIF
!
!======================================================================!
!======================================================================!
!
      RETURN
      END
