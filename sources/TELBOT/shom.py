#!/home/$USER/anaconda3/envs/py27/bin python
# -*- coding: utf-8 -*-
"""
@author: Sylvain Orseau (sylvain.orseau@tutanota.com)
"""
##################################################################################
# Import modules
import numpy as np
import pandas as pd
import urllib2


##################################################################################
class DataShom(object):
    def __init__(self, data_url):
        self.url = data_url
        try:
            self.file = urllib2.urlopen(self.url)
            self.scan = list(self.file.read().split("\n"))
            self.status = "COMPLETED"
        except urllib2.URLError as error:
            self.status = "FAILED"
            print error.reason
        self.file.close()
        # -- Variables
        self.header = list()
        self.header_data = list()
        self.header_label = list()
        self.raw_data = None
        self.surge_values = list()
        self.surge_time = list()
        self.variables = list()
        self.variable_id = list()
        self.variable_name = list()

    def extract(self):
        self.header = [row[2:] for row in self.scan if row.startswith("#")]
        self.header_data = [x[x.find(":") + 1:] for x in self.header if x.find(":") != -1]
        self.header_label = [row[:row.find(":") - 1] for row in self.header if row.find(":") != -1]
        self.variable_name = [self.header_data[i] for i in range(len(self.header_label))
                              if "Source" in self.header_label[i]]
        self.variable_id = np.arange(0, len(self.variable_name))
        self.variable_name = [name.title().replace(" ", "") for name in self.variable_name]
        self.raw_data = [x.split(";") for x in self.scan if not (x.startswith("#") and x.strip())]
        self.variables = {var_name: [] for var_name in self.variable_name}
        for row, values in enumerate(self.raw_data):
            if values == ['']:
                pass
            else:
                var_id = int(values[3])
                self.variables[self.variable_name[var_id]].append(values)

    def load_surge(self):
        self.surge_values = [float(self.variables["StormSurge"][i][2])
                             for i in range(len(self.variables["StormSurge"]))]
        self.surge_time = [self.variables["StormSurge"][i][0] for i in range(len(self.variables["StormSurge"]))]
        self.surge_time = pd.to_datetime(self.surge_time, format="%Y-%m-%d %H:%M:%S")


##################################################################################
