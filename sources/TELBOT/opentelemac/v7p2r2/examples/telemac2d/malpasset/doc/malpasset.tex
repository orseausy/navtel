% case name
\chapter{Malpasset}
%
%
\section{Description}
\bigskip
This test illustrates that \telemac{2d} is able to simulate a real 
dam break flow on an initially dry domain.
It also shows the propagation of the wave front and the evolution 
in time of the water surface and velocities in the valley downstream.

\bigskip
This case is the simulation of the propagation of the wave following 
the break of the Malpasset dam (South-East of France). Such accident really 
occurred in December 1959. The model represents the reservoir upstream 
from the dam and the valley and flood plain downstream. The entire valley 
is approximately 18~km long and between 200~m (valley) and 7~km wide (flood plain).
The complete study is described in details in [1].

\bigskip
Note that the simulation is performed using the treatment of negative depths 
introduced since \telemac{2d} 7.0. The historical simulation using the method 
of characteristics (named \sloppy "t2d\_malpasset-small\_charac.cas") has been kept. 
Nevertheless, the recommended advection scheme for velocities for such applications 
is now the NERD scheme (scheme 14) which is the presented cas here
(named "t2d\_malpasset-small\_pos.cas"). 
A simulation using a finer mesh (named "t2d\_malpasset-large") is also performed, 
but the results are not presented. 

\bigskip
The geometry dimensions of the model are around 17~km long and 9~km wide. 
The real bathymetry is shown in the Figure \ref{t2d:malpasset:fig:bathy}.\\
Note also that the horizontal turbulent viscosity is constant 
and equal to 1~$\text{m}^2\cdot\text{s}^{-1}$.
%
\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/bathy.png}
 \caption{Bathymetry of Malpasset domain.}
 \label{t2d:malpasset:fig:bathy}
\end{figure}
%
\section{Initial and boundary conditions}
\bigskip
At initial time, the velocity is null. The reservoir is 
full and  the downstream valley is dry.

\bigskip
The boundary conditions are:
\begin{itemize}
\item All lateral boundaries are solid, i.e., no entrance and no outlet in the domain.
\item At the bottom, Strickler formula with friction coefficient equal 
to 30~$\text{m}^{1/3}.\text{s}^{-1}$ is imposed.
\item No friction on the lateral boundaries.
\end{itemize}
%
\section{Mesh and numerical parameters}
\bigskip
The mesh is regular (see Figure \ref{t2d:malpasset:fig:mesh}). It is refined 
in the river valley (downstream from the dam) and on the banks. It is composed 
of 26,000 triangular elements (13,541 nodes) and the size of triangles ranges between 
17~m and 313~m. The triangular elements types are linear triangles (P1, 3 values 
per element, the corners) for water depth and for velocities.
%
\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/mesh.png}
 \caption{Mesh of Malpasset domain.}
 \label{t2d:malpasset:fig:mesh}
\end{figure}
%
\bigskip
The time step is 4~s  for a simulate period of 4,000~s. The resolution accuracy for 
the velocity is fixed at $10^{-4}$.

\bigskip
Note that for numerical resolution, the conjugate gradient is used for solving 
the propagation step (option 1). To solve advection, the NERD scheme is used 
for the velocities (scheme 14) with the treatment of negative
depths for tidal flats and the conservative scheme is used for the depth (scheme 5). 
In addition, the treatment of linear system is done with a wave equation. 
To finish, the implicitation coefficients for depth and velocities are
equal to 1.

\bigskip
Note that the finer mesh is also regular and it is composed of 104,000 triangular 
elements (53,081 nodes) and the size of triangles 
ranges between 8.5~m and 156.5~m. The triangular elements types are linear triangles 
(P1, 3 values per element, the corners) for water depth and for velocities.
The time step is 1 s due to the mesh. The free surface gradient compatibility is equal 
to 0.9 and the implication for velocity is equal to 1. The other parameters 
are similar to those of the previous test.

\bigskip
Other advection schemes for the velocities are also tested but the results are not 
presented here. These other schemes are:
\begin{itemize}
\item the ERIA scheme ("t2d\_malpasset-small\_ERIA.cas", scheme 15),
\item the historical method of characteristics 
("t2d\_malpasset-small\_charac.cas", scheme 1),
\item the 1$^{\rm{st}}$ Order Kinetic scheme 
("t2d\_malpasset-small\_cin.cas", finite volume method with the scheme 5),
\item the coupled primitive equations 
("t2d\_malpasset-small\_prim.cas", scheme 1 with option 1 for the treatment of linear system).
\end{itemize}
The time step is normally 4 s for these advection test cases except for 
the tests with the 1$^{\rm{st}}$ Order Kinetic (1 s) and for the using of the 
primitive equations (0.5 s). Some other parameters may vary between these tests. 
%
\section{Results}
%
Figure \ref{t2d:malpasset:fig:depth} illustrates the progression of the flood 
wave after the dam break (the simulation using the treatment of negative depths smooths
the results on tidal flats). The propagation of the wave front is very fast. 
The water depths increase rapidly in the valley downstream from the dam location. 
The wave spreads in the plain before the sea. During the simulation, 
no negative water depths are observed.

\bigskip
Figure \ref{t2d:malpasset:fig:velo} represents the velocity patterns at successive 
times as given by the computation. Maximum velocities are close to 14 $\text{m}\cdot\text{s}^{-1}$ 
in sharp and narrow meanders of the river valley in which the dam break wave propagates. 
Water mass balance shows that the mass conservation is very good: the relative error cumulated 
on volume is $0.308\cdot 10^{-14}$. % 0.3080188E-14
A complete comparison between simulation results produced by \telemac{2d} and in-situ data available 
collected immediately after the catastrophe has been reported by Hervouet in [1].
The tables \ref{t2d:malpasset:tab:zmax} and \ref{t2d:malpasset:tab:onde} show the comparison between 
the numerical model and the physical model for maximum water level and for the propagation 
time of a dam break wave. The \telemac{2d} results are close to these measured.

\bigskip
\telemac{2d} is so capable to simulate the propagation of a dam break wave 
in a river valley initially dry.
%
\begin{figure}[H]
 \begin{center}
 \includegraphicsmaybe{[width=0.73\textwidth]}{../img/figure_depth0s.png}\\
 \includegraphicsmaybe{[width=0.73\textwidth]}{../img/figure_depth200s.png}\\
 \includegraphicsmaybe{[width=0.73\textwidth]}{../img/figure_depth400s.png}\\
 \includegraphicsmaybe{[width=0.73\textwidth]}{../img/figure_depth600s.png}
  \end{center}
 \caption{Evolution of the water depth in time.}
 \label{t2d:malpasset:fig:depth}
\end{figure}
\begin{figure}[H]
 \begin{center}
 \includegraphicsmaybe{[width=0.73\textwidth]}{../img/figure_velocity0s.png}\\
 \includegraphicsmaybe{[width=0.73\textwidth]}{../img/figure_velocity200s.png}\\
 \includegraphicsmaybe{[width=0.73\textwidth]}{../img/figure_velocity400s.png}\\
 \includegraphicsmaybe{[width=0.73\textwidth]}{../img/figure_velocity600s.png}
 \end{center}
 \caption{Evolution of the velocity field in time.}
 \label{t2d:malpasset:fig:velo}
\end{figure}

\begin{table}[H]

\centering
\caption{Maximum water level (Z$_{\text{s Max}}$) on physical model and 2D numerical 
results with Strickler coefficient of 30~$\text{m}^{1/3}.\text{s}^{-1}$}
\begin{tabular}{l|}
  Measurement Points \\ \hline 6 (4947~m, 4289~m )\\ 7 (5717~m, 4407~m)\\
   8 (6775~m, 3869~m)\\ 9 (7128~m, 3162~m)\\ 10 (8585~m, 3443~m)\\
    11 (9674~m, 3085~m)\\ 12 (10939~m, 3044~m)\\ 13 (11724~m,2810~m)\\ 14 (12723~m, 2485~m)
\end{tabular}%
\begin{tabular}{c|}
 Z$_{\text{s Max}}$ of physical model (m)  \\\hline 84.2\\ 49.1\\ 54.0\\ 40.2\\
 34.9\\ 27.4\\ 21.5\\ 16.1\\ 12.9
\end{tabular}%
\begin{tabular}{c}
    Z$_{\text{s Max}}$ of \telemac{2d} (m)\\
\hline
\InputIfFileExists{../img/measurement.txt}{}{}
\end{tabular}
\label{t2d:malpasset:tab:zmax}
\end{table}

\begin{table}[H]
\centering
\caption{Propagation time of a dam break wave to point A  (5550~m, 4400~m), 
B (11900~m, 3250~m) and C (13000~m, 2700~m). Note that the arrival time 
at A for the nature case is approximate due to the break time of the dam.}
\begin{tabular}{l|}
   \hline Nature \\ \telemac{2d} 
\end{tabular}%
\begin{tabular}{c|c|c}
 Arrival time at A (s)  & Time from A to B (s) & Time from A to C (s) \\
\hline
100  & 1140 & 1320 \\
\InputIfFileExists{../img/arrival.txt}{}{}

\end{tabular}
\label{t2d:malpasset:tab:onde}
\end{table}
\section{Reference}
%
[1] Hydrodynamics of Free Surface Flows modelling with the finite
element method. Jean-Michel Hervouet (Wiley, 2007) pp. 281-288.
%
%
