!                    **************************
                     SUBROUTINE UTIMP_TELEMAC2D
!                    **************************
!
     &(LTL,ATL,GRADEBL,GRAPRDL,LISDEBL,LISPRDL)
!
!***********************************************************************
! TELEMAC2D   V6P1                                   21/08/2010
!***********************************************************************
!
!brief    WRITES OUT ADDITIONAL OUTPUT REQUIRED BY THE USER.
!
!note     THIS SUBROUTINE IS CALLED IN THE SAME PLACES AS THE
!+                MAIN TELEMAC2D OUTPUT SUBROUTINE (NAMED DESIMP),
!+                I.E. CALLED TWICE:
!+
!note   (1) ONCE PER RUN, WHEN LTL==0, INDEPENDENTLY OF WHETHER
!+             'OUTPUT OF INITIAL CONDITIONS : YES' IS SET OR NOT
!note   (2) EACH TIME STEP JUST AFTER DESIMP-OUTPUT
!
!warning  USER SUBROUTINE; DOES NOTHING BY DEFAULT
!
!history  JACEK A. JANKOWSKI PINXIT, BAW KARLSRUHE, JACEK.JANKOWSKI@BAW.DE
!+        **/08/2003
!+        V5P4
!+
!
!history  N.DURAND (HRW), S.E.BOURBAN (HRW)
!+        13/07/2010
!+        V6P0
!+   Translation of French comments within the FORTRAN sources into
!+   English comments
!
!history  N.DURAND (HRW), S.E.BOURBAN (HRW)
!+        21/08/2010
!+        V6P0
!+   Creation of DOXYGEN tags for automated documentation and
!+   cross-referencing of the FORTRAN sources
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!| ATL            |-->| TIME OF TIME STEP, IN SECONDS
!| GRADEBL        |-->| FIRST TIME STEP FOR GRAPHIC OUTPUTS
!| GRAPRDL        |-->| PERIOD OF GRAPHIC OUTPUTS
!| LISDEBL        |-->| FIRST TIME STEP FOR LISTING OUTPUTS
!| LISPRDL        |-->| PERIOD OF LISTING OUTPUTS
!| LTL            |-->| CURRENT TIME STEP
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE BIEF
      USE DECLARATIONS_TELEMAC
      USE DECLARATIONS_TELEMAC2D
! DAJ
! OPEN VARIABLES RELATED TO ALGAE TRANSPORT
      USE ALGAE_TRANSP
! FAJ
!
      USE DECLARATIONS_SPECIAL
      IMPLICIT NONE
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      DOUBLE PRECISION, INTENT(IN) :: ATL
      INTEGER, INTENT(IN) :: LTL,GRADEBL,GRAPRDL,LISDEBL,LISPRDL
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
!
!***********************************************************************
! USER OUTPUT

! DAJ
! RECORD FLUID VELOCITIES AT EVERY GRAPHIC PRINTOUT PERIOD
      IF(MOD(LT,LEOPRD).EQ.0)THEN
        CALL ENR_FLU(MESH%IKLE%I,3)
      END IF


! RECORD VELOCITY PROFILES
      CALL MESURES_PROF_VIT(NCSIZE,IPID,NFLOT_MAX,NFLOT,XFLOT%R,
     &                YFLOT%R,DX_A%R,DY_A%R,V_X%R,V_Y%R,V_X_0%R,
     &                V_Y_0%R,U_X%R,U_Y%R,U_X_0%R,U_Y_0%R)

! RECORD PARTICLE POSITION
      CALL MESURES_ALG(NFLOT_MAX,NFLOT,XFLOT%R,YFLOT%R,TAGFLO%I,LT,
     &       DT,DT,NCSIZE,IPID)


!       IF(MOD(LT,FLOPRD).EQ.0)THEN
! ! RECORD VELOCITY PROFILES
!         CALL MESURES_PROF_VIT(NCSIZE,IPID,NFLOT_MAX,NFLOT,XFLOT%R,
!      &                YFLOT%R,DX_A%R,DY_A%R,V_X%R,V_Y%R,V_X_0%R,
!      &                V_Y_0%R,U_X%R,U_Y%R,U_X_0%R,U_Y_0%R)
! 
! ! RECORD PARTICLE POSITION
!         CALL MESURES_ALG(NFLOT_MAX,NFLOT,XFLOT%R,YFLOT%R,TAGFLO%I,LT,
!      &       DT*REAL(FLOPRD),DT,NCSIZE,IPID)
!       END IF



! FAJ


!
!
!
!-----------------------------------------------------------------------
!
      RETURN
      END SUBROUTINE UTIMP_TELEMAC2D
