      MODULE MESURES_POSITIONS
C**********************************************************************
C DECLARATION OF VARIABLES USED TO RECORD RESULTS
C**********************************************************************
C PARAMETRES USED TO DESCRIBE THE RECORDED CASE
      INTEGER,PARAMETER  :: MESURE=2
      INTEGER,PARAMETER  :: LACHE=2
      INTEGER,PARAMETER  :: VIT=1
C TO DEFINE THE INITIAL PARTICLE POSITION
      DOUBLE PRECISION:: X_INI
      DOUBLE PRECISION:: Y_INI
C RESULT FILES
      CHARACTER (LEN=50) :: F_MES_ALG
      CHARACTER (LEN=50) :: F_PROF_VIT
      LOGICAL :: INIT_MES
      DATA INIT_MES /.TRUE./
      LOGICAL :: INIT_PROF
      DATA INIT_PROF /.TRUE./
C POSITIONS OF THE QUADRANTS
      DOUBLE PRECISION:: X_HD_1
      DOUBLE PRECISION:: X_HD_2
      DOUBLE PRECISION:: Y_HD_1
      DOUBLE PRECISION:: Y_HD_2
C
      DOUBLE PRECISION:: X_BD_1
      DOUBLE PRECISION:: X_BD_2
      DOUBLE PRECISION:: Y_BD_1
      DOUBLE PRECISION:: Y_BD_2
C
      DOUBLE PRECISION:: X_HG_1
      DOUBLE PRECISION:: X_HG_2
      DOUBLE PRECISION:: Y_HG_1
      DOUBLE PRECISION:: Y_HG_2
C
      DOUBLE PRECISION:: X_BG_1
      DOUBLE PRECISION:: X_BG_2
      DOUBLE PRECISION:: Y_BG_1
      DOUBLE PRECISION:: Y_BG_2
C RECORDED VARIABLES IN EACH QUADRANT
      INTEGER:: N_HD
      INTEGER:: N_BD
      INTEGER:: N_HG
      INTEGER:: N_BG
      INTEGER,DIMENSION(:,:),ALLOCATABLE:: PRESENCE
      DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE:: RESID
      DOUBLE PRECISION:: T_START
      DOUBLE PRECISION:: T_END
      INTEGER:: N_TOT
      LOGICAL:: YES_START
C VARIABLES USED TO RECORD VELOCITY PROFILES
      DOUBLE PRECISION                   ::X_PROF
      INTEGER         ,PARAMETER         ::NY=10
      DOUBLE PRECISION                   ::Y_PROF(NY)
      DOUBLE PRECISION                   ::DY_PROF
      INTEGER                            ::N_PROF(NY)
      DOUBLE PRECISION                   ::UX_PROF(NY)
      DOUBLE PRECISION                   ::UY_PROF(NY)
      DOUBLE PRECISION                   ::VX_PROF(NY)
      DOUBLE PRECISION                   ::VY_PROF(NY)
C VARIABLES USED TO WRITE THE RESULTS
      INTEGER                            ::N_WRI(NY)
      DOUBLE PRECISION                   ::UX_WRI(NY)
      DOUBLE PRECISION                   ::UY_WRI(NY)
      DOUBLE PRECISION                   ::VX_WRI(NY)
      DOUBLE PRECISION                   ::VY_WRI(NY)
      END MODULE

C                       **********************
                        SUBROUTINE MESURES_ALG
C                       **********************
C
     &(NP_TOT,N_A,X_A,Y_A,TAG,LT,DT,DTREAL,NCSIZE,IPID)
C
C***********************************************************************
C  ANTOINE JOLY
C
C***********************************************************************
C
C SUBROUTINE USED TO RECORD PARTICLE POSITIONS
C
C-----------------------------------------------------------------------
C                             ARGUMENTS
C .________________.____.______________________________________________.
C |      NOM       |MODE|                   ROLE                       |
C |________________|____|______________________________________________|
C |________________|____|______________________________________________|
C MODE : -->(DONNEE NON MODIFIEE), <--(RESULTAT), <-->(DONNEE MODIFIEE)
C
C-----------------------------------------------------------------------
C
C APPELE PAR : TELMAC
C
C SOUS-PROGRAMME APPELE : NEANT
C
C***********************************************************************
C
      USE MESURES_POSITIONS
C

      IMPLICIT NONE
C INPUT VARIABLES
      INTEGER         ,INTENT(IN)     :: NP_TOT,N_A,NCSIZE,IPID
      DOUBLE PRECISION,INTENT(IN)     :: X_A(NP_TOT)
      DOUBLE PRECISION,INTENT(IN)     :: Y_A(NP_TOT)
      INTEGER         ,INTENT(IN)     :: TAG(NP_TOT)
      INTEGER         ,INTENT(IN)     :: LT
      DOUBLE PRECISION,INTENT(IN)     :: DT
      DOUBLE PRECISION,INTENT(IN)     :: DTREAL
C VARIABLES FOR LOOPS
      INTEGER                         :: I_A
      INTEGER                         :: I_Q
C VARIABLES USED TO WRITE RESULT FILES
      DOUBLE PRECISION:: T_START_W
      DOUBLE PRECISION:: T_END_W
      INTEGER:: N_TOT_W
      INTEGER:: N_HD_W
      INTEGER:: N_BD_W
      INTEGER:: N_HG_W
      INTEGER:: N_BG_W
      DOUBLE PRECISION:: RESID_W(4)
      DOUBLE PRECISION:: X_DEC(10),Y_DEC(10)
      DOUBLE PRECISION:: N_HD_N
      DOUBLE PRECISION:: N_BD_N
      DOUBLE PRECISION:: N_HG_N
      DOUBLE PRECISION:: N_BG_N
      DOUBLE PRECISION:: RSD_HD_N
      DOUBLE PRECISION:: RSD_BD_N
      DOUBLE PRECISION:: RSD_HG_N
      DOUBLE PRECISION:: RSD_BG_N
      INTEGER:: SUM_N
      DOUBLE PRECISION:: SUM_RSD
      DOUBLE PRECISION:: MAX_N
      DOUBLE PRECISION:: MAX_RSD
C VARIABLES USED IN PARALLEL
      INTEGER  P_ISUM,P_IMAX
      DOUBLE PRECISION P_DSUM,P_DMIN,P_DMAX
      EXTERNAL P_ISUM,P_IMAX,P_DSUM,P_DMIN,P_DMAX

C
C-----------------------------------------------------------------------
C DEFINE THE QUARTERS
C-----------------------------------------------------------------------
C AT THE FIRST TIME STEP
      IF(INIT_MES)THEN
        INIT_MES=.FALSE.
C DEFINE RELEASE POINT
        IF(LACHE.EQ.1)THEN
          X_INI=-1.3D0
          Y_INI=0.19D0
        ELSEIF(LACHE.EQ.2)THEN
          X_INI=0.175D0
          Y_INI=0.45D0
        ELSEIF(LACHE.EQ.3)THEN
          X_INI=-0.19D0
          Y_INI=0.25D0
        ELSEIF(LACHE.EQ.4)THEN
          X_INI=0.98D0
          Y_INI=0.55D0
        END IF
C DEFINE THE WINDOW OF MEASUREMENT
        IF(MESURE.EQ.1)THEN
          X_HG_1=-1.05D0
          X_HG_2=-0.55D0
          Y_HG_1=0.5D0
          Y_HG_2=1.0D0
C
          X_BG_1=-1.05D0
          X_BG_2=-0.55D0
          Y_BG_1=0.D0
          Y_BG_2=0.5D0
C
          X_HD_1=-0.55D0
          X_HD_2=-0.05D0
          Y_HD_1=0.5D0
          Y_HD_2=1.0D0
C
          X_BD_1=-0.55D0
          X_BD_2=-0.05D0
          Y_BD_1=0.D0
          Y_BD_2=0.5D0
C
        ELSEIF(MESURE.EQ.2)THEN
          X_HG_1=0.225D0
          X_HG_2=0.725D0
          Y_HG_1=0.5D0
          Y_HG_2=1.0D0
C
          X_BG_1=0.225D0
          X_BG_2=0.725D0
          Y_BG_1=0.D0
          Y_BG_2=0.5D0
C
          X_HD_1=0.725D0
          X_HD_2=1.225D0
          Y_HD_1=0.5D0
          Y_HD_2=1.0D0
C
          X_BD_1=0.725D0
          X_BD_2=1.225D0
          Y_BD_1=0.D0
          Y_BD_2=0.5D0
C
        ELSEIF(MESURE.EQ.3)THEN
          X_HG_1=1.4D0
          X_HG_2=1.9D0
          Y_HG_1=0.5D0
          Y_HG_2=1.0D0
C
          X_BG_1=1.4D0
          X_BG_2=1.9D0
          Y_BG_1=0.D0
          Y_BG_2=0.5D0
C
          X_HD_1=1.9D0
          X_HD_2=2.4D0
          Y_HD_1=0.5D0
          Y_HD_2=1.0D0
C
          X_BD_1=1.9D0
          X_BD_2=2.4D0
          Y_BD_1=0.D0
          Y_BD_2=0.5D0
        END IF
C OPEN RESULT FILE
        IF(MESURE.EQ.1.AND.LACHE.EQ.1)THEN
          F_MES_ALG='../mesure1_lache1_Tel2d.txt'
        ELSEIF(MESURE.EQ.2.AND.LACHE.EQ.2)THEN
          F_MES_ALG='../mesure2_lache2_Tel2d.txt'
        ELSEIF(MESURE.EQ.2.AND.LACHE.EQ.3)THEN
          F_MES_ALG='../mesure2_lache3_Tel2d.txt'
        ELSEIF(MESURE.EQ.3.AND.LACHE.EQ.2)THEN
          F_MES_ALG='../mesure3_lache2_Tel2d.txt'
        ELSEIF(MESURE.EQ.3.AND.LACHE.EQ.4)THEN
          F_MES_ALG='../mesure3_lache4_Tel2d.txt'
        END IF
C INITIALISE THE VARIABLES
        N_HD=0
        N_BD=0
        N_HG=0
        N_BG=0
        IF(ALLOCATED(PRESENCE))DEALLOCATE(PRESENCE)
        ALLOCATE(PRESENCE(NP_TOT,4))
        IF(ALLOCATED(RESID))DEALLOCATE(RESID)
        ALLOCATE(RESID(4))
        DO I_A=1,NP_TOT
          DO I_Q=1,4
!             PRESENCE(I_A,I_Q)=.FALSE.
            PRESENCE(I_A,I_Q)=0
          END DO
        END DO
        DO I_Q=1,4
          RESID(I_Q)=0.D0
        END DO
C
        YES_START=.TRUE.
        T_START=1.D10
        T_END=0.D0
      END IF
C-----------------------------------------------------------------------
C CALCULATE THE VARIABLES FOR EACH QUARTER
C-----------------------------------------------------------------------
C
      N_TOT=0
C
      DO I_A=1,N_A
        IF(X_A(I_A).GE.X_HD_1.AND.X_A(I_A).LE.X_HD_2.AND.
     &     Y_A(I_A).GE.Y_HD_1.AND.Y_A(I_A).LE.Y_HD_2)THEN
          IF(PRESENCE(TAG(I_A),1).EQ.0)THEN
            N_HD=N_HD+1
            PRESENCE(TAG(I_A),1)=PRESENCE(TAG(I_A),1)+1
          ELSE
            RESID(1)=RESID(1)+DT
          END IF
          IF(YES_START)THEN
            T_START=LT*DTREAL
            YES_START=.FALSE.
          END IF
          IF(LT*DTREAL.GT.T_END)T_END=LT*DTREAL
        END IF
C
        IF(X_A(I_A).GE.X_BD_1.AND.X_A(I_A).LE.X_BD_2.AND.
     &     Y_A(I_A).GE.Y_BD_1.AND.Y_A(I_A).LE.Y_BD_2)THEN
          IF(PRESENCE(TAG(I_A),2).EQ.0)THEN
            N_BD=N_BD+1
            PRESENCE(TAG(I_A),2)=PRESENCE(TAG(I_A),2)+1
          ELSE
            RESID(2)=RESID(2)+DT
          END IF
          IF(YES_START)THEN
            T_START=LT*DTREAL
            YES_START=.FALSE.
          END IF
          IF(LT*DTREAL.GT.T_END)T_END=LT*DTREAL
        END IF
C
        IF(X_A(I_A).GE.X_HG_1.AND.X_A(I_A).LE.X_HG_2.AND.
     &     Y_A(I_A).GE.Y_HG_1.AND.Y_A(I_A).LE.Y_HG_2)THEN
          IF(PRESENCE(TAG(I_A),3).EQ.0)THEN
            N_HG=N_HG+1
            PRESENCE(TAG(I_A),3)=PRESENCE(TAG(I_A),3)+1
          ELSE
            RESID(3)=RESID(3)+DT
          END IF
          IF(YES_START)THEN
            T_START=LT*DTREAL
            YES_START=.FALSE.
          END IF
          IF(LT*DTREAL.GT.T_END)T_END=LT*DTREAL
        END IF
C
        IF(X_A(I_A).GE.X_BG_1.AND.X_A(I_A).LE.X_BG_2.AND.
     &     Y_A(I_A).GE.Y_BG_1.AND.Y_A(I_A).LE.Y_BG_2)THEN
          IF(PRESENCE(TAG(I_A),4).EQ.0)THEN
            N_BG=N_BG+1
            PRESENCE(TAG(I_A),4)=PRESENCE(TAG(I_A),4)+1
          ELSE
            RESID(4)=RESID(4)+DT
          END IF
          IF(YES_START)THEN
            T_START=LT*DTREAL
            YES_START=.FALSE.
          END IF
          IF(LT*DTREAL.GT.T_END)T_END=LT*DTREAL
        END IF
      END DO
C
C-----------------------------------------------------------------------
C COMMUNICATE RESULTS FOUND IN EACH PROCESSOR
C-----------------------------------------------------------------------
C
      IF(NCSIZE.GT.0)THEN
        DO I_A=1,NP_TOT
          PRESENCE(I_A,1)=P_IMAX(PRESENCE(I_A,1))
          PRESENCE(I_A,2)=P_IMAX(PRESENCE(I_A,2))
          PRESENCE(I_A,3)=P_IMAX(PRESENCE(I_A,3))
          PRESENCE(I_A,4)=P_IMAX(PRESENCE(I_A,4))
C
          IF(PRESENCE(I_A,1).GT.0.OR.PRESENCE(I_A,2).GT.0.OR.
     &       PRESENCE(I_A,3).GT.0.OR.PRESENCE(I_A,4).GT.0)THEN
            N_TOT=N_TOT+1
          END IF
        END DO
      ELSE
        DO I_A=1,NP_TOT
          IF(PRESENCE(I_A,1).GT.0.OR.PRESENCE(I_A,2).GT.0.OR.
     &       PRESENCE(I_A,3).GT.0.OR.PRESENCE(I_A,4).GT.0)THEN
            N_TOT=N_TOT+1
          END IF
        END DO
      END IF
!
C-----------------------------------------------------------------------
C CALCULATE THE VARIABLES USED FOR THE RESULT FILE
C-----------------------------------------------------------------------
C IN PARRALLEL
      IF(NCSIZE.GT.0)THEN
        T_START_W=P_DMIN(T_START)
        T_END_W=P_DMAX(T_END)
        N_TOT_W=N_TOT
        N_HD_W=P_ISUM(N_HD)
        N_BD_W=P_ISUM(N_BD)
        N_HG_W=P_ISUM(N_HG)
        N_BG_W=P_ISUM(N_BG)
        RESID_W(1)=P_DSUM(RESID(1))
        RESID_W(2)=P_DSUM(RESID(2))
        RESID_W(3)=P_DSUM(RESID(3))
        RESID_W(4)=P_DSUM(RESID(4))

        IF(IPID.EQ.0)THEN
C DEFINE THE COORDINATES OF THE QUARTERS
          X_DEC(1)=X_BG_1
          Y_DEC(1)=Y_BG_1
          X_DEC(2)=X_BD_2
          Y_DEC(2)=Y_BD_1
          X_DEC(3)=X_HD_2
          Y_DEC(3)=Y_HD_2
          X_DEC(4)=X_HG_1
          Y_DEC(4)=Y_HG_2
          X_DEC(5)=X_BG_1
          Y_DEC(5)=Y_BG_1
          X_DEC(6)=X_BG_2
          Y_DEC(6)=Y_BG_1
          X_DEC(7)=X_HG_2
          Y_DEC(7)=Y_HG_2
          X_DEC(8)=X_HG_1
          Y_DEC(8)=Y_HG_2
          X_DEC(9)=X_HG_1
          Y_DEC(9)=Y_HG_1
          X_DEC(10)=X_HD_2
          Y_DEC(10)=Y_HD_1
C CALCULATE THE SUMS
          SUM_N=N_HD_W+N_BD_W+N_HG_W+N_BG_W
          SUM_RSD=RESID_W(1)+RESID_W(2)+RESID_W(3)+RESID_W(4)
C CALCULATE THE NON-DIMENSIONNAL PROPORTION AND TIME OF RESIDENCE
          IF(SUM_N.NE.0)THEN
            N_BG_N=REAL(N_BG_W)/REAL(SUM_N)
            N_HG_N=REAL(N_HG_W)/REAL(SUM_N)
            N_HD_N=REAL(N_HD_W)/REAL(SUM_N)
            N_BD_N=REAL(N_BD_W)/REAL(SUM_N)
          ELSE
            N_BG_N=0.D0
            N_HG_N=0.D0
            N_HD_N=0.D0
            N_BD_N=0.D0
          END IF

          IF(SUM_RSD.NE.0)THEN
            IF(N_BG_N.GT.0.D0)THEN
              RSD_BG_N=RESID_W(4)/SUM_RSD/N_BG_N
            ELSE
              RSD_BG_N=0.D0
            ENDIF
            IF(N_HG_N.GT.0.D0)THEN
              RSD_HG_N=RESID_W(3)/SUM_RSD/N_HG_N
            ELSE
              RSD_HG_N=0.D0
            ENDIF
            IF(N_HD_N.GT.0.D0)THEN
              RSD_HD_N=RESID_W(1)/SUM_RSD/N_HD_N
            ELSE
              RSD_HD_N=0.D0
            ENDIF
            IF(N_BD_N.GT.0.D0)THEN
              RSD_BD_N=RESID_W(2)/SUM_RSD/N_BD_N
            ELSE
              RSD_BD_N=0.D0
            ENDIF
          ELSE
            RSD_BG_N=0.D0
            RSD_HG_N=0.D0
            RSD_HD_N=0.D0
            RSD_BD_N=0.D0
          END IF
        END IF
C IN SEQUENTIAL
      ELSE
C DEFINE THE COORDINATES OF THE QUARTERS
        X_DEC(1)=X_BG_1
        Y_DEC(1)=Y_BG_1
        X_DEC(2)=X_BD_2
        Y_DEC(2)=Y_BD_1
        X_DEC(3)=X_HD_2
        Y_DEC(3)=Y_HD_2
        X_DEC(4)=X_HG_1
        Y_DEC(4)=Y_HG_2
        X_DEC(5)=X_BG_1
        Y_DEC(5)=Y_BG_1
        X_DEC(6)=X_BG_2
        Y_DEC(6)=Y_BG_1
        X_DEC(7)=X_HG_2
        Y_DEC(7)=Y_HG_2
        X_DEC(8)=X_HG_1
        Y_DEC(8)=Y_HG_2
        X_DEC(9)=X_HG_1
        Y_DEC(9)=Y_HG_1
        X_DEC(10)=X_HD_2
        Y_DEC(10)=Y_HD_1
C CALCULATE THE SUMS
        SUM_N=N_HD+N_BD+N_HG+N_BG
        SUM_RSD=RESID(1)+RESID(2)+RESID(3)+RESID(4)
C CALCULATE THE NON-DIMENSIONNAL PROPORTION AND TIME OF RESIDENCE
        IF(SUM_N.NE.0)THEN
          N_BG_N=REAL(N_BG)/REAL(SUM_N)
          N_HG_N=REAL(N_HG)/REAL(SUM_N)
          N_HD_N=REAL(N_HD)/REAL(SUM_N)
          N_BD_N=REAL(N_BD)/REAL(SUM_N)
        ELSE
          N_BG_N=0.D0
          N_HG_N=0.D0
          N_HD_N=0.D0
          N_BD_N=0.D0
        END IF

        IF(SUM_RSD.NE.0)THEN
          IF(N_BG_N.GT.0.D0)THEN
            RSD_BG_N=RESID(4)/SUM_RSD/N_BG_N
          ELSE
            RSD_BG_N=0.D0
          ENDIF
          IF(N_HG_N.GT.0.D0)THEN
            RSD_HG_N=RESID(3)/SUM_RSD/N_HG_N
          ELSE
            RSD_HG_N=0.D0
          ENDIF
          IF(N_HD_N.GT.0.D0)THEN
            RSD_HD_N=RESID(1)/SUM_RSD/N_HD_N
          ELSE
            RSD_HD_N=0.D0
          ENDIF
          IF(N_BD_N.GT.0.D0)THEN
            RSD_BD_N=RESID(2)/SUM_RSD/N_BD_N
          ELSE
            RSD_BD_N=0.D0
          ENDIF
        ELSE
          RSD_BG_N=0.D0
          RSD_HG_N=0.D0
          RSD_HD_N=0.D0
          RSD_BD_N=0.D0
        END IF
      END IF
C-----------------------------------------------------------------------
! WRITE RESULTS
C-----------------------------------------------------------------------
      IF(IPID.EQ.0)THEN
C DEFINE THE SCALLING FACTORS
          MAX_N=0.5D0
          MAX_RSD=2.5D0
C
 9401 FORMAT(A,1X,A,1X,A,1X,A,1X,A,1X,A,1X,A,1X,A)
        OPEN(9994,FILE=F_MES_ALG)
        WRITE(9994,'(A)')'# canal_chatou.f'
        WRITE(9994,'(A)')'# Maximum used to rescale values:'
        WRITE(9994,'(A,F3.1)')'# Max N_part = ',MAX_N
        WRITE(9994,'(A,F3.1)')'# Max t_resid = ',MAX_RSD
        WRITE(9994,'(A)')'# Maximum calculated for this case:'
        WRITE(9994,'(A,F5.3)')'# Max N_part = ',
     *        MAX(N_BG_N,N_HG_N,N_HD_N,N_BD_N)
        WRITE(9994,'(A,F5.3)')'# Max t_resid = ',
     *        MAX(RSD_BG_N,RSD_HG_N,RSD_HD_N,RSD_BD_N)
C 
        WRITE(9994,9401) 'x_decoup','y_decoup','x_np',
     &        'y_np','x_rsd','y_rsd','x_rel','y_rel'
        WRITE(9994,'(8F12.5)') X_DEC(1),Y_DEC(1),
     &      X_BG_2-N_BG_N/MAX_N*(X_BG_2-X_BG_1),
     &      Y_BG_2-N_BG_N/MAX_N*(Y_BG_2-Y_BG_1),
     &      X_BG_2-RSD_BG_N/MAX_RSD*(X_BG_2-X_BG_1),
     &      Y_BG_2-RSD_BG_N/MAX_RSD*(Y_BG_2-Y_BG_1),
     &      X_INI,Y_INI
        WRITE(9994,'(6F12.5)') X_DEC(2),Y_DEC(2),
     &      X_HG_2-N_HG_N/MAX_N*(X_HG_2-X_HG_1),
     &      Y_HG_1+N_HG_N/MAX_N*(Y_HG_2-Y_HG_1),
     &      X_HG_2-RSD_HG_N/MAX_RSD*(X_HG_2-X_HG_1),
     &      Y_HG_1+RSD_HG_N/MAX_RSD*(Y_HG_2-Y_HG_1)
        WRITE(9994,'(6F12.5)') X_DEC(3),Y_DEC(3),
     &      X_HD_1+N_HD_N/MAX_N*(X_HD_2-X_HD_1),
     &      Y_HD_1+N_HD_N/MAX_N*(Y_HD_2-Y_HD_1),
     &      X_HD_1+RSD_HD_N/MAX_RSD*(X_HD_2-X_HD_1),
     &      Y_HD_1+RSD_HD_N/MAX_RSD*(Y_HD_2-Y_HD_1)
        WRITE(9994,'(6F12.5)') X_DEC(4),Y_DEC(4),
     &      X_BD_1+N_BD_N/MAX_N*(X_BD_2-X_BD_1),
     &      Y_BD_2-N_BD_N/MAX_N*(Y_BD_2-Y_BD_1),
     &      X_BD_1+RSD_BD_N/MAX_RSD*(X_BD_2-X_BD_1),
     &      Y_BD_2-RSD_BD_N/MAX_RSD*(Y_BD_2-Y_BD_1)
        WRITE(9994,'(6F12.5)') X_DEC(5),Y_DEC(5),
     &      X_BG_2-N_BG_N/MAX_N*(X_BG_2-X_BG_1),
     &      Y_BG_2-N_BG_N/MAX_N*(Y_BG_2-Y_BG_1),
     &      X_BG_2-RSD_BG_N/MAX_RSD*(X_BG_2-X_BG_1),
     &      Y_BG_2-RSD_BG_N/MAX_RSD*(Y_BG_2-Y_BG_1)
        WRITE(9994,'(2F12.5)') X_DEC(6),Y_DEC(6)
        WRITE(9994,'(2F12.5)') X_DEC(7),Y_DEC(7)
        WRITE(9994,'(2F12.5)') X_DEC(8),Y_DEC(8)
        WRITE(9994,'(2F12.5)') X_DEC(9),Y_DEC(9)
        WRITE(9994,'(2F12.5)') X_DEC(10),Y_DEC(10)
        CLOSE(9994)
      END IF
C
      RETURN
      END SUBROUTINE MESURES_ALG


C                       ***************************
                        SUBROUTINE MESURES_PROF_VIT
C                       ***************************
C
     &(NCSIZE,IPID,NP_TOT,N_A,X_A,Y_A,DX,DY,V_X,V_Y,V_X_0,V_Y_0,U_X,
     & U_Y,U_X_0,U_Y_0)
C
C***********************************************************************
C  ANTOINE JOLY
C
C***********************************************************************
C
C SUBROUTINE USED TO RECORD THE PARTICLE VELOCITY PROFILES
C
C-----------------------------------------------------------------------
C                             ARGUMENTS
C .________________.____.______________________________________________.
C |      NOM       |MODE|                   ROLE                       |
C |________________|____|______________________________________________|
C |________________|____|______________________________________________|
C MODE : -->(DONNEE NON MODIFIEE), <--(RESULTAT), <-->(DONNEE MODIFIEE)
C
C-----------------------------------------------------------------------
C
C APPELE PAR : TELMAC
C
C SOUS-PROGRAMME APPELE : NEANT
C
C***********************************************************************
C
      USE MESURES_POSITIONS
C

      IMPLICIT NONE
C INPUT VARIABLES
      INTEGER         ,INTENT(IN)        ::NCSIZE,IPID
C INPUT VARIABLES FOR THE BODIES
      INTEGER         ,INTENT(IN)        ::NP_TOT
      INTEGER         ,INTENT(IN)        ::N_A
      DOUBLE PRECISION,INTENT(IN)        ::X_A(NP_TOT)
      DOUBLE PRECISION,INTENT(IN)        ::Y_A(NP_TOT)
      DOUBLE PRECISION,INTENT(IN)        ::DX(NP_TOT)
      DOUBLE PRECISION,INTENT(IN)        ::DY(NP_TOT)
      DOUBLE PRECISION                   ::X_A_0
      DOUBLE PRECISION                   ::Y_A_0
      DOUBLE PRECISION,INTENT(IN)        ::V_X(NP_TOT)
      DOUBLE PRECISION,INTENT(IN)        ::V_Y(NP_TOT)
      DOUBLE PRECISION,INTENT(IN)        ::V_X_0(NP_TOT)
      DOUBLE PRECISION,INTENT(IN)        ::V_Y_0(NP_TOT)
      DOUBLE PRECISION,INTENT(IN)        ::U_X(NP_TOT)
      DOUBLE PRECISION,INTENT(IN)        ::U_Y(NP_TOT)
      DOUBLE PRECISION,INTENT(IN)        ::U_X_0(NP_TOT)
      DOUBLE PRECISION,INTENT(IN)        ::U_Y_0(NP_TOT)
C VARIABLES USED IN LOOPS
      INTEGER                         :: I_A
      INTEGER                         :: I_Y
C VARIABLES USED TO RECORD THE PROFILES
      DOUBLE PRECISION                   ::X_MULT
      INTEGER:: SUM_N
C VARIABLES USED IN PARALLEL
      INTEGER  P_ISUM
      DOUBLE PRECISION P_DSUM
      EXTERNAL P_ISUM,P_DSUM
C-----------------------------------------------------------------------
C DEFINE THE VARIABLES FOR THE VELOCITY PROFILES
C-----------------------------------------------------------------------
      IF(INIT_PROF)THEN
        INIT_PROF=.FALSE.
C
        IF(VIT.EQ.1)THEN
          X_PROF=0.55D0
        END IF
C
        DY_PROF=2.D0/REAL(NY)
C
        DO I_Y=1,NY
          Y_PROF(I_Y)=REAL(I_Y)*DY_PROF-DY_PROF/2.D0
C
          N_PROF(I_Y)=0
          UX_PROF(I_Y)=0.D0
          UY_PROF(I_Y)=0.D0
          VX_PROF(I_Y)=0.D0
          VY_PROF(I_Y)=0.D0
        END DO
      END IF
C-----------------------------------------------------------------------
C CALCULATE THE VELOCITY PROFILES
C-----------------------------------------------------------------------

      DO I_A=1,N_A
        X_A_0=X_A(I_A)-DX(I_A)
        Y_A_0=Y_A(I_A)-DY(I_A)
        IF((X_A_0.LT.X_PROF.AND.X_A(I_A).GE.X_PROF).OR.
     &     (X_A_0.GE.X_PROF.AND.X_A(I_A).LT.X_PROF))THEN
          IF(X_A(I_A).EQ.X_A_0)THEN
            X_MULT=0.D0
          ELSE
            X_MULT=(X_PROF-X_A_0)/(X_A(I_A)-X_A_0)
          END IF
C
          DO I_Y=1,NY
            IF(Y_PROF(I_Y)-DY_PROF/2.D0.LT.
     &         Y_A_0+X_MULT*(Y_A(I_A)-Y_A_0).AND.
     &         Y_PROF(I_Y)+DY_PROF/2.D0.GE.
     &         Y_A_0+X_MULT*(Y_A(I_A)-Y_A_0))THEN

              N_PROF(I_Y)=N_PROF(I_Y)+1
C
              IF(U_X_0(I_A).GE.1000000.D0)THEN
              ! THE PARTICLE WAS IN ANOTHER PROCESSOR AT TIME LT_0 (OBSOLESCENT)
                UX_PROF(I_Y)=UX_PROF(I_Y)+U_X(I_A)
                UY_PROF(I_Y)=UY_PROF(I_Y)+U_Y(I_A)
                VX_PROF(I_Y)=VX_PROF(I_Y)+V_X(I_A)
                VY_PROF(I_Y)=VY_PROF(I_Y)+V_Y(I_A)
              ELSE
                UX_PROF(I_Y)=UX_PROF(I_Y)+U_X_0(I_A)+X_MULT*(U_X(I_A)
     &                     -U_X_0(I_A))
                UY_PROF(I_Y)=UY_PROF(I_Y)+U_Y_0(I_A)+X_MULT*(U_Y(I_A)
     &                     -U_Y_0(I_A))
                VX_PROF(I_Y)=VX_PROF(I_Y)+V_X_0(I_A)+X_MULT*(V_X(I_A)
     &                     -V_X_0(I_A))
                VY_PROF(I_Y)=VY_PROF(I_Y)+V_Y_0(I_A)+X_MULT*(V_Y(I_A)
     &                     -V_Y_0(I_A))
              END IF

            END IF
          END DO
        END IF
      END DO

C-----------------------------------------------------------------------
C SEND THESE INFORMATION TO PROCESSOR 0
C-----------------------------------------------------------------------
      IF(NCSIZE.GT.1) THEN
        SUM_N=0
        DO I_Y=1,NY
          N_WRI(I_Y)=P_ISUM(N_PROF(I_Y))
          UX_WRI(I_Y)=P_DSUM(UX_PROF(I_Y))
          UY_WRI(I_Y)=P_DSUM(UY_PROF(I_Y))
          VX_WRI(I_Y)=P_DSUM(VX_PROF(I_Y))
          VY_WRI(I_Y)=P_DSUM(VY_PROF(I_Y))
          !
          SUM_N=SUM_N+N_WRI(I_Y)
        END DO
      ELSE
        SUM_N=0
        DO I_Y=1,NY
          N_WRI(I_Y)=N_PROF(I_Y)
          UX_WRI(I_Y)=UX_PROF(I_Y)
          UY_WRI(I_Y)=UY_PROF(I_Y)
          VX_WRI(I_Y)=VX_PROF(I_Y)
          VY_WRI(I_Y)=VY_PROF(I_Y)
          !
          SUM_N=SUM_N+N_WRI(I_Y)
        END DO
      END IF
C IGNORE VALUES THAT ARE TO LOW (NUMERICAL NOISE)
      DO I_Y=1,NY
        IF(SUM_N.NE.0)THEN
          IF(N_WRI(I_Y)/REAL(SUM_N).LE.0.001)THEN
            N_WRI(I_Y)=0
            UX_WRI(I_Y)=0.D0
            UY_WRI(I_Y)=0.D0
            VX_WRI(I_Y)=0.D0
            VY_WRI(I_Y)=0.D0
          END IF
        END IF
      END DO
C
C-----------------------------------------------------------------------
C WRITE THE RESULTS
C-----------------------------------------------------------------------
C
      IF(IPID.EQ.0)THEN
        WRITE(F_PROF_VIT,'(A,I1,A)')
     &           '../prof_vit_',VIT,'.txt'
 9401   FORMAT(A)
 9402   FORMAT(6F16.8)
C
        OPEN(9994,FILE=F_PROF_VIT)
        WRITE(9994,9401) '#canal_chatou.f'
        WRITE(9994,'(A,F5.3)') '#Velocity profil at x =',X_PROF
        WRITE(9994,'(A,I6)') '#Sum_N = ',SUM_N
        WRITE(9994,9401) 'Y N U_X U_Y V_X V_Y'
        DO I_Y=1,NY
          IF(N_WRI(I_Y).NE.0)THEN
            WRITE(9994,9402) Y_PROF(I_Y),REAL(N_WRI(I_Y))/REAL(SUM_N),
     &       UX_WRI(I_Y)/REAL(N_WRI(I_Y)),UY_WRI(I_Y)/REAL(N_WRI(I_Y)),
     &       VX_WRI(I_Y)/REAL(N_WRI(I_Y)),VY_WRI(I_Y)/REAL(N_WRI(I_Y))
     &       
          ELSE
            WRITE(9994,9402) Y_PROF(I_Y),0.D0,0.D0,0.D0,0.D0,0.D0
          END IF
        END DO
C
        CLOSE(9994)
C
      END IF
C
      RETURN
      END
C

