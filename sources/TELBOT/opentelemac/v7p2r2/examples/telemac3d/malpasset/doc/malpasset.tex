% case name
\chapter{malpasset}
%
%
\section{Description}
\bigskip
This test illustrates that \telemac{3d} is able to simulate a real dam
break flow on an initially dry domain.
It also shows the propagation of the wave front and the evolution in
time of the water surface and velocities in the valley downstream.

\bigskip
This case is the simulation of the propagation of the wave following
the break of the Malpasset dam (South-East of France).
Such accident really occurred in December 1959.
The model represents the reservoir upstream from the dam and the valley
and flood plain downstream. 
The entire valley is approximately 18~km long and between 200~m (valley)
and 7~km wide (flood plain).
The complete study is described in details in [1].


\bigskip
The geometry dimensions of the model are around 17~km long and 9~km wide. 
The real bathymetry is shown in Figure \ref{t3d:malpasset:fig:bathy}.
Note that the turbulent viscosity is constant in all the directions 
and equal to 0.1~m$^2$.s$^{-1}$.
The simulation is performed using 2 or 6 vertical planes 
(\sloppy "t3d\_malpasset\_pos\_p2" and \sloppy "t3d\_malpasset\_pos\_p6", 
subsequently named respectively "p2\_pos" and "p6\_pos") 
using the treatment of negative depths introduced since \telemac{3d} version 7.0.
The historical simulation using 2 vertical planes and the method of
characteristics (named \sloppy "t3d\_malpasset\_charac\_p2", subsequently named "p2\_charac") 
has been kept (scheme 1).
Nevertheless, the recommended advection scheme for velocities for such
applications is now the N-type MURD scheme (scheme 14).
A simulation using 2 vertical planes with a large mesh, i.e. finer mesh, (named
\sloppy "t3d\_malpasset\_large",  subsequently named "p2\_large") is also performed.\\
\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/bathy.png}
 \caption{Bathymetry of Malpasset domain.}
 \label{t3d:malpasset:fig:bathy}
\end{figure}

\section{Initial and boundary conditions}
\bigskip
At initial time, the velocity is null. The reservoir is 
full and the downstream valley is dry.

\bigskip
The boundary conditions are:
\begin{itemize}
\item Solid boundaries are without roughness (slip conditions) everywhere,
 i.e., no entrance and no outlet in the domain,
\item On the bottom, Strickler formula with friction coefficient equal 
to 30~$\text{m}^{1/3}.\text{s}^{-1}$ is imposed.
\end{itemize}



\section{Mesh and numerical parameters}
%
\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/mesh.png}
 \caption{Mesh of Malpasset domain.}
 \label{t3d:malpasset:fig:mesh}
\end{figure}

\bigskip
The regular mesh (the cases "p2\_pos", "p6\_pos", "p2\_chara")  is refined 
in the river valley (downstream from the dam) and on the banks 
(seen Figure \ref{t3d:malpasset:fig:mesh}). It is composed 
of 26,000 triangular elements (13,541 nodes), with 2 or 6 planes  
regularly spaced on the vertical to form prism elements.
The size of triangles ranges between 
17~m and 313~m.\\
Then, the large mesh or finer mesh (seen Figure \ref{t3d:malpasset:fig:meshL}, 
the case "p2\_large") is composed of 104,000 triangular elements 
(53,081 nodes), with 2 planes  regularly spaced on 
the vertical to form prism elements. It is also refined in the river valley 
(downstream from the dam) and on the banks. The size of triangles ranges from
8.5 to 156.5~m . 
\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/meshL.png}
 \caption{Large mesh of Malpasset domain.}
 \label{t3d:malpasset:fig:meshL}
\end{figure}

%
\bigskip
The time step is 4~s for the regular mesh and 1~s for the large mesh case.
The simulated period is 4,000~s (= 1 h 6 min 40 s).

\bigskip
This case is computed with the non hydrostatic version. 
For the historical case, the method of characteristics is used 
for the velocities (scheme 1) to solve advection.
For others cases, the N-type MURD scheme for tidal flats
with the treatment of negative depths is used for the velocities (scheme 14).


\section{Results}

\bigskip
Figure \ref{t3d:malpasset:fig:depth} illustrates the progression of 
the flood wave after the dam break 
(simulation using the treatment of negative depths 
that smoothes the results on tidal flats).
The propagation of the wave front is very fast.
The water depths increase then rapidly in 
the valley downstream from the dam location. 
The wave spreads in the plain when arriving to the sea.\\
Moreover, the table \ref{t3d:malpasset:tab:onde} shows 
the propagation times of the dam break wave between several points.
Generally, the propagation times are lower than those measured 
except for "p2\_charac" case. 
However, the time at the A position is neglected because its value is uncertain.

\bigskip
During the simulation, no negative water depths are observed.
Figure \ref{t3d:malpasset:fig:depth_comp} presents the water depth at time = 400~s 
obtained with 2 planes ("p2\_pos"), 
6 planes ("p6\_pos") and the treatment of negative depths, 2 planes using 
the method of characteristics ("p2\_charac")
and 2 planes using the fine mesh ("p2\_large") calculations.
The results are similar.\\
However, the time profiles (Figure \ref{t3d:malpasset:fig:profiles}) 
concerning the point located at x$=5479.03125$~m and y$=4442.33984375$~m,
% ref mesh: 4382, large mesh: 52185
show that the "p2\_pos" planes simulation slightly 
underestimate the maximum water elevation.
Finally the table \ref{t3d:malpasset:tab:zmax} shows the comparison of 
maximum water level between the \telemac{3d} solution and the data measured.
Generally, the results are greater than the measured data,
although the \telemac{3d} solutions are close to these measurements.



\begin{figure}[!htbp]
 \begin{center}
 \includegraphicsmaybe{[width=0.73\textwidth]}{../img/figure_depth.png}
  \end{center}
 \caption{Wave flood propagation.}
 \label{t3d:malpasset:fig:depth}
\end{figure}
%
\begin{table}[!htbp]
\centering
\caption{Propagation time of a dam break wave to point A  (5550~m, 4400~m), 
B (11900~m, 3250~m) and C (13000~m, 2700~m). Note that the arrival time 
at A for the nature case is approximate due to the break time of the dam.}
\begin{tabular}{l|}
\\    \hline Nature \\ "p2\_pos" case\\"p6\_pos" case\\"p2\_charac" case\\"p2\_large" case
\end{tabular}%
\begin{tabular}{c|c|c}
 Arrival time at A (s)  & Time from A to B (s) & Time from A to C (s) \\
\hline
100  & 1140 & 1320 \\
\InputIfFileExists{../img/arrival_pos_2.txt}{}{}
\InputIfFileExists{../img/arrival_pos_6.txt}{}{}
\InputIfFileExists{../img/arrival_p2_charac.txt}{}{}
\InputIfFileExists{../img/arrival_p2_large.txt}{}{}
\end{tabular}
\label{t3d:malpasset:tab:onde}
\end{table}
%
\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/conf.png}
 \caption{Water depth comparison for the configurations at 400~s. }
 \label{t3d:malpasset:fig:depth_comp}
\end{figure}

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/profil_depth.png}
 \caption{Temporal profiles of water depth at control point.}
 \label{t3d:malpasset:fig:profiles}
\end{figure}

\begin{table}[!htbp]
\centering
\caption{Maximum water level (Z$_{\text{s Max}}$) on physical model and 2D numerical 
results with Strickler coefficient of 30~$\text{m}^{1/3}.\text{s}^{-1}$}
\hspace{-1cm}
\begin{tabular}{c|}
\\  Measurement  Points \\ \\\hline 6 (4947~m, 4289~m )\\ 7 (5717~m, 4407~m)\\
   8 (6775~m, 3869~m)\\ 9 (7128~m, 3162~m)\\ 10 (8585~m, 3443~m)\\ 
   11 (9674~m, 3085~m)\\ 12 (10939~m, 3044~m)\\ 13 (11724~m,2810~m)\\ 14 (12723~m, 2485~m)
\end{tabular}%
\begin{tabular}{p{1.7cm}|}
 Z$_{\text{s Max}}$ of \newline physical model (m)  \\\hline 84.2\\ 49.1\\ 54.0\\ 40.2\\
 34.9\\ 27.4\\ 21.5\\ 16.1\\ 12.9
\end{tabular}%
\begin{tabular}{p{1.7cm}|}

    Z$_{\text{s Max}}$ of \newline "p2\_pos" case (m)\\
\hline
\InputIfFileExists{../img/measurement_pos_2.txt}{}{}
\end{tabular}
\begin{tabular}{p{1.7cm}|}
    Z$_{\text{s Max}}$ of \newline "p6\_pos" case (m)\\
\hline
\InputIfFileExists{../img/measurement_pos_6.txt}{}{}
\end{tabular}
\begin{tabular}{p{1.7cm}|}
    Z$_{\text{s Max}}$ of \newline "p2\_charac" case (m)\\
\hline
\InputIfFileExists{../img/measurement_p2_charac.txt}{}{}
\end{tabular}
\begin{tabular}{p{1.7cm}}
    Z$_{\text{s Max}}$  of \newline "p2\_large" case (m)\\
\hline
\InputIfFileExists{../img/measurement_p2_large.txt}{}{}
\end{tabular}
\label{t3d:malpasset:tab:zmax}
\end{table}


%\section{Conclusion}

\bigskip
To conclude, \telemac{3d} is capable of simulating the propagation of a dam break
wave in a river valley initially dry.

\section{Reference}
%
[1] Hydrodynamics of Free Surface Flows modelling with the finite
element method. Jean-Michel Hervouet (Wiley, 2007) pp. 281-288.
%
