\chapter{  TelApy module description}
\label{ch:TelApy_description}

As mentioned in  the introduction part (section \ref{ch:intro}), the \TelApy module is used to control the APIs of \telemacsystem in the Python programming language. The \telemacsystem APIs are developed in Fortran. However, it is relatively easy to use these Fortran routines directly in Python using the "f2py" tool of the python Scipy library \cite{Peterson2009}. This tool will make it possible to compile Fortran code such as it is accessible and usable in Python. This compilation step is directly integrated into the compilation of the \telemacsystem and is thus transparent to the user. Moreover, in order to make the \TelApy module more user friendly, a python wrapper has been developed in order to encapsulate and simplify the different API Python calls. This set of transformation constitutes the \TelApy module.

The first section of this chapter is dedicated to the Fortran API of \telemacsystem. Then, a short description of the wrapper is given.

\section{ \telemacsystem Fortran API description}
%--------------------------------------------------------------------------------
%
An API (Application Programming Interface) is a library allowing to control the execution of a program. Here is part of the definition from Wikipedia:\\

"In computer programming, an application programming interface (API) specifies a software component in terms of its operations, their inputs and outputs and underlying types. Its main purpose is to define a set of functionalities that are independent of their respective implementation, allowing both definition and implementation to vary without compromising each other.

In addition to accessing databases or computer hardware, such as hard disk drives or video cards, an API can be used to ease the work of programming graphical user interface components, to allow integration of new features into existing applications (a so-called "plug-in API"), or to share data between otherwise distinct applications. In practice, many times an API comes in the form of a library that includes specifications for routines, data structures, object classes, and variables." \\

The API’s main goal is to have control on a simulation while running a case. For example, it must allow the user to stop the simulation at any time step, retrieve some variables values and change them. In order to make this possible, a Fortran structure called instance was developed in the API. This informatic struture is described in the paragraph \ref{subsec:instan}. The instance structure gives direct access to the physical memory of variables, and allows therefore the variable control (see paragraph \ref{subsec:var}). Furthermore, modifications have been made in \telemacsystem main subroutines to make hydraulic cases execution possible time step by time step. This will be presented in the paragraph \ref{subsec:exec}. And finaly, the parallelism is evoked (paragraph \ref{subsec:para}).
All Fortran routines are available in the directory "api" of \telemacsystem sources. 

\begin{WarningBlock}{Warning:}
\centering
 In the following sections, all presented API routines are related to \telemac{2D}. However, the API implementation of \telemacsystem modules is generic that is to say based on the same structure (in the following routines the sequence "t2d" related to \telemac{2D} module can be replace by "sis" related to \sisyphe module for instance).
\end{WarningBlock}

\subsection{Instanciation}
\label{subsec:instan}
An instance is an informatic structure that gathers all the variables alterable by the API. The definition of the "instance" structure is made in a Fortran module dedicated to this purpose and is composed of:

\begin{itemize}
\item An indice defining the instance I.D.
\item A string which can contained error messages
\item Some pointers to the concerned module variables. This is what makes it possible to manipulate the variables of the module by having a direct access to their memory location.
\end{itemize}
 
In addition to the instance definition, the module includes all routines needed to manipulate it (creation, deletion, and so on).

\subsection{Variable control}
\label{subsec:var}
The way in which the instance is defined (pointers) allows manipulation of  variables during the simulation. So, to get information on the variables the following set of functions has been implemented:

\begin{itemize}

\item get the list of variables reachable with the API:
\begin{lstlisting}
subroutine get_var_list_t2d(varname, varinfo, ierr)
!
  character(len=t2d_var_len), intent(out) :: varname(nb_var_t2d)
  character(len=t2d_info_len), intent(out) :: varinfo(nb_var_t2d)
  integer, intent(out) :: ierr
!
end subroutine 
\end{lstlisting}
\begin{itemize}
\item \textbf{varname}: An array  of dimension \verb!nb_var_t2d! containing long strings of size \verb!t2d_var_len!, which gives the list of the variable names.
\item \textbf{varinfo}: An array  of dimension \verb!nb_var_t2d! containing long strings of size \verb!t2d_var_len!, which gives  a short description for each variable.
\item \textbf{ierr}: 0 if everything went smoothly an error index otherwise.
\end{itemize}

\item get the type of a variable.

\begin{lstlisting}
subroutine get_var_type_t2d(varname, vartype, readonly, ndim, ierr)
!
  character(len=t2d_var_len),  intent(in)  :: varname
  character(len=t2d_type_len), intent(out) :: vartype
  integer,                     intent(out) :: readonly
  integer,                     intent(out) :: ndim
  integer,                     intent(out) :: ient
  integer,                     intent(out) :: jent
  integer,                     intent(out) :: kent
  integer,                     intent(out) :: getpos
  integer,                     intent(out) :: setpos
  integer,                     intent(out) :: ierr
!
end subroutine get_var_type_t2d
\end{lstlisting}
%
\begin{itemize}
\item \textbf{varname}: The name of the variable.
\item \textbf{vartype}: The type of the variable (DOUBLE, INTEGER, STRING, BOOLEAN).
\item \textbf{readonly}: 0 if the variable can be only read, and 1 if it can be write as well.
\item \textbf{ndim}: Number of dimensions of the variable (maximum is 3 and for a scalar the number of dimension is 0)
\item \textbf{ient}: 1 if the index corresponds to a mesh point
\item \textbf{jent}: 1 if the index corresponds to a mesh point
\item \textbf{kent}: 1 if the index corresponds to a mesh point
\item \textbf{getpos}: give the function name after which one the variable information can be "get"
\item \textbf{setpos}: give the function name after which one the variable information can be "set"
\item \textbf{ierr}: 0 if everything went smoothly, an error index otherwise.
\end{itemize}

\item get the size of a variable.

\begin{lstlisting}
subroutine get_var_size_t2d(id, varname, dim1, dim2, dim3, ierr)
!
  integer,               intent(in) :: id
  character(len=t2d_var_len), intent(in)  :: varname
  integer,               intent(out) :: dim1
  integer,               intent(out) :: dim2
  integer,               intent(out) :: dim3
  integer,               intent(out) :: ierr
!
end subroutine get_var_size_t2d
\end{lstlisting}
%
\begin{itemize}
\item \textbf{varname}: The name of the variable.
\item \textbf{dim1}: Size of the first dimension.
\item \textbf{dim2}: Size of the second dimension.
\item \textbf{dim3}: Size of the third dimension.
\item \textbf{ierr}: 0 if everything went smoothly, an error index otherwise.
\end{itemize}
%
\begin{CommentBlock}{Comment:}
\centering
If the desired variable is a scalar then, it has no respectively first, second or third dimension and dim1,dim2,dim3 are equal to zero.
\end{CommentBlock}

\item get/set the value of a variable for a given index.

These functions depend on the variable type. However, the concept is the same for all kind of type. Moreover, the type distinction is removed in TelApy based on Python benefits. Therefore, for these reasons, the next routine header is focused only on the double type variable. The information for the other routine are available in the Fortran file section of API in the \telemacsystem. 

\begin{lstlisting}
subroutine get_double_t2d
  (id, varname, value, global_num, index1, index2, index3, ierr)
!
  integer,           intent(in)  :: id
  character(len=t2d_var_len), intent(in)  :: varname
  double precision,  intent(out) :: value
  integer,           intent(in)  :: index1
  integer,           intent(in)  :: index2
  integer,           intent(in)  :: index3
  logical,           intent(in)  :: global_num
  integer,           intent(out) :: ierr
!        
end subroutine get_double_t2d
\end{lstlisting}

\begin{lstlisting}
subroutine set_double_t2d
  (id, varname, value, global_num, index1, index2, index3, ierr)
!
  integer,           intent(in)  :: id
  character(len=t2d_var_len), intent(in)  :: varname
  double precision,  intent(in)  :: value
  integer,           intent(in)  :: index1
  integer,           intent(in)  :: index2
  integer,           intent(in)  :: index3
  logical,           intent(in)  :: global_num
  integer,           intent(out) :: ierr
!        
end subroutine set_double_t2d
\end{lstlisting}

\begin{itemize}
\item \textbf{id}: the id of the instance.
\item \textbf{varname}: The name of the variable.
\item \textbf{value}: Contains the value to be read/written.
\item \textbf{index1}: Index of the first dimension (For array of at least one
dimension, not used otherwise).
\item \textbf{index2}: Index of the second dimension (For array of at least two
dimension, not used otherwise).
\item \textbf{index3}: Index of the third dimension (For array of at least
three dimension, not used otherwise).
\item \textbf{global\_num}: Logical to know if the given index are given in mesh global numbering
\item \textbf{ierr}: 0 if everything went smoothly, an error index otherwise.
\end{itemize}

\end{itemize}

\subsection{Computation control}
\label{subsec:exec}
The computation control is carried out using some specific routines to launch the simulation. These routines constitute a decomposition of the main function of each Telemac-Mascaret modules considered corresponding to the following different computation steps:

\begin{itemize}
\item Configuration setup. This function initialises the instance and the output. The instance, characterised by the $ID$ integer parameter, represents a run of telemac2d.

\begin{lstlisting}[language=Fortran]
subroutine run_set_config_t2d(id,lu,lng,ierr)    
!
  integer, intent(out) :: id
  integer, intent(in)  :: lu, lng
  integer, intent(out) :: ierr
!
end subroutine run_set_config_t2d
\end{lstlisting}

\begin{itemize}
\item \textbf{id}: Contains the id of the instance.
\item \textbf{lu}: Defines the output canal for \telemac{2D} (6 will be the standard
output).
\item \textbf{lng}: Defines the output language of \telemac{2D} (1 For French, 2 for
English).
\item \textbf{ierr}: 0 if everything went smoothly an error index otherwise.
\end{itemize}

\begin{CommentBlock}{Comment:}
\centering
In further version, the API will be able to have multiple instances running at the same time. In the current version you can only have one instance at a time.
\end{CommentBlock}

\item Reading the Telemac steering file. This function reads the case file and set the variable of the \telemac{2D} steering file accordingly. 
\begin{lstlisting}[language=Fortran]
subroutine run_read_case_t2d(id,cas_file, dico_file, ierr)
!
  integer,            intent(in)  :: id
  character(len=144), intent(in)  :: cas_file
  character(len=144), intent(in)  :: dico_file
  integer,            intent(out) :: ierr
!
end subroutine run_read_case_t2d
\end{lstlisting}

\begin{itemize}
\item \textbf{id}: The id of the instance.
\item \textbf{cas\_file}: Path to the steering file.
\item \textbf{dico\_file}: Path to the \telemac{2D} dictionary.
\item \textbf{ierr}: 0 if everything went smoothly, an error index otherwise.
\end{itemize}

\begin{WarningBlock}{Warning:}
\centering
With the API we are not using the temporary folder (this folder was created by the Python/Perl environment and all the file declared in the steering file where copied and renamed inside that folder) which means that the name and path given in the steering file will be used. 
\end{WarningBlock}

\item Memory allocation. This function run the allocation of all the data needed in \telemac{2D}. Any modifications to quantities of \telemac{2D} should be done before the call to that function.

\begin{lstlisting}[language=Fortran]
subroutine run_allocation_t2d(id,ierr)
!
  integer, intent(in)  :: id
  integer, intent(out) :: ierr
!
end subroutine run_allocation_t2d
\end{lstlisting}

\begin{itemize}
\item \textbf{id}: The id of the instance.
\item \textbf{ierr}: 0 if everything went smoothly, an error index otherwise.
\end{itemize}

\item Initialization. This function will do the setting of the initial conditions of \telemac{2D} It corresponds to the time-step 0 of a \telemac{2D} run.

\begin{lstlisting}[language=Fortran]
subroutine run_init_t2d(id,ierr)
!
  integer, intent(in)  :: id
  integer, intent(out) :: ierr
!
end subroutine run_run_init_t2d
\end{lstlisting}

\begin{itemize}
\item \textbf{id}: The id of the instance.
\item \textbf{ierr}: 0 if everything went smoothly an error index otherwise.
\end{itemize}


\item Computation function that runs one time-step of \telemac{2D}. To compute all time steps, a loop on this function must be done.

\begin{lstlisting}[language=Fortran]
subroutine run_timestep_t2d(id,ierr)    
!
  integer, intent(in)  :: id
  integer, intent(out) :: ierr
!
end subroutine run_timestep_t2d
\end{lstlisting}

\begin{itemize}
\item \textbf{id}: The id of the instance.
\item \textbf{ierr}: 0 if everything went smoothly, an error index otherwise.
\end{itemize}

\item Finalization. This function concludes the run of \telemac{2D} and will delete the instance. To start a new execution of \telemac{2D} the function RUN\_SET\_CONFIG must be run again.

\begin{lstlisting}[language=Fortran]
subroutine run_finalize_t2d(id,ierr)    
!
  integer, intent(in)  :: id
  integer, intent(out) :: ierr
!
end subroutine run_finalize_t2d
\end{lstlisting}

\begin{itemize}
\item \textbf{id}: The id of the instance.
\item \textbf{ierr}: 0 if everything went smoothly an error index otherwise.
\end{itemize}

\end{itemize}

For each routines defined above, the identity number of the instance is used as an input argument allowing all computation variables to be linked with the corresponding instance pointers. These routines are then called in the good way in order to insure  a well execution of the computation in the API main program.

\subsection{Parralelisation}
\label{subsec:para}
All steps associated with parallel computation must be performed by the user when he chooses to launch his calculation on several processors. In this case, after initializing the MPI environment, the user must partition the input files (geometry file, boundary conditions files, and so on) using the Fortran function "partel". Then, when the calculation is complete, it is necessary to merge each subdomains result files using the "gretel" routine of the \telemacsystem. The MPI environment can then be closed.

\section{TelApy module}

It is relatively easy to use the Fortran API routines directly in Python using the "$f2py$" tool of the python Scipy library. This tool will make it possible to compile Fortran code such as it is accessible and usable in Python. For more details on this tool, the interested reader can refer directly to \cite{Peterson2009}. However, based on the advantage of the python language, it is possible to implement a wrapper in order to provide user friendly function of the Fortran API. Thus, a python overlay was developed in order to encapsulate and simplify the different API Python calls. The different python functions writte to simplify the use of API are available in the directory "$myTelemacInstall/scripts/python27/TelAPy/$". 
A doxygen documentation is available and allows the user to visualize python classes, functions that can be used as well as its input and output variables and so on.

In order to launch the doxygene documentation, the user need to copy and paste the link "$myTelemacInstall/documentation/TelAPy/doxygen/html/index.html$". Then, the user can navigate in the doxygene environment in order to find information. For example, if the package list widget is selected, then, all python tools are visibles such as:

\begin{itemize}
\item TelApy
\begin{itemize}
\item api
\begin{itemize}
\item api\_module: The generic python class for \telemacsystem APIs
\item generate\_study: Automatic generator of \telemacsystem API template python
\item hermes: Input and Output library allowing to write and read different \telemacsystem formats
\item sis: The \sisyphe python class for APIs
\item t2d: The \telemac{2D} python class for APIs
\end{itemize}
\end{itemize}
\begin{itemize}
\item tools
\begin{itemize}
\item genop: Optimization tool based on genetic algorithms
\begin{itemize}
\item costfunction
\item crossover
\item genop
\item genpop
\item mutation
\item selection
\item showresults
\item validate
\end{itemize}
\item newop: Optimization tool based onthe SciPy minimizer function
\begin{itemize}
\item newop
\item numval
\item validate
\end{itemize}
\item polygon: Function allowing to give point indices which are in a polygon defined by the user
\item simul:
\item test\_genop
\item test\_newop
\end{itemize}
\end{itemize}
\end{itemize}

Then, if some information are desierd on a specific class as for example "api\_module", by click on this class, all function contained in it are listed as follow:

\begin{itemize}
\item \verb+def _init_+: Constructor for api module
\item \verb+def set_case+: Read the steering file and run allocation
\item \verb+def init_state_default+: Initialize the state of the model Telemac 2D with the values of disharges and water levels as indicated by the steering file
\item \verb+def run_one_time_step+: Run one the time step
\item \verb+def run_all_time_steps+: Run all the time steps
\item \verb+def get_mesh+: Get the 2D mesh of triangular cells
\item \verb+def get_node+: Get the nearest node number for the coordinates (xval, yval)
\item \verb+def get_elem+: Get the triangle where the point (xval, yval) is
\item \verb+def show_mesh+: Show the 2D mesh with topography
\item \verb+def list_variables+: List the names and the meaning of available variables and parameters
\item \verb+def get+: Get the value of a variable of \telemacsystem modules
\item \verb+def set+: Set the value of a variable of \telemacsystem modules
\item \verb+def get_array+: Retrieves all the values from a variable into a numpy array
\item \verb+def set_array+: Changes all the values from a variable into a numpy array
\item \verb+def get_on_polygon+: Retrieves values for point within the polygon poly Warning this works only on array that are of size NPOIN
\item \verb+def set_on_polygon+: Set varname to value on all points that are within the polygon poly Warning this works only on array that are of size NPOIN
\item \verb+def get_on_range+: Retrieves the values of the variable on the range given as argument
\item \verb+def set_on_range+: set the values of the variable on the range given as argument
\item \verb+def get_error_message+: Get the error message from the Fortran sources
\item \verb+def finalize+: Delete the \telemacsystem instance
\item \verb+def generate_var_info+: Returns a dictionary containg specific informations for each variable
\end{itemize}

For each of the previous function all information concerning the inputs and outputs are available such as for example for the "set" fonction which allows to get the value of the variable of \telemacsystem module:

\begin{lstlisting}[language=Python]
def set(self,varname,value,i=0,j=0,k=0,global_nul=True)
\end{lstlisting}

\begin{itemize}
\item \textbf{varname}: Name of the varaible
\item \textbf{i}: index otherwise.index of the first dimension
\item \textbf{j}: index otherwise.index of the second dimension
\item \textbf{k}: index otherwise.index of the third dimension
\item \textbf{global\_num}: are the index on local/global numbering variable value
\end{itemize}