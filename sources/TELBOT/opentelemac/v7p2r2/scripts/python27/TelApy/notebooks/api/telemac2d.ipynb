{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style=\"float:left;\">Licence CC BY-SA</span><span style=\"float:right;\">Fabrice Zaoui&nbsp;</span><br/>\n",
    "___"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This tutorial is intended for people who want to run Telemac 2D in an interactive mode with the help of the Python programming language. The interactive mode means that communication with Telemac becomes possible throughout the simulation without having to stop it. One can easily set or get the value of any variables at each time step with the help of special communication functions: the [Application Programming Interfaces](https://en.wikipedia.org/wiki/Application_programming_interface) (API).\n",
    "\n",
    "By using the APIs, research and engineering with Telemac is encouraged in every fields where communication is crucial: optimization, code coupling, control system, sensitivity analysis and so on."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Telemac 2D as a Python module"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Building the interface"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When using Telemac in a classic way (i.e. without API), you first have to compile all the Fortran sources of Telemac with (for example) the following command:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "compileTELEMAC.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "*Note: if an error occurs, check for the environment variables HOMETEL and SYSTELCFG before running the previous command.*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the compilation is successful, you will then run Telemac on your test case providing a steering file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "telemac2d.py my_steering_file.cas"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Note: as the name of the steering file here is fictive, no computation is done*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The previous command requires all the static librairies to be linked with the Fortran user file in an executable. When using the API with Python, it is the dynamic libraries of the Telemac system that are used."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The interface between the Telemac libraries and the Python language is automatically created using [f2py](http://docs.scipy.org/doc/numpy-dev/f2py/). This corresponds to the compilation of the Telemac API for Python. It has to be done by adding :\n",
    "options: api\n",
    "in the configuration file (systel.cfg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Using the interface"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Telemac/Python is now ready to be used as a hydrodynamic 2D solver with the import system:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import TelApy.api.t2d"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The module 'TelApy.api.t2d' is located in the '$HOMETEL/scripts/python27/TelApy/api' directory. If an error occurs while attempting to import, check the value of the environment variable PYTHONPATH. Alternatively you can also work with the file 'apiT2d.py' listed in your working directory.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "echo $PYTHONPATH"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The module 'TelApy.api.t2d' defines a class named 'Telemac2d'. The user will have to instantiate an object from this class in order to run Telemac with all the facilities offered by the API."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Changing of directory : 'examples' directory of the Telemac sources\n",
    "import os\n",
    "HOMETEL = os.environ.get('HOMETEL')\n",
    "os.chdir(HOMETEL + '/examples/telemac2d/breach')\n",
    "\n",
    "# Instantiation of a Telemac2d object from the test case named 'breach'\n",
    "# - steering file : 't2d_breach.cas'\n",
    "# - language : french (1) or english (2: default)\n",
    "from TelApy.api.t2d import Telemac2d\n",
    "from mpi4py import MPI\n",
    "my_case = Telemac2d('t2d_breach.cas', lang=1, comm=MPI.COMM_WORLD)\n",
    "my_case"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An object 'my_case' is now created. This object offers some useful methods to communicate with the Telemac computational kernel. These (non exaustive) methods are seen with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dir(my_case)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "# API description"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The methods listed above give access to the Fortran Telemac API and to some extra functionalities based on Python packages like 'numpy' or 'matplotlib'. The list is still uncomplete and will be improved in future."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Get the value of a Telemac variable"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(my_case.get)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Get a node number"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(my_case.get_node)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Get an element number"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(my_case.get_elem)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Error message from Telemac"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(my_case.get_error_message)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Mesh coordinates and connectivity"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(my_case.get_mesh)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Hydraulic state"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(my_case.get_state)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(my_case.set_state)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Read the steering file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(my_case.set_case)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### State initialisation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(my_case.init_state_default)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Save a state"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(my_case.save_state)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Back to a saved state"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(my_case.restore_state)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Run Telemac for one time step"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(my_case.run_one_time_step)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Run Telemac for all the time steps"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(my_case.run_all_time_steps)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Change the value of a Telemac variable"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(my_case.set)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plotting the mesh"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(my_case.show_mesh)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plotting the state"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(my_case.show_state)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Telemac Variables"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All the variables and parameters available with the API to set or get a value can be seen with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "varnames, varinfo = my_case.list_variables()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for a, b in zip(varnames, varinfo):\n",
    "    print a + '\\t\\t\\t' + b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Example of use"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Knowing now the API and the variables, a Telemac computation on the test example named 'breach' could be:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Read the steering file and do allocations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "my_case.set_case()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Initialisation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "my_case.init_state_default()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Mesh view"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "my_figure = my_case.show_mesh(visu2d=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Simulation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for i in xrange(100):\n",
    "    my_case.run_one_time_step()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Current state"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "h, u, v = my_case.get_state()\n",
    "\n",
    "print 'Mean water height (m) = ', h.mean()\n",
    "print 'Max velocity (m/s) = ', np.amax(np.sqrt(u * u + v * v))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Graph"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "my_new_figure = my_case.show_state(show=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Save"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "my_case.save_state()\n",
    "my_case.hsave\n",
    "my_case.usave\n",
    "my_case.vsave"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Change"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# to null velocity\n",
    "number_of_nodes = my_case.get('MODEL.NPOIN')\n",
    "for i in xrange(number_of_nodes):\n",
    "    my_case.set('MODEL.VELOCITYU', 0., i=i)\n",
    "    my_case.set('MODEL.VELOCITYV', 0., i=i)\n",
    "   \n",
    "h, u, v = my_case.get_state()\n",
    "print 'Mean velocity (m/s) = ', np.mean(np.sqrt(u * u + v * v))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Restore"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "err = my_case.restore_state()\n",
    "\n",
    "h, u, v = my_case.get_state()\n",
    "print 'Max velocity (m/s) = ', np.amax(np.sqrt(u * u + v * v))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# bottom elevation at node no. 102\n",
    "my_case.get('MODEL.BOTTOMELEVATION', 102)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# friction coefficient (Strickler value) at node no. 500\n",
    "my_case.get('MODEL.CHESTR', 500)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Deletion"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "my_case.finalize()\n",
    "del my_case"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
