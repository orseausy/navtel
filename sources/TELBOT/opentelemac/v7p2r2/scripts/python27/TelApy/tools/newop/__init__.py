"""
NEWOP : a Python (v2) Newton Optimizer

Based on the SciPy Minimizer function

Auteur : Fabrice Zaoui (EDF R&D LNHE)

Copyright EDF 2017

"""


__author__ = "Fabrice Zaoui"
__copyright__ = "Copyright EDF 2017"
__license__ = "GPL"
__maintainer__ = "Fabrice Zaoui"
__email__ = "fabrice.zaoui@edf.fr"
__status__ = "Implementation"
__version__ = "0.01"
__all__ = ['newop', 'numval', 'validate', 'Newop']


import newop
import numval
import validate
from newop import Newop
