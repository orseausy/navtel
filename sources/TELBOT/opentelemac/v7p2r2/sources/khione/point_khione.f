!                    ***********************
                     SUBROUTINE POINT_KHIONE
!                    ***********************
!
     &( MESH,IELMX )
!
!***********************************************************************
! KHIONE   V7P3
!***********************************************************************
!
!brief    Memory allocation of structures, aliases, blocks...
!
!history  F. HUANG (CLARKSON U.) AND S.E. BOURBAN (HRW)
!+        11/11/2016
!+        V7P3
!+        Coupling TELEMAC-2D with KHIONE (ice modelling component)
!+        Initial developments
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE BIEF
      USE DECLARATIONS_SPECIAL
      USE DECLARATIONS_KHIONE
!      USE DECLARATIONS_WAQTEL, ONLY: TDEW,VISBI
!
      IMPLICIT NONE
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER,         INTENT(IN) :: IELMX
      TYPE(BIEF_MESH), INTENT(IN) :: MESH
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER NTR,I,J
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
!     KHIONE DEPENDENCIES:
!     - WATER TEMPERATURE TAKEN FROM TELEMAC-2D
!     - AIR TEMPERATURE TAKEN FROM WAQTEL
!     - WIND TAKEN FROM TELEMAC-2D
!
!      CALL BIEF_ALLVEC(1,SUMPH, 'SUMPH ',IELMX,1,1,MESH)
      CALL BIEF_ALLVEC(1,PHCL,  'PHCL  ',IELMX,1,1,MESH)
      CALL BIEF_ALLVEC(1,PHRI,  'PHRI  ',IELMX,1,1,MESH)
      CALL BIEF_ALLVEC(1,PHPS,  'PHPS  ',IELMX,1,1,MESH)
      CALL BIEF_ALLVEC(1,PHIB,  'PHIB  ',IELMX,1,1,MESH)
      CALL BIEF_ALLVEC(1,PHIE,  'PHIE  ',IELMX,1,1,MESH)
      CALL BIEF_ALLVEC(1,PHIH,  'PHIH  ',IELMX,1,1,MESH)
      CALL BIEF_ALLVEC(1,PHIP,  'PHIP  ',IELMX,1,1,MESH) ! /!\ RAIN ?
      CALL BIEF_ALLVEC(1,PHIWI, 'PHIWI ',IELMX,1,1,MESH)
!
!     ICE CONCENTRATION AS COMPUTED FROM SURFACE ICE PARTICLES
      CALL BIEF_ALLVEC(1,ANFEM, 'ANFEM ',IELMX,1,1,MESH)
!
!
!      CALL BIEF_ALLVEC(1,TDEW,  'TDEW  ',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,VISBI,  'VISBI ',IELMX,1,1,MESH)
!
!###>  NOW CV_F REPLACED BY TR%ADR(IND_FC)
!      CALL BIEF_ALLVEC(1,CV_F,  'CV_F  ',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,TW,  'TW    ',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,HTW,  'HTW   ',IELMX,1,1,MESH)
!
!      CALL BIEF_ALLVEC(1,THETA0, 'THETA0',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,THETA1, 'THETA1',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,GAMC  , 'GAMC  ',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,GAMA  , 'GAMA  ',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,BETA1 , 'BETA1 ',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,VBB   ,'VBB   ',IELMX,1,1,MESH)
!
!      CALL BIEF_ALLVEC(1,ZWI   ,'ZWI   ',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,THIFEM,'THIFEM',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,THIFEMF,'THIFEMF',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,DTHIFEM,'DTHIFE',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,UICE  ,'UICE  ',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,VICE  ,'VICE  ',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,ZICE  ,'ZICE  ',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,ZWAT  ,'ZWAT  ',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,UQX   ,'UQX   ',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,UQY   ,'UQY   ',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,QX    ,'QX    ',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,QY    ,'QX    ',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,HBED  ,'HBED  ',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,HICE  ,'HICE  ',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,ETA   ,'ETA   ',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,ETAB  ,'ETAB  ',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,DETAX ,'DETAX ',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,DETAY ,'DETAY ',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,TMICE ,'TMICE ',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,CNIEND,'CNIEND',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,THIFEMS ,'THIFEMS',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,THIFEMF ,'THIFEMF',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,TISFEM,'TISFEM',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,THUN,'THUN  ',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,HUN,'HUN   ',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,TIWX,'TIWX  ',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(1,TIWY,'TIWY  ',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(2,JAMFEM,'JAMFEM',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(2,ISBORDER,'ISBORDER',IELMX,1,1,MESH)
!      CALL BIEF_ALLVEC(2,ICEREGION,'ICEREGION',IELMX,1,1,MESH)
!
!
!      CNI = 0.02 ! ice roughness
!      CNISLD = 0.02 ! ice island roughness
!      CNIMAX = 0.06 ! max ice roughness
!      THI0 = 0.2  ! single layer ice thickness
!      ANMAX = 0.6 ! max ice concentration
!      DARCYILD = 0.0
!      DARCYRUB = 0.0

!     ice parcels
!
!      CALL BIEF_ALLVEC(1,upx,      'upx   ',0,2,0,MESH)
!      CALL BIEF_ALLVEC(1,upy,      'upy   ',0,2,0,MESH)
!      CALL BIEF_ALLVEC(1,xp,       'xp    ',0,2,0,MESH)
!      CALL BIEF_ALLVEC(1,yp,       'yp    ',0,2,0,MESH)
!      CALL BIEF_ALLVEC(1,strfx,    'strfx ',0,2,0,MESH)
!      CALL BIEF_ALLVEC(1,strfy,    'strfy ',0,2,0,MESH)
!      CALL BIEF_ALLVEC(2,nparel,   'nparel',0,2,0,MESH)
!      CALL BIEF_ALLVEC(2,idv,      'idv   ',0,2,0,MESH)
!      CALL BIEF_ALLVEC(2,idelta,   'idelta',0,2,0,MESH)
!      CALL BIEF_ALLVEC(1,dudxt,    'dudxt ',0,2,0,MESH)
!      CALL BIEF_ALLVEC(1,dvdyt,    'dvdyt ',0,2,0,MESH)
!      CALL BIEF_ALLVEC(1,dudydvdxt,'duvdyx',0,2,0,MESH)
!      CALL BIEF_ALLVEC(1,sigxx,    'sigxx ',0,2,0,MESH)
!      CALL BIEF_ALLVEC(1,sigyy,    'sigyy ',0,2,0,MESH)
!      CALL BIEF_ALLVEC(1,sigxy,    'sigxy ',0,2,0,MESH)
!      CALL BIEF_ALLVEC(1,tisp,     'tisp  ',0,2,0,MESH)
!      CALL BIEF_ALLVEC(1,tipp,     'tipp  ',0,2,0,MESH)
!      CALL BIEF_ALLVEC(1,ep,       'ep    ',0,2,0,MESH)
!      CALL BIEF_ALLVEC(1,thips,    'thips ',0,2,0,MESH)
!      CALL BIEF_ALLVEC(1,thipf,    'thipf ',0,2,0,MESH)
!      CALL BIEF_ALLVEC(1,areap,    'areap ',0,2,0,MESH)
!      CALL BIEF_ALLVEC(1,um,       'um    ',0,2,0,MESH)
!      CALL BIEF_ALLVEC(1,pms,      'pms   ',0,2,0,MESH)
!      CALL BIEF_ALLVEC(1,pmf,      'pmf   ',0,2,0,MESH)
!      CALL BIEF_ALLVEC(1,pm,       'pm    ',0,2,0,MESH)
!      CALL BIEF_ALLVEC(1,thi0p,    'thi0p ',0,2,0,MESH)
!      CALL BIEF_ALLVEC(2,iceOrgP,  'iceOrg',0,2,0,MESH)
!      CALL BIEF_ALLVEC(2,iceTypeP, 'iceTyp',0,2,0,MESH)
!      CALL BIEF_ALLVEC(2,nparelm,  'nparel',0,2,0,MESH)
!      CALL BIEF_ALLVEC(1,upx0,     'upx0  ',0,2,0,MESH)
!      CALL BIEF_ALLVEC(1,upy0,     'upy0  ',0,2,0,MESH)
!      CALL BIEF_ALLVEC(1,xp0,      'xp0   ',0,2,0,MESH)
!      CALL BIEF_ALLVEC(1,yp0,      'yp0   ',0,2,0,MESH)
!      CALL BIEF_ALLVEC(1,hpi0p,    'hpi0p ',0,2,0,MESH)
!      CALL BIEF_ALLVEC(1,umi0p,    'umi0p ',0,2,0,MESH)
!      CALL BIEF_ALLVEC(1,HydroPr,  'HydrPr',0,2,0,MESH)
!      CALL BIEF_ALLVEC(1,nsort,    'nsort ',0,2,0,MESH)
!      CALL BIEF_ALLVEC(1,KSTRSTATE,'KSTRST',0,2,0,MESH)
!
!
!-----------------------------------------------------------------------
!
!     ALLOCATE THE BLOCK WHICH CONNECTS A VARIABLE NAME TO ITS ARRAY
!     FOR GRAPHICAL OUTPUT
!
      CALL ALLBLO(VARSOR ,'VARSOR')
!
! 01  THERMAL BUDGET: SOLAR RADIATION UNDER CLEAR SKY
      CALL ADDBLO(VARSOR,PHCL)
! 02  THERMAL BUDGET: SOLAR RADIATION UNDER CLOUDY SKY
      CALL ADDBLO(VARSOR,PHRI)
! 03  THERMAL BUDGET: NET SOLAR RADIATION AFTER REFLECTION
      CALL ADDBLO(VARSOR,PHPS)
! 04  THERMAL BUDGET: EFFECTIVE BACK RADIATION
      CALL ADDBLO(VARSOR,PHIB)
! 05  THERMAL BUDGET: EVAPORATION HEAT FLUX
      CALL ADDBLO(VARSOR,PHIE)
! 06  THERMAL BUDGET: CONDUCTIVITY HEAT FLUX
      CALL ADDBLO(VARSOR,PHIH)
!
!-----------------------------------------------------------------------
!
!     DIFFERENTIATED VARIABLES
!
      IF( NADVAR.GT.0 ) THEN
        J = VARSOR%N + 1
        CALL ALLBLO(ADVAR ,'ADVAR ')
        CALL BIEF_ALLVEC_IN_BLOCK(ADVAR,NADVAR,1,'AD    ',IELMX,
     &                            1,2,MESH)
        DO I = 1,NADVAR
          CALL AD_GET_KHIONE(I,ADVAR%ADR(I)%P)
!
          IF( J.GT.MAXVAR ) THEN
            IF(LNG.EQ.1) THEN
              WRITE(LU,*) 'POINT : TROP DE DERIVEES A IMPRIMER'
            ENDIF
            IF(LNG.EQ.2) THEN
              WRITE(LU,*) 'POINT : TOO MANY DERIVATIVES TO PRINT OUT'
            ENDIF
            CALL PLANTE(1)
            STOP
          ENDIF
!
          IF( SORLEO(J).OR.SORIMP(J) ) THEN
            CALL ADDBLO(VARSOR,ADVAR%ADR(I)%P)
            J = J + 1
          ENDIF
!
        ENDDO
      ENDIF
!
!-----------------------------------------------------------------------
!
!     WORKING ARRAYS
!
!     ONLY 2 REQUIRED FOR THE CALL TO WRITE_MESH WITHIN OUTPUT_KHIONE
      NTR = 2
!
!     ALLOCATES NTR WORKING ARRAYS
!     TB WILL CONTAIN ARRAYS T1,T2,...
!
      CALL ALLBLO(TB ,'TB    ')
      CALL BIEF_ALLVEC_IN_BLOCK(TB,NTR,1,'TB    ',IELMX,1,2,MESH)
!
!     ALIAS FOR THE WORKING ARRAYS OF THE BLOCK: TB
!
      T1 =>TB%ADR( 1)%P
      T2 =>TB%ADR( 2)%P
!
!-----------------------------------------------------------------------
!
      RETURN
      END
