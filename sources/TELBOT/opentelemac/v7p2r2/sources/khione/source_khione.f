!                    ************************
                     SUBROUTINE SOURCE_KHIONE
!                    ************************
!
     &( NPOIN,TEXP,TIMP,TN,HPROP,U,V,
     &  LATIT,LONGIT,DT,AT,MARDAT,MARTIM,DEBUG, PHI0,LAMBD0 )
!
!***********************************************************************
! KHIONE   V7P2                                             02/11/2016
!***********************************************************************
!
!brief    COMPUTES CONTRIBUTION TO TRACER SOURCE TERMS RESULTING
!+        FROM ICE PROCESSES.
!+        IN PARTICULAR (DEPENDING ON ICEPROCESS):
!+          #2.- THERMAL BALANCE
!+          #3.- ...
!
!history  F. HUANG (CLARKSON U.) AND S.E. BOURBAN (HRW)
!+        19/11/2016
!+        V7P2
!+        INITIAL DEVELOPMENTS
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!| NPOIN      |-->| NUMBER OF NODES IN THE MESH
!| IND_T      |-->| TRACER INDEX FOR WATER TEMPERATURE
!| LONGIT     |-->| LONGITUTE OF ORIGIN POINT
!| LATIT      |-->| LATITUDE OF ORIGIN POINT
!| DT         |-->| TIME STEP
!| AT         |-->| TIME IN SECONDS
!| NTRAC      |-->| NUMBER OF TRACERS
!| TEXP       |<--| EXPLICIT SOURCE TERM.
!| TIMP       |<--| IMPLICIT SOURCE TERM.
!| TN         |-->| TRACERS AT TIME N
!| HPROP      |-->| PROPAGATION DEPTH
!| PHI0       |-->| LONGITUDE OF ORIGIN POINT (KEYWORD, IN DEGREES)
!| LAMBD0     |-->| LATITUDE OF ORIGIN POINT (KEYWORD, IN DEGREES)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE BIEF
      USE DECLARATIONS_SPECIAL
      USE DECLARATIONS_WAQTEL, ONLY : RO0,CP_EAU
      USE DECLARATIONS_KHIONE, ONLY : PHCL,PHRI,PHPS,PHIB,PHIE,PHIH,
     &  PHIP,PHIWI,ANFEM,TMELT,ICEPROCESS,IND_FC,IND_T
      USE METEO_KHIONE,        ONLY : SYNC_METEO,
     &  WINDX,WINDY,TAIR,TDEW,CLDC,VISBI,PLUIE
      USE THERMAL_KHIONE,      ONLY : THERMAL_FLUXES
!
      IMPLICIT NONE
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER, INTENT(IN)            :: DEBUG,NPOIN
      INTEGER, INTENT(IN)            :: MARDAT(3),MARTIM(3)
!
      DOUBLE PRECISION,INTENT(IN)    :: LATIT,LONGIT,DT,AT
!
      TYPE(BIEF_OBJ), INTENT(IN)     :: TN
      TYPE(BIEF_OBJ), INTENT(INOUT)  :: TEXP
      TYPE(BIEF_OBJ), INTENT(INOUT)  :: TIMP
      TYPE(BIEF_OBJ), INTENT(IN)     :: HPROP,U,V
      DOUBLE PRECISION, INTENT(IN)   :: PHI0,LAMBD0
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER                     :: I,CICE
      DOUBLE PRECISION, PARAMETER :: EPS=1.D-3
      DOUBLE PRECISION            :: CONSTSS
!      DOUBLE PRECISION            :: THERMIC
      DOUBLE PRECISION            :: DH,VELMAG,WDMAG,HWIN,SUMPH
!
      INTRINSIC MAX
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
! ICEPROCESS:
!    PRIME NUMBER DEFINING WHICH PROCESS IS SWITCHED ON:
!    - 2: THERMAL BALANCE
!    - 3:
!    - 0: ALL PROCESSES ABOVE BECAUSE N*INT(0/N) = 0
!    - 1: NONE OF THE PROCESSES BECAUSE N*INT(1/N) <> 1
!
!-----------------------------------------------------------------------
!
!=======================================================================
!
!     1 - FOR GOOD MEASURE - SHOULD BE DONE AT THE TELEMAC-2D LEVEL
!
!-----------------------------------------------------------------------
!
!     NOTE THAT "AT" FROM DIFSOU IS AREADY TOO FAR GONE
      CALL SYNC_METEO(AT-DT)
!
!
!=======================================================================
!
!     2 - THERMAL BALANCE
!
!-----------------------------------------------------------------------
!
      IF( INT(ICEPROCESS/2)*2 .EQ. ICEPROCESS ) THEN
!
!       MAJORATED RADIATION
        CONSTSS = 1.D0/(RO0*CP_EAU)
!
!       FLUXES
        DO I = 1,NPOIN
!
! ~~>     WIND SPEED EFFECTS ON ICE
          WDMAG = SQRT( WINDX%R(I)**2 + WINDY%R(I)**2 )
!
! ~~>     OPEN WATER CONDITIONS
          CICE = 0
! ~~>     ICE COVER CONDITIONS
          IF( ANFEM%R(I).GT.0.D0 ) CICE = 1
!
          CALL THERMAL_FLUXES(TAIR%R(I),TN%ADR(IND_T)%P%R(I),TDEW%R(I),
     &         CLDC%R(I),VISBI%R(I),WDMAG,PLUIE%R(I),
     &         PHCL%R(I),PHRI%R(I),
     &         PHPS%R(I),PHIB%R(I),PHIE%R(I),PHIH%R(I),PHIP%R(I),
     &         CICE,DT,AT,MARDAT,MARTIM,SUMPH, PHI0,LAMBD0 )
!
!     EXPLICITE SOURCE TERM - HOW ABOUT IMPLICITE ?
!
          TEXP%ADR(IND_T)%P%R(I) =
     &      CONSTSS*SUMPH*( 1.D0-ANFEM%R(I) ) / MAX(HPROP%R(I),EPS)
!
        ENDDO
!
!=======================================================================
!
!     3 - ICE COVER -- TO BE VALIDATED
!
!-----------------------------------------------------------------------
!
        DO I=1,NPOIN

          VELMAG = SQRT(U%R(I)**2 + V%R(I)**2)
          IF (ANFEM%R(I).GT.0.5) THEN   ! consider ice effects
            DH = 2.0*HPROP%R(I)  ! subject to htmin, used to calc hwin
!!          CALL FHC_KHIONE(HWIN,DH,VELMAG,TN%ADR(IND_T)%P%R(I))
          ELSE   ! no ice effects
            DH = 4.0*HPROP%R(I)  ! subject to htmin, used to calc hwin
!!          CALL FHC_KHIONE(HWIN,DH,VELMAG,TN%ADR(IND_T)%P%R(I))
          ENDIF

          PHIWI%R(I) = -HWIN*(TN%ADR(IND_T)%P%R(I)-TMELT)

!         TEXP%ADR(IND_T)%P%R(I) = TEXP%ADR(IND_T)%P%R(I) +
!     &  CONSTSS*PHIWI%R(I)*(ANFEM%R(I))/ MAX(HPROP%R(I),EPS)

!   COEF FOR IMPLICITE
!         TIMP%ADR(IND_T)%P%R(I) = TIMP%ADR(IND_T)%P%R(I) +
!     &   CONSTSS*ANFEM%R(I)*HWIN/
!     &   MAX(HPROP%R(I),EPS)

!    EXPLICIT SOURCE PART
!         TEXP%ADR(IND_T)%P%R(I) = TEXP%ADR(IND_T)%P%R(I) -
!     &   CONSTSS*ANFEM%R(I)*HWIN*TMELT/
!     &   MAX(HPROP%R(I),EPS)
        ENDDO
!
      ENDIF
!-----------------------------------------------------------------------
!
      RETURN
      END
