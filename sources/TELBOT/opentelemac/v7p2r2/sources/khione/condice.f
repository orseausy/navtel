!                    ******************
                     SUBROUTINE CONDICE
!                    ******************
!
!***********************************************************************
! KHIONE   V7P3
!***********************************************************************
!
!brief    INITIALISES THE PHYSICAL PARAMETERS FOR ICE.
!
!history  F. HUANG (CLARKSON U.) AND S.E. BOURBAN (HRW)
!+        09/09/2017
!+        V7P3
!+        INITIAL DEVELOPMENTS
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE BIEF
!      USE DECLARATIONS_TELEMAC2D,ONLY : MESH,H
      USE DECLARATIONS_KHIONE
!      USE DECLARATIONS_WAQTEL, ONLY :TDEW,VISBI
!
      USE DECLARATIONS_SPECIAL
      IMPLICIT NONE
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
!      INTEGER ITRAC,I,NPOIN,J,K,J1,J2,J3,KN2,I1
!      DOUBLE PRECISION TICE
!      CHARACTER(LEN=50) CASENAME2
!      CHARACTER(LEN=3) NSERIAL
!      logical lg
!
!-----------------------------------------------------------------------
!
!   ATMOSPHERIC HEAT FLUXES
!
!     PHCL: SOLAR RAD (FLUX) REACHING SURFACE, UNDER CLEAR SKY
      CALL OS('X=C     ', X=PHCL, C=0.D0 )
!     PHRI: SOLAR RAD (FLUX) REACHING SURFACE, UNDER CLOUDY SKY
      CALL OS('X=C     ', X=PHRI, C=0.D0 )
!     PHPS: NET SOLAR RAD (FLUX) AFTER REFLECTION
      CALL OS('X=C     ', X=PHPS, C=0.D0 )
!     PHIB: EFFECTIVE BACK RADIATION (OPEN WATER OR ICE)
      CALL OS('X=C     ', X=PHIB, C=0.D0 )
!     PHIE: EVAPORATIVE HEAT TRANSFER
      CALL OS('X=C     ', X=PHIE, C=0.D0 )
!     PHIH: CONVECTIVE HEAT TRANSFER
      CALL OS('X=C     ', X=PHIH, C=0.D0 )
!     PHIP: HEAT TRANSFER DUE TO PRECIPITATION
      CALL OS('X=C     ', X=PHIP, C=0.D0 )
!     PHIWI: HEAT TRANSFER BETWEEN WATER AND ICE
      CALL OS('X=C     ', X=PHIWI, C=0.D0 )
!
!-----------------------------------------------------------------------
!
!   ICE COVER
!
!     MORE ICE THAN WATER AT SURFACE IF ANFEM(I) > 0.5
      CALL OS('X=C     ', X=ANFEM, C=0.D0 )
!
!-----------------------------------------------------------------------
!
!
!        CALL OS('X=C     ', X=SUMPH, C=0.D0 )

!]      CALL OS('X=C     ', X=TDEW, C=0.D0 )
!]      CALL OS('X=C     ', X=VISBI, C=0.D0
!###>  NOW CV_F REPLACED BY TR%ADR(IND_FC)
!      CALL OS('X=C     ', X=CV_F, C=0.D0 )
!]      CALL OS('X=C     ', X=TW, C=0.D0 )
!]      CALL OS('X=C     ', X=THETA0, C=0.D0 )
!]      CALL OS('X=C     ', X=THETA1, C=0.D0 )
!]      CALL OS('X=C     ', X=GAMC, C=0.D0 )
!]      CALL OS('X=C     ', X=GAMA, C=0.D0 )
!]      CALL OS('X=C     ', X=HTW, C=0.D0 )
!]      CALL OS('X=C     ', X=THIFEMF, C=0.D0 )
!]      CALL OS('X=C     ', X=BETA1, C=0.D0 )
!]      CALL OS('X=C     ', X=VBB, C=0.D0 )
!]      CALL OS('X=C     ', X=ZWI, C=0.D0 )
!]      CALL OS('X=C     ', X=THIFEM, C=0.D0 )
!]      CALL OS('X=C     ', X=DTHIFEM, C=0.D0 )
!]      CALL OS('X=C     ', X=UICE, C=0.D0 )
!]      CALL OS('X=C     ', X=VICE, C=0.D0 )
!]      CALL OS('X=C     ', X=UQX, C=0.D0 )
!]      CALL OS('X=C     ', X=UQY, C=0.D0 )
!]      CALL OS('X=C     ', X=QX, C=0.D0 )
!]      CALL OS('X=C     ', X=QY, C=0.D0 )
!]      CALL OS('X=C     ', X=TMICE, C=0.D0 )
!]      CALL OS('X=C     ', X=CNIEND, C=0.D0 )
!]      CALL OS('X=C     ', X=DETAX, C=0.D0 )
!]      CALL OS('X=C     ', X=DETAY, C=0.D0 )
!]      CALL OS('X=C     ', X=ETA, C=0.D0 )
!]      CALL OS('X=C     ', X=ETAB, C=0.D0 )
!]      CALL OS('X=C     ', X=ZWAT, C=0.D0 )
!]      CALL OS('X=C     ', X=ZICE, C=0.D0 )
!]      CALL OS('X=C     ', X=HBED, C=0.D0 )
!]      CALL OS('X=C     ', X=HICE, C=0.D0 )
!]      CALL OS('X=C     ', X=THIFEMS, C=0.D0 )
!]      CALL OS('X=C     ', X=TISFEM, C=0.D0 )
!]      CALL OS('X=C     ', X=THUN, C=0.D0 )
!]      CALL OS('X=C     ', X=HUN, C=0.D0 )
!]      CALL OS('X=C     ', X=TIWX, C=0.D0 )
!]      CALL OS('X=C     ', X=TIWY, C=0.D0 )

!      !CALL OS('X=C     ', X=JAMFEM, C=0 )
!      CALL BIEF_ALLVEC(2,JAMFEM,'JAMFEM',0,1,0,MESH)
!]      CALL BIEF_ALLVEC(2,ISBORDER,'ISBORDER',MESH%NPOIN,1,0,MESH)
!]      CALL BIEF_ALLVEC(2,JAMFEM,'JAMFEM',MESH%NPOIN,1,0,MESH)
!]      CALL BIEF_ALLVEC(2,ICEREGION,'ICEREGION',MESH%NPOIN,1,0,MESH)

!]      NPOIN = MESH%NPOIN

!      DO I = 1, NPOIN
!        IF(MESH%X%R(I).LT.4001) THEN
!!        IF(MESH%X%R(I).LT.400001) THEN
!           THIFEM%R(I) = 0.3
!           ANFEM%R(I) = 1.0
!!           ISBORDER%I(I) = 0
!           JAMFEM%I(I) = -3
!           ICEREGION%I(I) = 0
!        ELSE
!           THIFEM%R(I) = 0.0
!           ANFEM%R(I) = 0.0
!!           ISBORDER%I(I) = 0
!           JAMFEM%I(I) = 0
!           ICEREGION%I(I) = 0
!        ENDIF
!      ENDDO

!]      DO I = 1, NPOIN
!]        ISBORDER%I(I) = 0
!]        JAMFEM%I(I) = 0
!]        ICEREGION%I(I) = 0
!]      ENDDO
!
!      OPEN(111,FILE='ice input 004.dat')
!      DO I = 1,NPOIN
!        READ(111,*) I1,JAMFEM%I(I),THIFEM%R(I),ANFEM%R(I)
!      ENDDO
!      CLOSE(111)
!
!!      DO I=1,NPOIN
!!         TICE = THIFEM%R(NB)
!!         H%R(I) = H%R(I) - RHO_ICE*TICE
!!      ENDDO
!]      CALL OS( 'X=X-Y   ' , X=H , Y=THIFEM )

!      CASENAME2 = 'OUT'
!      KN2 = 0
!      DO 11 I = LEN(CASENAME2), 1, -1
!       IF ( CASENAME2(I:I) .NE. ' ' ) THEN
!          GOTO 21
!       ELSE
!          KN2 = KN2 + 1
!       END IF
!  11 CONTINUE
!  21 CONTINUE
!
!      INQUIRE ( FILE = CASENAME2(1:KN2), EXIST = LG )
!
!      IF ( .NOT. LG ) THEN
!        WRITE(*,*) '"OUT" FOLDER DOES NOT EXIST UNDER CASE NAME'
!        STOP
!      END IF
!
!      FNAME = 'R'
!      DO I = LEN(FNAME),1,-1
!       IF(FNAME(I:I).NE.'') THEN
!        DO J=2,1000
!          K=J-1
!          IF (K.LT.10) THEN
!             J1=48
!             J2=48
!             J3=K+48
!          ELSEIF (K.LT.100) THEN
!             J1=48
!             J2=K/10+48
!             J3=K-K/10*10+48
!          ELSE
!             J1=K/100+48
!             J2=(K-K/100*100)/10+48
!             J3=MOD(K,10)+48
!          END IF
!          NSERIAL = CHAR(J1)//CHAR(J2)//CHAR(J3)
!!         HDWFILE(J)='\OUT\'//FNAME(1:I)//NSERIAL//'.HDW'
!          PLTFILE(J)='OUT\'//FNAME(1:I)//NSERIAL//'.PLT'
!!         HOTFILE(J)='\OUT\'//FNAME(1:I)//NSERIAL//'.HOT'
!!         STRFILE(J)='\OUT\'//FNAME(1:I)//NSERIAL//'.STR'
!!         PROFILE(J)='\OUT\'//FNAME(1:I)//NSERIAL//'.DAT'
!         ENDDO
!       ENDIF
!      ENDDO
!]      KHDW = 2
!]      KHDI = 2
!]      KHOT = 2
!]      KSTR = 2

!]      IHDW0 = 301
!]      IHDW = 302
!]      IHOT0 = 303
!]      IHOT = 304
!]      ISTR0 = 305
!]      ISTR = 306
!]      IPRO = 307
!]      IPLT = 308




!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!

!
!-----------------------------------------------------------------------
!
!   INITIALISES THE TRACERS
!
!      IF(NTRAC.GT.0) THEN
!        DO ITRAC=1,NTRAC
!          CALL OS('X=C     ',X=T%ADR(ITRAC)%P,C=TRAC0(ITRAC))
!        ENDDO
!      ENDIF
!
!-----------------------------------------------------------------------
!
! INITIALISES THE VISCOSITY
!
!      CALL OS('X=C     ',X=VISC,C=PROPNU)
!
!-----------------------------------------------------------------------
!
      RETURN
      END SUBROUTINE
