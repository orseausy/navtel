!                     ************************
                      SUBROUTINE NOMVAR_KHIONE
!                     ************************
!
     &(TEXTE,TEXTPR,MNEMO,NADVAR,NAMES_ADVAR)
!
!***********************************************************************
! KHIONE
!***********************************************************************
!
!brief    GIVES THE VARIABLE NAMES FOR THE RESULTS AND GEOMETRY
!+                FILES (IN TEXTE) AND FOR THE PREVIOUS COMPUTATION
!+                RESULTS FILE (IN TEXTPR).
!+
!+                TEXTE AND TEXTPR ARE GENERALLY EQUAL EXCEPT IF THE
!+                PREVIOUS COMPUTATION COMES FROM ANOTHER SOFTWARE.
!
!history  S.E.BOURBAN (HRW)
!+        14/06/2017
!+        V7P3
!+   Initial implementation with heat fluxes as an example.
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!| MNEMO          |<--| MNEMONIC FOR 'VARIABLES FOR GRAPHIC OUTPUTS'
!| TEXTE          |<--| SEE ABOVE
!| TEXTPR         |<--| SEE ABOVE
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE DECLARATIONS_SPECIAL
      IMPLICIT NONE
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER, INTENT(IN)              :: NADVAR
      CHARACTER(LEN=32), INTENT(INOUT) :: TEXTE(*),TEXTPR(*)
      CHARACTER(LEN=8),  INTENT(INOUT) :: MNEMO(*)
      CHARACTER(LEN=32), INTENT(IN)    :: NAMES_ADVAR(*)
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      CHARACTER(LEN=2) CHAR2
      INTEGER I,ILAST,INEXT
!
!-----------------------------------------------------------------------
!
!  ENGLISH
!
      IF(LNG.EQ.2) THEN
!
        TEXTE (1 ) = 'SOLRAD CLEAR SKY[SI]            '
        TEXTE (2 ) = 'SOLRAD CLOUDY   [SI]            '
        TEXTE (3 ) = 'NET SOLRAD      [SI]            '
        TEXTE (4 ) = 'EFFECTIVE SOLRAD[SI]            '
        TEXTE (5 ) = 'EVAPO HEAT FLUX [SI]            '
        TEXTE (6 ) = 'CONDUC HEAT FLUX[SI]            '
!
! TEXTPR IS USED TO READ PREVIOUS COMPUTATION FILES.
! IN GENERAL TEXTPR=TEXTE BUT YOU CAN FOLLOW UP A COMPUTATION
! FROM ANOTHER CODE WITH DIFFERENT VARIABLE NAMES, WHICH MUST
! BE GIVEN HERE:
!
        TEXTPR(1 ) = 'SOLRAD CLEAR SKY[SI]            '
        TEXTPR(2 ) = 'SOLRAD CLOUDY   [SI]            '
        TEXTPR(3 ) = 'NET SOLRAD      [SI]            '
        TEXTPR(4 ) = 'EFFECTIVE SOLRAD[SI]            '
        TEXTPR(5 ) = 'EVAPO HEAT FLUX [SI]            '
        TEXTPR(6 ) = 'CONDUC HEAT FLUX[SI]            '
!
!-----------------------------------------------------------------------
!
!  FRANCAIS OU AUTRE
!
      ELSE
!
        TEXTE (1 ) = 'SOLRAD CLEAR SKY[SI]            '
        TEXTE (2 ) = 'SOLRAD CLOUDY   [SI]            '
        TEXTE (3 ) = 'NET SOLRAD      [SI]            '
        TEXTE (4 ) = 'EFFECTIVE SOLRAD[SI]            '
        TEXTE (5 ) = 'EVAPO HEAT FLUX [SI]            '
        TEXTE (6 ) = 'CONDUC HEAT FLUX[SI]            '
!
! TEXTPR SERT A LA LECTURE DES FICHIERS DE CALCULS PRECEDENTS
! A PRIORI TEXTPR=TEXTE MAIS ON PEUT ESSAYER DE FAIRE UNE SUITE
! DE CALCUL A PARTIR D'UN AUTRE CODE.
!
        TEXTPR(1 ) = 'SOLRAD CLEAR SKY[SI]            '
        TEXTPR(2 ) = 'SOLRAD CLOUDY   [SI]            '
        TEXTPR(3 ) = 'NET SOLRAD      [SI]            '
        TEXTPR(4 ) = 'EFFECTIVE SOLRAD[SI]            '
        TEXTPR(5 ) = 'EVAPO HEAT FLUX [SI]            '
        TEXTPR(6 ) = 'CONDUC HEAT FLUX[SI]            '
!
      ENDIF
!
!-----------------------------------------------------------------------
!
!   ALIASES FOR THE VARIABLES IN THE STEERING FILE
!
!     UVCHSBFQTKEDIJMXYPWAGLNORZ
!     THERMAL BUDGET: SOLAR RADIATION UNDER CLEAR SKY
      MNEMO(1)   = 'PHCL    '
!     THERMAL BUDGET: SOLAR RADIATION UNDER CLOUDY SKY
      MNEMO(2)   = 'PHRI    '
!     THERMAL BUDGET: NET SOLAR RADIATION AFTER REFLECTION
      MNEMO(3)   = 'PHPS    '
!     THERMAL BUDGET: EFFECTIVE BACK RADIATION
      MNEMO(4)   = 'PHIB    '
!     THERMAL BUDGET: EVAPORATION HEAT FLUX
      MNEMO(5)   = 'PHIE    '
!     THERMAL BUDGET: CONDUCTIVITY HEAT FLUX
      MNEMO(6)   = 'PHIH    '
!
!     THE LAST RANK 34
!
      ILAST = 6
      INEXT = ILAST+1
!
!-----------------------------------------------------------------------
!
!     DIFFERENTIATORS
!
      IF(NADVAR.GT.0) THEN
        DO I=1,NADVAR
          TEXTE(ILAST+I)  = NAMES_ADVAR(I)
          TEXTPR(ILAST+I) = NAMES_ADVAR(I)
          WRITE(CHAR2,'(I2)') I
          MNEMO(ILAST+I) = 'AD'//ADJUSTL(CHAR2)//'    '
        ENDDO
      ENDIF
!
!-----------------------------------------------------------------------
!
      RETURN
      END

