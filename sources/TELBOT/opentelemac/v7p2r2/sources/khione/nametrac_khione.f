!                      **************************
                       SUBROUTINE NAMETRAC_KHIONE
!                      **************************
!
     &  (NAMETRAC,NTRAC,PROCESS)
!
!
!***********************************************************************
! KHIONE      V7P3
!***********************************************************************
!
!brief    Gives names to tracers added by the ice modelling component
!
!history  F. HUANG (CLARKSON U.) AND S.E. BOURBAN (HRW)
!+        11/11/2016
!+        V7P3
!+        Coupling TELEMAC-2D with KHIONE (ice modelling component)
!+        Initial developments
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!| NAMETRAC |<--| ARRAY OF NAMES OF TRACERS
!| NTRAC    |-->| MODIFYING NUMBER OF TRACER IF NECESARY
!| PROCESS  |-->| ALSO ICEPROCESS, DEFINES THE ICE PROCESSES
!|          |   | - 2: HEAT EXCHANGE WITH AIR, BASED ONLY ON
!|          |   |   TEMPERATURE DIFFERENCE
!|          |   | - 3: HEAT EXCHANGE WITH AIR, BASED ON ALL
!|          |   |   ATMOSPHERIC HEAT SOURCES
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE DECLARATIONS_SPECIAL
      USE DECLARATIONS_KHIONE
!      USE INTERFACE_KHIONE, EX_NAMETRAC_KHIONE => NAMETRAC_KHIONE
!
      IMPLICIT NONE
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
!
      INTEGER          , INTENT(IN   )::  PROCESS
      INTEGER          , INTENT(INOUT)::  NTRAC
      CHARACTER(LEN=32), INTENT(INOUT)::  NAMETRAC(*)
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
!     TODO: THIS SHOULD BE MOVED TO DECLARATIONS_KHIONE,
!     EVEN IF SOME ARE DUPLICATED IN TELEMAC
!      INTEGER :: IND_FT,IND_FC,IND_T
!
      LOGICAL :: FOUND
!
!-----------------------------------------------------------------------
!
      FOUND = .FALSE.
!
!-----------------------------------------------------------------------
!
!     INITIALISATION
!
      IF( PROCESS.EQ.1 ) THEN
        FOUND = .TRUE.
!
        ICETR = 0
!
      ELSE IF( PROCESS*INT(ICEPROCESS/PROCESS).NE.ICEPROCESS ) THEN
        IF(LNG.EQ.1) THEN
          WRITE(LU,11) PROCESS,ICEPROCESS
11    FORMAT(1X,'NAMETRAC_KHIONE: LE PROCESSUS APPELE ',I4,
     &       ' N EST PAS UNE VALEUR MULTIPLICATIVE DE : ',I4)
        ELSE
          WRITE(LU,21) PROCESS,ICEPROCESS
21    FORMAT(1X,'NAMETRAC_KHIONE: PROCESS CALLED ',I4,
     &       ' IS NOT A MULTIPLYING VALUE OF: ',I4)
        ENDIF
        CALL PLANTE(1)
        STOP
      ENDIF
!
!-----------------------------------------------------------------------
!
!       THERMAL BUDGET WITH FRAZIL PRODUCTION
!
      IF( ( 2*INT(ICEPROCESS/2) .EQ. ICEPROCESS ).OR.
     &    ( 3*INT(ICEPROCESS/3) .EQ. ICEPROCESS ) ) THEN
        FOUND = .TRUE.
!
!     1. ~~> FRAZIL
        CALL ADDTRACER(NAMETRAC,NTRAC,
     &    IND_FT,
     &    'FRASIL (TEMP)   ','FRAZIL (TEMP)   ','  mgIce/l       ')
        CALL ADDTRACER(NAMETRAC,NTRAC,
     &    IND_FC,
     &    'FRASIL (CONC)   ','FRAZIL (CONC)   ','  mgIce/l       ')
!     2. ~~> TEMPERATURE
        CALL ADDTRACER(NAMETRAC,NTRAC,
     &    IND_T,
     &    'TEMPERATURE     ','TEMPERATURE     ','   oC           ')
!
      ENDIF
!
!-----------------------------------------------------------------------
!
!     UNKNOWN PROCESS
!
      IF( .NOT.FOUND ) THEN
        IF(LNG.EQ.1) THEN
          WRITE(LU,10) PROCESS
10    FORMAT(1X,'NAMETRAC_KHIONE: MODULE ICE INCONNU : ',I4)
        ELSE
          WRITE(LU,20) PROCESS
20    FORMAT(1X,'NAMETRAC_KHIONE: UNKNOWN ICE MODULE: ',I4)
        ENDIF
        CALL PLANTE(1)
        STOP
      ENDIF
!
!-----------------------------------------------------------------------
!
      RETURN
      END SUBROUTINE
