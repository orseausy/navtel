!                    *******************************
                     SUBROUTINE T3D_WAC_CPL_UPDATE(NIT_ORI)
!                    *******************************
!
!***********************************************************************
! TELEMAC3D
!***********************************************************************
!
!brief    Update data exhanged with tomawac
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
        USE DECLARATIONS_TOMAWAC, ONLY : CPL_WAC_DATA
        USE DECLARATIONS_TELEMAC3D, ONLY : PERCOU_WAC,
     &      U2D, V2D, H, DIRMOY, HM0, TPR5, ORBVEL, FXH, FYH, T2_01,
     &      T2_02, DT, AT
        IMPLICIT NONE
!
!-----------------------------------------------------------------------
!
        INTEGER, INTENT(IN) :: NIT_ORI
!
!-----------------------------------------------------------------------
!
        CPL_WAC_DATA%NIT_TEL = NIT_ORI
        CPL_WAC_DATA%PERCOU_WAC = PERCOU_WAC
        CPL_WAC_DATA%U_TEL => U2D
        CPL_WAC_DATA%V_TEL => V2D
        CPL_WAC_DATA%H_TEL => H
        CPL_WAC_DATA%DIRMOY_TEL => DIRMOY
        CPL_WAC_DATA%HM0_TEL => HM0
        CPL_WAC_DATA%TPR5_TEL => TPR5
        CPL_WAC_DATA%ORBVEL_TEL => ORBVEL
        CPL_WAC_DATA%FX_WAC => FXH
        CPL_WAC_DATA%FY_WAC => FYH
        CPL_WAC_DATA%UV_WAC => T2_01
        CPL_WAC_DATA%VV_WAC => T2_02
        CPL_WAC_DATA%DT_TEL = DT
        IF(NIT_ORI.EQ.0) THEN
          CPL_WAC_DATA%AT_TEL = AT
        ELSE
          CPL_WAC_DATA%AT_TEL = AT-DT
        ENDIF
!
      END SUBROUTINE
