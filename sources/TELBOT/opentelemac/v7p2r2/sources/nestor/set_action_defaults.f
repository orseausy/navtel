!*********************************************************************************************
!*********************************************************************************************
!***                                              ********************************************
!***                                              ********************************************
      SUBROUTINE  Set_Action_Defaults            !********************************************
!***                                              ********************************************
!***                                              ********************************************
     &( A )
!
      USE m_TypeDefs_InterFace
      USE m_TypeDefs_Nestor
!
      IMPLICIT NONE
!
      TYPE(t_Action),INTENT(INOUT)   :: A
!
#ifndef  NESTOR_INTERFACES
!
      !------- local variables ---------------
!
!       none local variables
!
!
!      dbug WRITE(6,*)'?>-------  SR Set_Action_Defaults ----------'
!
!
      A%ActionType     = -11
      A%ActionTypeStr  = 'aaaaaaaaaaa'
      A%ActionDateStr  = 'aaaaaaaaaaa'
      A%FieldDig       = '000_aaaaaaaa'
      A%FieldDigID     = -1
      A%ReferenceLevel = '-1aaaa'!
      A%TimeStart      = -11.1D34         !> in practice here negative values may occure
      A%TimeEnd        = -11.1D34         !  thus to initialise a value far bejond probability is used
      A%TimeRepeat     = -11.1D0
!
      A%DigVolume       =  -0.1D0
      A%DigRate         =  -0.1D0
      A%MaxDig_dz_ts    =  -0.1D0        !> interesting value in case DigPlanar=T
      A%DigDepth        = -11.1D34       !> in practice here negative values may occure
      A%DigPlanar       = .False.        !  thus to initialise a value far bejond probability is used
      A%CritDepth       = -11.1D34       !> in practice here negative values may occure
      A%MinVolume       =  -0.1D0        !  thus to initialise a value far bejond probability is used
      A%MinVolumeRadius =  -0.1D0
      A%FieldDump       = '000_aaaaaaaa'
      A%FieldDumpID     = -1
      A%DumpVolume      = -0.1D0
      A%DumpRate        = -0.1D0
      A%DumpPlanar      = .False.
      A%MaxDump_dz_ts   = -0.1D0         !> interesting value in case DumpPlanar=T
      A%MovedVolume     = -0.1D0
!
!
!         internals
!        
      A%FirstTimeActive = .TRUE.
! 
      A%State    = -11        !> Status of Action: 0 = not yet     active
                              !                    1 = currently   active
                              !                    2 = temporary inactive
                              !                    9 = for ever  inactive
      A%DumpMode = -1         !> Mode: 10 = Dump_by_Time
                              !        11 = Dump_by_Time_Planar
                              !        20 = Dump_by_Rate
                              !        21 = Dump_by_Rate_Planar
      A%nts      = -11        !> number of time steps that is
                              !  needed till the action is finished
      A%tsCount  = -11        !> count time steps while action is working
!     
      A%sumInput = -11.11D0   !> amount of sediment that was carried
                              !  through sediment transport into the field.
      A%fillArea = -11.11D0   !> sum of node areas which are to be filled
                              !  while dumping planar
!     
      A%dt_ts    = -11.11D0   !> time      per  time step
      A%dz_ts    = -11.11D0   !> evolution per  time step
      A%dz_dt    = -11.11D0   !> evolution per  time
      A%dzTot    = -11.11D0   !> total evolution
!     
!     
      A%SaveTime = -11.11D0 !> used to save the time
!     
      A%Solo     = .FALSE.    !> indicates if action is is combined with of other actions
!
!
!      dbug WRITE(6,*)'?>-------  SR Set_Action_Defaults END ------'
      RETURN
!***                                              ********************************************
!***                                              ********************************************
#endif
      END SUBROUTINE Set_Action_Defaults         !********************************************
!***                                              ********************************************
!***                                              ********************************************
!*********************************************************************************************
!*********************************************************************************************
