!*********************************************************************************************
!*********************************************************************************************
!***                                              ********************************************
!***                                              ********************************************
      SUBROUTINE  Backfill_to_level              !********************************************
!***                                              ********************************************
!***                                              ********************************************
     &(   A, F, dt_ts, z_sis, dzCL_sis
     &  , time, m     )
!
!
!
      USE m_TypeDefs_InterFace
      USE m_TypeDefs_Nestor
      USE m_Nestor , ONLY :  ParallelComputing, nGrainClass, ipid
     &                     , npoinGlobal
     &                     , waterLevel_saved_1, waterLevel_saved_2
     &                     , waterLevel_saved_3
!
      USE INTERFACE_PARALLEL, ONLY : P_ISUM
!
!
!
#ifndef NESTOR_INTERFACES
      USE m_Interfaces_Nestor, ONLY :  InfoMessage
     &                               , ErrMsgAndStop
     &                               , Set_RefLevel_by_Profiles
#endif
!
      IMPLICIT NONE
!
      TYPE(t_Action),INTENT(INOUT)   :: A
      TYPE(t_Field) ,INTENT(INOUT)   :: F
      REAL (KIND=R8),INTENT(IN)      :: dt_ts        ! time-step-duration  [ s ]
      REAL (KIND=R8),INTENT(IN)      :: z_sis(:)     ! bottom [m+NN] at time (assumed-shape array)
      TYPE( t_PointerToArrayOfReals )
     &              ,INTENT(INOUT)   :: dzCL_sis(:)
      REAL (KIND=R8),INTENT(IN)      :: time         !  time [s]
      INTEGER       ,INTENT(IN)      :: m            ! number of Action
!
!
!
!
!
#ifndef NESTOR_INTERFACES
!
      !------- local variables ---------------
!
      !EXTERNAL         ::  P_ISUM
      !INTEGER          ::  P_ISUM
!
!
      TYPE(t_String_Length) :: SRname ! name of current Subroutine
!
      INTEGER            :: i, iCL, iMesh, status
      INTEGER            :: nLessNodesToFill, TotalNumNodeToDig
      REAL (KIND=R8)     :: dz_ts, dzFill, remainingHightToFill
!
!      dbug WRITE(6,*)'?>-------  SR Backfill_to_level ------------'
      SRname%s = "Backfill_to_level"   !> subroutine name
      SRname%i =  17                   !> length of name string
!
!
      IF( A%FirstTimeActive )  THEN
        A%State = 1     !> 1 = Action currently active
        CALL InfoMessage( A, m, time )
!
       !ALLOCATE( F%km(        F%nNodes ), stat=status)
        ALLOCATE( F%NodeToDig( F%nNodes ), stat=status)
        IF( A%ReferenceLevel(1:4) /= 'GRID') THEN
          ALLOCATE( F%refZ( F%nNodes ), stat=status)
          ALLOCATE( F%km( F%nNodes ), stat=status)
          F%refZ = 9999.9D0
          F%km   = 9999.9D0
        ENDIF
!
!
        IF(   A%ReferenceLevel(1:8) == 'WATERLVL') THEN
          CALL Set_RefLevel_by_Waterlevel( F, A, m )   ! the result is F%refZ(:)
        ELSE IF( A%ReferenceLevel(1:8) == 'SECTIONS')  THEN
          CALL Set_RefLevel_by_Profiles( F )   ! the result is F%refZ(:)
        ENDIF
!
!
        F%NodeToDig(:) = .FALSE. ! initialisation
        F%nNodeToDig   = 0       ! initialisation
        DO i=1, F%nNodes         ! mark nodes to dig
          iMesh = F%Node(i)      ! mesh index of field node
          IF( z_sis(iMesh) < (F%refZ(i) - A%CritDepth) ) THEN
            F%NodeToDig(i) = .TRUE.
            F%nNodeToDig   = F%nNodeToDig + 1
          ENDIF
        ENDDO
!
        !WRITE(6,*) '?> dz_ts pro time step = ',A%dz_ts  ! debug
!
        A%FirstTimeActive = .FALSE.
      ENDIF  !( FirstTimeActive )
!
!
!
      nLessNodesToFill = 0
!
      IF( F%nNodeToDig > 0 ) THEN
        dz_ts  = dt_ts * A%DumpRate         !> Hight to fill during one time step
        DO i=1, F%nNodes
!
          IF( .NOT. F%NodeToDig(i) ) CYCLE
!
          iMesh = F%Node(i)
!
          dzFill = dz_ts
          remainingHightToFill = F%refZ(i)-A%CritDepth - z_sis(iMesh)
!
          IF( remainingHightToFill < dzFill ) THEN
!
            dzFill = remainingHightToFill
!
            F%NodeToDig(i)  = .FALSE.               !> No more digging for this node.
            nLessNodesToFill = nLessNodesToFill + 1 !> count nodes that are no longer supposed to be dug.
          ENDIF
!
          A%dzCL_ts(:) = dzFill * A%GrainClass(:)    !> Change of z per time step per class
!
          DO iCL=1, nGrainClass                      !> Dump it
            dzCL_sis(iCL)%R(iMesh) =   dzCL_sis(iCL)%R(iMesh)
     &                               + A%dzCL_ts(iCL)
          ENDDO
!
        ENDDO  ! i=1, nNodes
!
      ENDIF  ! IF F%nNodeToDig > 0
!
      F%nNodeToDig = F%nNodeToDig - nLessNodesToFill
!
      !> finalise action
!
      IF( ParallelComputing ) THEN
        TotalNumNodeToDig = P_ISUM( F%nNodeToDig )
      ELSE
        TotalNumNodeToDig = F%nNodeToDig
      ENDIF
!
      IF(      time >= A%TimeEnd
     &   .AND. TotalNumNodeToDig > 0  ) Call ErrMsgAndStop(
     &   "reason:  The period specified is too short.",43
     &  ,"repair:  Increase the priod or the DumpRate!",44 ," ",1
     &  ,"occured in Action number : ", 27, m, SRname, ipid       )
!
!
!
!
      IF( TotalNumNodeToDig  <=  0  ) THEN !> backfilling is accomplished
        A%State = 9                   !  9 = for ever inactive
        DEALLOCATE( F%refZ       , stat=status )
        DEALLOCATE( F%km         , stat=status )
        DEALLOCATE( F%NodeToDig  , stat=status )
!
        CALL InfoMessage( A, m, time )
!
        A%TimeStart = A%TimeStart + (24.0D0 * 3600.0D0)                ! Achtung  consider mor_Faktor
        A%TimeEnd   = A%TimeEnd   + (24.0D0 * 3600.0D0)                ! Achtung
        A%State     = 0  !> Status of Action:  0 = not yet active      ! Achtung
        A%FirstTimeActive = .TRUE.                                     ! Achtung
!
      ENDIF
!
!
!
!
!
!      dbug WRITE(6,*)'?>-------  SR Backfill_to_level END --------'
!
      RETURN
!***                                              ********************************************
!***                                              ********************************************
#endif
      END SUBROUTINE Backfill_to_level           !********************************************
!***                                              ********************************************
!***                                              ********************************************
!*********************************************************************************************
!*********************************************************************************************
