!*********************************************************************************************
!*********************************************************************************************
!***                                              ********************************************
!***                                              ********************************************
      SUBROUTINE  Dump_by_Rate_Planar            !********************************************
!***                                              ********************************************
!***                                              ********************************************
     &( A, dt_ts, z_sis, dzCL_sis, ELAY0, time, m )
!
      USE m_TypeDefs_InterFace
      USE m_TypeDefs_Nestor
      USE m_Nestor , ONLY :  F, ParallelComputing, nGrainClass, ipid
     &                     , npoinGlobal
      USE INTERFACE_PARALLEL, ONLY : P_DSUM, P_ISUM, P_DMAX
!
#ifndef  NESTOR_INTERFACES
      USE m_Interfaces_Nestor, ONLY :  ErrMsgAndStop
     &                               , Calculate_PlanarLevel
     &                               , Set_RefLevel_by_Profiles
     &                               , Set_RefLevel_by_Waterlevel
#endif
!
      IMPLICIT NONE
!
      TYPE(t_Action),INTENT(INOUT) :: A
      REAL (KIND=R8),INTENT(IN)    :: dt_ts      ! time-step-duration  [ s ]
      REAL (KIND=R8),INTENT(IN)    :: z_sis(:)     ! bottom [m+NN] at time (assumed-shape array)
      TYPE( t_PointerToArrayOfReals )
     &              ,INTENT(INOUT) :: dzCL_sis(:)
      REAL (KIND=R8),INTENT(IN)    :: ELAY0        ! activLayerThickness  [ m ]
      REAL (KIND=R8),INTENT(IN)    :: time         ! time [s]
      INTEGER       ,INTENT(IN)    :: m            ! number of Action

#ifndef NESTOR_INTERFACES
!
      !------- local variables ---------------
      INTEGER            :: i, iCL, iMesh, n, status
      INTEGER            :: nLessNodesToDump

      REAL (KIND=R8)     :: dz_ts, dzts, sumDump
!
      TYPE(t_String_Length) :: SRname ! name of current Subroutine
!
!      dbug WRITE(6,*)'?>-------  SR Dump_by_Rate_Planar ----------'
      SRname%s = "Dump_by_Rate_Planar" ! subroutine name
      SRname%i =  19                   ! length of name string
!
      n = A%FieldDumpID
      !__________________________________________________________
      !                                                         /
      !                                                        /
      IF( A%FirstTimeActive )  THEN  !________________________/
!
        ALLOCATE( F(n)%Z( F(n)%nNodes ), stat=status)
        DO i=1, F(n)%nNodes                 !>  copy value to the Field-Structur
          F(n)%Z(i) = z_sis( F(n)%Node(i) ) !>  F(n)%Node(i)= mesh index of field node
        ENDDO
!
        ALLOCATE( F(n)%dZ( F(n)%nNodes ), stat=status)
        F(n)%dZ(:) = 9999.9D0
        
        ALLOCATE( F(n)%NodeToDump( F(n)%nNodes ), stat=status)
        F(n)%NodeToDump(:) = .FALSE.
!
        IF(      A%ReferenceLevel(1:8) == 'WATERLVL') THEN
          CALL Set_RefLevel_by_Waterlevel( F(n), A, m )   ! the result is F(n)%refZ(:)
        ELSE IF( A%ReferenceLevel(1:8) == 'SECTIONS') THEN
          ALLOCATE( F(n)%refZ( F(n)%nNodes ), stat=status)
          ALLOCATE( F(n)%km( F(n)%nNodes )  , stat=status)
          F(n)%refZ(:) = 9999.9D0
          F(n)%km(:)   = 9999.9D0
          CALL Set_RefLevel_by_Profiles( F(n) )           ! the result is F(n)%refZ(:)
        ENDIF
!
        CALL Calculate_PlanarLevel( F(n), A%DigVolume, 1 ) !> 1 => dump;  We use A%DigVolume because
                                                           !  we want to dump the dig volume.
                                                           !  The result is total F%dz(:)and F%NodeToDump(:).
        F(n)%nNodeToDump = 0
        DO i=1, F(n)%nNodes
          IF(F(n)%NodeToDump(i)) F(n)%nNodeToDump = F(n)%nNodeToDump+1
        ENDDO
!
        A%MaxDump_dz_ts = A%DumpRate * dt_ts
!
        A%fillArea = SUM( F(n)%NodeArea(:), MASK=F(n)%NodeToDump(:) )
        
        IF(ParallelComputing) THEN
          A%fillArea       = P_DSUM( A%fillArea )
          F(n)%nNodeToDump = P_ISUM( F(n)%nNodeToDump )
          !WRITE(6,*)' ?>  sum NoTuDu = ', F(n)%nNodeToDump ! debug
          !stop
        ENDIF
!
        A%sumInput        = 0.0D0
!
        !IF( A%Solo ) A%FirstTimeActive = .FALSE.
!
      ENDIF  !(IF A%FirstTimeActive )                         \
      !                                                        \
      !_________________________________________________________\
!
!
!
      IF( .NOT. A%FirstTimeActive )  THEN
!
        !> --- calc hight to dump during current time step ----
        dz_ts = dt_ts * A%DumpRate
        IF( A%DumpVolume  <  dz_ts * A%fillArea ) THEN
          dz_ts = A%DumpVolume / A%fillArea
        ENDIF
!
        dzts             = dz_ts
        sumDump          = 0.0D0
        nLessNodesToDump = 0
!
        DO i=1, F(n)%nNodes  !----- dump one time step -----
        
          IF( .NOT. F(n)%NodeToDump(i) ) CYCLE
!
          iMesh = F(n)%Node(i)     !> mesh index of field node
          dz_ts = dzts
!
          IF( F(n)%dz(i) < dz_ts ) THEN
            dz_ts = F(n)%dz(i)
            F(n)%NodeToDump(i) = .FALSE.
            nLessNodesToDump = nLessNodesToDump + 1
          ENDIF
!
          DO iCL=1, nGrainClass          !< dumping happens here
            dzCL_sis(iCL)%R(iMesh) =   dzCL_sis(iCL)%R(iMesh)
     &                               + A%GrainClass(iCL) * dz_ts
          ENDDO
!
          F(n)%dz(i) = F(n)%dz(i) - dz_ts
!
          sumDump = sumDump + dz_ts * F(n)%NodeArea(i)
!
         !IF(F(n)%NodeToDump(i))WRITE(*,*)'&> NoToDu:',F(n)%nNodeToDump!debug
!
        ENDDO !-- i=1, F(n)%nNodes---------------------------------------------
!
!
        !IF(F(n)%nNodeToDump==0)WRITE(*,*)'&> NoToDu:',F(n)%nNodeToDump !debug
        !WRITE(*,*)'&> NoToDu:==========',F(n)%nNodeToDump !debug
!
        A%fillArea = SUM( F(n)%NodeArea(:), MASK=F(n)%NodeToDump(:) )
!
        IF(ParallelComputing) THEN
          A%fillArea       = P_DSUM( A%fillArea )
          sumDump          = P_DSUM( sumDump )
          nLessNodesToDump = P_ISUM( nLessNodesToDump )
        ENDIF
!
        F(n)%nNodeToDump = F(n)%nNodeToDump - nLessNodesToDump
!
           !WRITE(6,*)' ?>  1 DumpVolume = ', A%DumpVolume ! debug
        A%DumpVolume = A%DumpVolume - sumDump
           !WRITE(6,*)' ?>  2 DumpVolume = ', A%DumpVolume ! debug
!
        IF( F(n)%nNodeToDump == 0 ) A%DumpVolume = -123.4567890D0

!
!
      ENDIF !( .NOT. A%FirstTimeActive )    
!
!
!      dbug WRITE(6,*)'?>-------  SR Dump_by_Rate_Planar END-------'
      RETURN
!***                                              ********************************************
!***                                              ********************************************
#endif
      END SUBROUTINE Dump_by_Rate_Planar         !********************************************
!***                                              ********************************************
!***                                              ********************************************
!*********************************************************************************************
!*********************************************************************************************
