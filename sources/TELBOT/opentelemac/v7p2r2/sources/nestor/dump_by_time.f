!*********************************************************************************************
!*********************************************************************************************
!***                                              ********************************************
!***                                              ********************************************
      SUBROUTINE  Dump_by_Time                   !********************************************
!***                                              ********************************************
!***                                              ********************************************
     &(   A, dt_ts, z_sis, dzCL_sis
     &  , ELAY0, time, m   )
!
      USE m_TypeDefs_InterFace
      USE m_TypeDefs_Nestor
      USE m_Nestor , ONLY :  F, ParallelComputing, nGrainClass, ipid
      USE INTERFACE_PARALLEL, ONLY : P_DSUM, P_DMAX
!
#ifndef  NESTOR_INTERFACES
      USE m_Interfaces_Nestor, ONLY :  InfoMessage
     &                               , ErrMsgAndStop
     &                               , Dealloc_Dump_Field
     &                               , Calculate_PlanarLevel
     &                               , Set_RefLevel_by_Profiles
     &                               , Set_RefLevel_by_Waterlevel
#endif
!
      IMPLICIT NONE
      TYPE(t_Action),INTENT(INOUT) :: A            !> Action
      REAL (KIND=R8),INTENT(IN)    :: dt_ts        !> time-step-duration  [ s ]
      REAL (KIND=R8),INTENT(IN)    :: z_sis(:)     !> bottom [ m+NN ] assumed-shape array
      TYPE( t_PointerToArrayOfReals )
     &              ,INTENT(INOUT) :: dzCL_sis(:)  !> bedload evolution per Class  [ m ]
      REAL (KIND=R8),INTENT(IN)    :: ELAY0        !> activLayerThickness  [ m ]
      REAL (KIND=R8),INTENT(IN)    :: time         !> time [ s ]
      INTEGER       ,INTENT(IN)    :: m            !> number of Action
!
#ifndef  NESTOR_INTERFACES
!
      !------- local variables ---------------
      TYPE(t_String_Length) :: SRname ! name of current Subroutine
      INTEGER               :: i, n, iCL, iMesh !, nodeIndex
      REAL (KIND=R8)        :: areaRatio
!      dbug WRITE(6,*)'?>-------  SR Dump_by_Time -----------------'
      SRname%s = "Dump_by_Time"        !> subroutine name
      SRname%i =  12                   !> length of name string
!
      n = A%FieldDumpID
!
      !_________________________________________________________
      !                                                        /
      IF( A%FirstTimeActive )  THEN  !________________________/
!
        A%Solo = .FALSE.
        IF( A%ActionTypeStr(1:12) == 'Dump_by_time') THEN !> If Dump_by_Time is not part
                                                          !  of other actions we have to do 
                                                          !  some aditional initialisation
          A%Solo    = .TRUE.
          A%State   = 1                                   !> 1 = Action currently active
          A%tsCount = 0
          CALL InfoMessage( A, m, time )
!
          A%nts = INT(   (A%TimeEnd - time) / dt_ts ) !> calculate number of time
                                                      !  steps (nts) to fulfil the Action
          IF( A%nts < 1 ) Call ErrMsgAndStop( " ",           1
     &    ,"reason:  period for this action is too short ", 45
     &    ," ",1,"occured in action number:",25, m, SRname, ipid )
!
          A%dzTot      = A%DumpVolume / F(n)%Area     !> change of z to fulfil the Action
          A%dz_ts      = A%dzTot / DBLE( A%nts )      !> change of z per time step
          A%dzCL_ts(:) = A%dz_ts * A%GrainClass(:)    !> change of z per time step per
                                                      !  Grain CLass
          A%MaxDump_dz_ts = A%dz_ts
!
        ELSE  ! dumping is part of digging action
                                              !> We dig and dump each time step the same volume.
          areaRatio =   F(A%FieldDigID)%Area  !  Thus applying to the evolution per time step (dz_ts): 
     &                / F(A%FieldDumpID)%Area !      dump_dz_ts = areaRatio * dig_dz_ts
!
          A%MaxDump_dz_ts = A%dz_ts * areaRatio !> Here dumping is part of digging
                                                !  thus A%dz_ts is given by the digging.
!
        ENDIF !( A%ActionTypeStr(1:12) == 'Dump_by_time')
!
!
        IF( A%Solo ) A%FirstTimeActive = .FALSE.
!
      ENDIF  !( A%FirstTimeActive )                           \
      !________________________________________________________\
!
      IF( .NOT. A%FirstTimeActive ) THEN
!
        IF( .NOT. A%Solo ) THEN               !> We dig and dump each time step the same volume.
          areaRatio =   F(A%FieldDigID)%Area  !  Thus applying to the evolution per time step (dz_ts): 
     &                / F(A%FieldDumpID)%Area !  dump_dz_ts = areaRatio * dig_dz_ts
!
          A%dzCL_ts(:) = A%dz_ts * A%GrainClass(:) * areaRatio !> Change of z per time step per Grain CLass.
                                                               !  Here dumping is part of dig action
        ENDIF                                                  !  thus A%dz_ts is given by the digging.
!
        IF( A%tsCount <=  A%nts ) THEN
!
!              !> Before dumping we calc. the amount of sediment that was
!              !  transported by morphodynamic during the last time step
!              !  into the field.
!              !  I case there is a futher action operating at the same time
!              !  on this field and it is carried out allready (depends on the
!              !  internal order of execution), then it will
!              !  appear here as sumInput too.
!              DO iCL=1, nGrainClass    !  Only nodes below the planar
!                DO i=1, F%nNodes       !  level are included for it.
!                  iMesh = F%Node(i)    !> mesh index of field node
!                  IF( F%dz(i) > 0.0D0 ) A%sumInput
!         &          =    A%sumInput
!         &             + dzCL_sis(iCL)%R(iMesh) * F%NodeArea(i)
!                ENDDO
!              ENDDO
!
          !----  dump it -------------------------------
          !countDump = countDump + 1                            ! debug
          !WRITE(6,*) 'countDump = ',countDump                  ! debug
          DO iCL=1, nGrainClass
            DO i=1, F(n)%nNodes
              iMesh = F(n)%Node(i)     ! mesh index of field node
              dzCL_sis(iCL)%R(iMesh) =   dzCL_sis(iCL)%R(iMesh)
     &                                 + A%dzCL_ts(iCL) 
            ENDDO
          ENDDO !----------------------------------------
          !mySum = mySum + A%dz_ts                           ! debug
        ENDIF !( A%tsCount <=  A%nts )
!
        !IF( A%tsCount ==  A%nts ) THEN                      ! debug
        !  WRITE(6,*) 'preset dump   volume= ', A%DumpVolume ! debug
        !  WRITE(6,*) '       dumped volume= ', mySum*F%Area ! debug
        !ENDIF                                               ! debug
!
        IF( A%Solo ) THEN
          A%tsCount = A%tsCount + 1
          IF( A%tsCount  >=  A%nts ) THEN !> The action is over
            A%State = 9                   !> 9 = Action for ever inactive
            CALL Dealloc_Dump_Field( A )
            CALL InfoMessage( A, m, time )
           !IF(ParallelComputing) A%sumInput = P_DSUM(A%sumInput)
          ENDIF
        ENDIF !( A%Solo )
!
      ENDIF !( .NOT. A%FirstTimeActive )
!
!      dbug WRITE(6,*)'?>-------  SR Dump_by_Time END -------------'
      RETURN
!***                                              ********************************************
!***                                              ********************************************
#endif
      END SUBROUTINE Dump_by_Time                !********************************************
!***                                              ********************************************
!***                                              ********************************************
!*********************************************************************************************
!*********************************************************************************************
