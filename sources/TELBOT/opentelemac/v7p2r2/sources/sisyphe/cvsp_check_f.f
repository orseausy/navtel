!                    *****************************
                     RECURSIVE FUNCTION CVSP_CHECK_F
!                    *****************************
!
     &(J,K, SOMETEXT) RESULT(RET)
!
!***********************************************************************
! SISYPHE   V7P2                                   16/05/2017
!***********************************************************************
!
!brief   CHECKS IF SUM OF FRACTIONS = 1 FOR
!+        A SECTION IN THE VERTICAL SORTING PROFILE
!
!history UWE MERKEL
!+        19/08/2011
!+        V6P2
!+
!
!history  P. A. TASSI (EDF R&D, LNHE)
!+        12/03/2013
!+        V6P3
!+   Cleaning, cosmetic
!
!history UWE MERKEL, R. KOPMANN (BAW)
!+        19/08/2016 / 2017
!+        V6P3 / V7P2
!+        many changes!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!| J              |<--| INDEX OF A POINT IN MESH
!| K              |<--| INDEX OF A SECTION IN VERTICAL SORTING PROFILE
!| SOMETEXT       |<--| DEBUGING TEXT FOR LOG-OUTPUT
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE BIEF_DEF, ONLY : IPID, NCSIZE
      USE BIEF
      USE DECLARATIONS_SISYPHE
      USE CVSP_OUTPUTFILES, ONLY: CP
!
      USE DECLARATIONS_SPECIAL
      IMPLICIT NONE
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER,          INTENT(IN) :: J
      INTEGER,          INTENT(IN) :: K
      CHARACTER(LEN=10),INTENT(IN) :: SOMETEXT
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      DOUBLE PRECISION TEMP, AT, ERRTOCORR
      INTEGER I, JG
      LOGICAL RET
!-----------------------------------------------------------------------
      AT = DT*LT/PERCOU
      JG = J
      IF (NCSIZE > 1) JG = MESH%KNOLG%I(J)
      RET = .TRUE.
      TEMP = 0.D0
!-----------------------------------------------------------------------
!SUM UP AND SLIGHT CORRECTION
!-----------------------------------------------------------------------
      DO I=1,NSICLA
        IF (PRO_F(J,K,I).LT.0.D0) THEN
          IF (PRO_F(J,K,I).LE.-1.D-7.AND.CP) WRITE(LU,*)
     &     'CVSP CF:PRO_F<0: WARN,J;K;F_I;%: ',
     &     SOMETEXT,JG,K,I,PRO_F(J,K,I)
          IF(PRO_F(J,K,I).GE.-1.D-3) THEN
            PRO_F(J,K,I) = 0.D0
          ELSE
            CALL CVSP_P('./','PRO_F.lt'//SOMETEXT, J)
            WRITE(LU,*) 'CVSP CF:PRO_F<0: ERR,LT,J;K;F_I;%: '
     &                 ,SOMETEXT,LT,JG,K,I,PRO_F(J,K,I)
            CALL PLANTE(1)
            STOP
          ENDIF
        ENDIF
        TEMP = TEMP + PRO_F(J,K,I)
      ENDDO
!-----------------------------------------------------------------------
! CHECK AND CORRECT DEVIATIONS
!-----------------------------------------------------------------------
      IF(ABS(TEMP-1.D0).GT.0.D0) THEN
        IF(ABS(TEMP).LT.1.D-6) THEN
!          SEVERE ERROR, FOR DEBUGGING ONLY RESET to 1 / NSICLA
           IF(CP) WRITE(LU,*) 'CVSP CF: |SUM_ERR|~0;LT;J;K;SUM:'
     &                   ,SOMETEXT,LT,JG,K,TEMP
           RET = .FALSE.
           IF(CP) WRITE(LU,*) 'CVSP  --> NSICLA: ', NSICLA
           DO I=1,NSICLA
             IF(CP) WRITE(LU,*) 'CVSP  --> ;LT;Pnt_J;Lay_K;F_I,%: '
     &                  ,LT,JG,K,I,PRO_F(J,K,I)
             PRO_F(J,K,I) = 1.0D0 / NSICLA
           ENDDO
        ELSEIF(ABS(TEMP-1.D0).GT.1.D-6) THEN
!STRONG DIFFERENCES ARE CORRECTED BY NORMALIZING ALL FRACTIONS
!!!!!!!!!!! README!
!The following warning occured in 0.00025 of all cases
!In almost every case |SUM_ERR| < 2*1.D-5
!To remove this remaining errors would cost 2-3 times higher
!computational expense with no significant global effects
!So the following warning is just meant to remember you on truncation errors
!that still exist
          IF(ABS(TEMP-1.D0).GT.5.D-5) THEN
            IF(CP) WRITE(LU,*) 'CVSP CF: |SUM_ERR|>1.-5 ;LT;J;K;SUM:'
     &                  ,SOMETEXT,LT,JG,K,TEMP
          ENDIF
          RET = .FALSE.
          DO I=1,NSICLA
            IF(PRO_F(J,K,I).GT.0.D0) THEN
              PRO_F(J,K,I) = PRO_F(J,K,I) / TEMP
            ENDIF
          ENDDO
        ELSE
! SLIGHT DIFFERENCES TO 0 ARE CORRECTED BY CHANGING ONLY
! THE FIRST FRACTION BIG ENOUGH
          ERRTOCORR = 1.D0-TEMP
          DO I=1,NSICLA
            IF(PRO_F(J,K,I)+ERRTOCORR.GT.0.D0) THEN
              PRO_F(J,K,I) = PRO_F(J,K,I) + ERRTOCORR
              EXIT
            ENDIF
          ENDDO
        ENDIF
      ENDIF
!-----------------------------------------------------------------------
! RECHECK
!-----------------------------------------------------------------------
      IF(RET .EQV. .FALSE.) THEN
        RET = CVSP_CHECK_F(J,K,'ReCheck   ')
      ENDIF
!
!-----------------------------------------------------------------------
!
      RETURN
      END FUNCTION CVSP_CHECK_F
