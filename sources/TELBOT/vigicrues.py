#           15/01/2020, #V0P1
# ***********************************************************
"""
@author: Sylvain Orseau (Cerema) - sylvain.orseau@tutanota.com
Help:
--------------------------------------------------------------------------------
* Vigicrue.py - # V0

history:
01/2020 - file creation

--------------------------------------------------------------------------------
"""
################################################################################
import pandas as pd
import urllib2
import json
import time
import datetime
import statistics
import logging
import sys

# import datetime as dt
# import numpy as np
# import pandas as pd
import requests
# import warnings
# import xml.etree.ElementTree as et


class hubeau:
    def __init__(self, code_station):
        self.code_station = code_station
        self._baseUrl = "https://hubeau.eaufrance.fr/api/v1/hydrometrie/observations_tr?"
        self._urlParams = {
            #  Station de Vernon
            "code_entite": self.code_station,
            "grandeur_hydro": "Q",
            "fields": "date_obs,resultat_obs",
            "size": str(5000)
        }
        #  URL finale (_baseUrl + _urlParams)
        self.url = self._baseUrl + "&".join(["{}={}".format(k, v) for k, v in self._urlParams.items()])
        self._fakeUserAgent = 'Mozilla/5.0'
        # ---
        self.date = list()
        self.river_discharge = list()
        self.river_discharge_daily = list()
        self.time = list()

    def load(self):
        logging.debug("Opening " + self.url)
        try:
            response = urllib2.urlopen(urllib2.Request(self.url, headers={'User-Agent': self._fakeUserAgent}))
            text = response.read().decode('utf-8')
            logging.debug("Response: "+text)
            return json.loads(text)
        except urllib2.URLError as e:
            print e.reason
            # logging.critical(e)
            sys.exit(1)

    def parse(self, resultats):
        # ~ resultats = self._load_url_as_json ()
        try:
            self.date = [data["date_obs"] for data in resultats["data"]]
            self.time = pd.to_datetime(self.date, format="%Y-%m-%dT%H:%M:%SZ")
            self.river_discharge = [data["resultat_obs"]/1000 for data in resultats["data"]]
            data = pd.DataFrame({"Time": self.time, "River Discharge": self.river_discharge})
            data_filtered = data.set_index("Time").resample("D").mean().reset_index()
            self.time_daily = data_filtered["Time"]
            self.river_discharge_daily = (data_filtered["River Discharge"]).astype(int)
            logging.debug("River discharge pour la station %s : %f l/s", self._urlParams["code_entite"],
                          self.river_discharge)
            return self.river_discharge_daily
        except statistics.StatisticsError as e:
            logging.error("No data founded for " + self._urlParams["code_entite"])


dordogne = hubeau("P555001001")
res = dordogne.load()
data = dordogne.parse(res)
print dordogne.river_discharge_daily

# # >> Private functions
# def load_rss(url, xml_filename):
#     response = requests.get(url)
#     with open(xml_filename, "wb") as f:
#         f.write(response.content)
#     f.close()
#     if response.status_code != 200:
#         return "FAILED"
#     else:
#         return "COMPLETED"
#
#
# def parse_xml(xml_filename):
#     tree = et.parse(xml_filename)
#     root = tree.getroot()
#     time = []
#     river_discharge = []
#     for child in root.iter():
#         if "DtObsHydro" in child.tag:
#             time.append(child.text)
#         elif "ResObsHydro" in child.tag:
#             river_discharge.append(child.text)
#         else:
#             pass
#     return time, river_discharge
#
#
# ################################################################################
# class Vigicrues:
#     def __init__(self, url):
#         self.url = url
#         self.observation_hydro = None
#         self.raw_data = None
#         self.status = str()
#         self.time = None
#         self.time_daily = None
#         self.time_py = None
#         self.values = None
#         self.values_daily = None
#
#     def load(self):
#         self.status = load_rss(self.url, dt.datetime.strftime(dt.datetime.now(), "%d%m%Y") + "_vigicrue.xml")
#         self.time, self.values = parse_xml(dt.datetime.strftime(dt.datetime.now(), "%d%m%Y") + "_vigicrue.xml")
#         self.time_py = pd.to_datetime(self.time, format="%Y-%m-%dT%H:%M:%S+00:00")
#         self.values = np.asarray(self.values).astype(float)
#         data = pd.DataFrame({"Time": self.time_py, "River Discharge": self.values})
#         data_filtered = data.set_index("Time").resample("D").mean().reset_index()
#         self.time_daily = data_filtered["Time"]
#         self.values_daily = (data_filtered["River Discharge"]).astype(int)
#         # Check values
#         if any(self.values_daily < 0):
#             self.status = "FAILED"
#             warnings.warn("Some of river discharge values are negative!")
#
#
# ################################################################################
