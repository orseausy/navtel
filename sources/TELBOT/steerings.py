#!/home/$USER/anaconda3/envs/py27/bin python
# -*- coding: utf-8 -*-
"""
@author: Sylvain Orseau (sylvain.orseau@tutanota.com)
"""
##################################################################################
# -- Import public modules
import collections
import datetime as dt
import os
import re
import textwrap


# -- Functions
def value_format(value):
    return "%.1f" % value


def contains_numbers(input_string):
    return any(char.isdigit() for char in input_string)


##################################################################################
class OrderedClassMembers(type):
    @classmethod
    def __prepare__(self, name, bases):
        return collections.OrderedDict()

    def __new__(self, name, bases, classdict):
        classdict['__ordered__'] = [key for key in classdict.keys()
                if key not in ('__module__', '__qualname__')]
        return type.__new__(self, name, bases, classdict)


class Telemac2dSteeringFile(metaclass=OrderedClassMembers):
    def __init__(self):
        # -----| FILES |----------
        self.geometry_file = "geom.slf"
        self.boundary_conditions_file = "BoundaryConditions.cli"
        self.harmonic_constants_file = "HarmonicConstants.CL"
        self.liquid_boundaries_file = "q_navtel.txt"
        self.formatted_data_file_1 = "surge.txt"
        self.fortran_file = "'user_fortran'"
        self.results_file = "t2d_navtel_" + dt.datetime.strftime(dt.datetime.now(), "%d%m%Y") + ".slf"
        # -----| TIME DISCRETISATION |----------
        self.number_of_time_steps = 155520
        self.time_step = 10
        self.original_date_of_time = (dt.datetime.now() - dt.timedelta(days=15)).strftime("%Y;%m;%d")
        Date = dt.datetime.strptime(self.original_date_of_time, "%Y;%m;%d")
        t0 = Date + dt.timedelta(hours=8)
        self.original_hour_of_time = t0.strftime("%H;%M;%S")
        # -----| INFORMATIONS |----------
        self.title = "'NavTEL #V0 TELEMAC-2HD'"
        self.listing_printout_period = 30
        self.graphic_printout_period = 30
        self.variables_for_graphic_printouts = "'U, V, W, H, S, T1'"
        self.mass_balance = "YES"
        # -----| PROPAGATION |----------
        self.turbulence_model = 1
        self.velocity_diffusivity = 1.0
        # -----| BOTTOM FRICTION |----------
        self.law_of_bottom_friction = 3
        self.friction_coefficient = 60
        # -----| SALINITY |----------
        self.density_effects = "YES"
        self.number_of_tracers = 1
        self.names_of_tracers = "'SALINITY        KG/M3           '"
        self.coefficient_for_diffusion_of_tracers = 1.0
        self.initial_values_of_tracers = 0.0
        self.scheme_for_advection_of_tracers = 14
        self.prescribed_tracers_values = [35, 0, 0]
        # -----| INITIAL CONDITIONS |----------
        self.initial_conditions = "'CONSTANT ELEVATION'"
        self.initial_elevation = 3.5
        # -----| BOUNDARY CONDITIONS |----------
        self.prescribed_flowrates = [0, 0, 0]
        self.prescribed_velocities = [0, 0, 0]
        self.velocity_profiles = "1;1;1"
        self.option_for_liquid_boundaries = "2;1;1"
        self.option_for_tidal_boundary_conditions = "1;0;0"
        self.tidal_data_base = 3
        self.coefficient_to_calibrate_sea_level = 0.4438
        # -----| NUMERICAL PARAMETERS |----------
        self.treatment_of_the_linear_system = 2
        self.implicitation_for_depth = 0.6
        self.mass_lumping_on_h = 1
        self.initial_guess_for_h = 1
        self.implicitation_for_velocity = 0.6
        self.supg_option = "1;0"
        self.solver_option = 3
        self.solver = 1
        self.solver_accuracy = 1.E-4
        self.maximum_number_of_iterations_for_solver = 500
        self.tidal_flats = "YES"
        self.continuity_correction = "YES"
        self.free_surface_gradient_compatibility = 0.9
        self.treatment_of_negative_depths = 2
        # -----| SISYPHE |----------
        self.coupling_with = "'SISYPHE'"
        self.sisyphe_steering_file = "sis_navtel.cas"
        self.filename = None
        self.variable_names = []
        self.values = []

    def write(self, name):
        self.filename = name
        d = collections.OrderedDict()
        for attr, val in self.__dict__.items():
            self.variable_names.append(re.sub(r"[\W_]", r" ", attr).upper())
            self.values.append(val)
        #   >>> Writing
        line_break = "\n"
        l1 = "/========================================\n"
        l2 = "/---------------------------------------------------------------------\n"
        first_lines = [l1, "/		TELEMAC-2D\n", l1, "PARALLEL PROCESSORS		= 12\n"]
        headers = [["\n/ :: FILES\n", l2],
              ["\n/ :: TIME DISCRETISATION\n", l2],
              ["\n/ :: GENERAL INFORMATIONS\n", l2],
              ["\n/ :: PROPAGATION\n", l2],
              ["\n/ :: BOTTOM FRICTION\n", l2],
              ["\n/ :: SALINITY\n", l2],
              ["\n/ :: INITIAL CONDITIONS\n", l2],
              ["\n/ :: BOUNDARY CONDITIONS\n", l2],
              ["\n/ :: NUMERICAL PARAMETERS\n", l2],
              ["\n/ :: SISYPHE\n", l2]]
        str_index = ["NUMBER OF TIME STEPS", "TITLE", "TURBULENCE MODEL", "LAW OF BOTTOM FRICTION", "DENSITY EFFECTS",
                    "INITIAL CONDITIONS", "PRESCRIBED FLOWRATES", "TREATMENT OF THE LINEAR SYSTEM", "COUPLING WITH"]
        # Write file
        # #####################################
        i_1 = [self.variable_names.index(x) for x in str_index]
        with open(self.filename, "w+") as f:
            for i in first_lines:
                f.write(i)
            f.write(line_break)
            [f.write(x) for x in headers[0]]
            k = 1
            for i, j in zip(self.variable_names[:-3], self.values[:-3]):
                if self.variable_names.index(i) in i_1:
                    f.write(line_break)
                    [f.write(x) for x in headers[k]]
                    if isinstance(j, list):
                        f.write(("{} = ".format(i) + ";".join("{:.2f}".format(number) for number in j) + "\n"))
                        k = k+1
                    else:
                        f.write("%s = %s\n" % (i, j))
                        k = k + 1
                else:
                    if ("MASS BALANCE" in i) or ("MASS LUMPING" in i):
                        i = i.replace(" ", "-", 1)
                        f.write("%s = %s\n" % (i, j))
                    else:
                        if isinstance(j, list):
                            f.write(("{} = ".format(i) + ";".join("{:.2f}".format(number) for number in j) + "\n"))
                        else:
                            f.write("%s = %s\n" % (i, j))


class SisypheSteeringFile(metaclass=OrderedClassMembers):
    def __init__(self):
        # -----| FILES |----------
        self.geometry_file = "geom.slf"
        self.boundary_conditions_file = "BoundaryConditions.cli"
        self.fortran_file = "'user_fortran'"
        self.results_file = "sis_navtel_" + dt.datetime.strftime(dt.datetime.now(), "%d%m%Y") + ".slf"
        # -----| GENERAL INFORMATIONS |----------
        self.title = "'NavTEL V0 - SISYPHE'"
        self.variables_for_graphic_printouts = "'CS1, CS2, TOB'"
        # -----| SEDIMENT PROPERTIES |----------
        self.mixed_sediment = "YES"
        self.number_of_size_classes_of_bed_material = 2
        self.cohesive_sediments = "NO; YES"
        self.sediment_diameters = [0.0004, 0.00005]
        self.settling_velocities = [0.01, 0.00075]
        # -----| SEDIMENT TRANSPORT |----------
        self.bed_load = "YES"
        self.bed_load_transport_formula = 1
        self.mpm_coefficient = 0.
        self.suspension = "YES"
        self.mass_concentration = "YES"
        self.concentration_per_class_at_boundaries = [0., 0., 0., 7.55E-5, 0., 7.55E-5]
        self.initial_suspension_concentrations = [0., 0.]
        self.treatment_of_fluxes_at_the_boundaries = "2;2;2"
        self.law_of_bottom_friction = 3
        self.partheniades_constant = 0.002
        self.critical_shear_velocity_for_mud_deposition = 10
        self.minimal_value_of_the_water_height = 0.01
        # -----| CONSOLIDATION |----------
        self.mud_consolidation = "YES"
        self.consolidation_model = 1
        self.number_of_layers_of_the_consolidation_model = 20
        self.mud_concentration_per_layer = "75;93;111;129;147;165;183;201;219;237; " \
                                           "255;273;291;309;327;363;399;435;471;507"
        self.critical_erosion_shear_stress_of_the_mud = "0.03;0.05;0.08;0.1;0.14;0.17;0.22;0.26;0.31;0.37; " \
                                                        "0.43;0.5;0.58;0.65;0.74;0.93;1.14;1.38;1.64;1.94"
        self.mass_transfer_per_layer = "0.003;0.003;0.003;0.002;0.001;0.0005;0.00005;0.00003;0.0003; " \
                                       "0.00003;0.00003;0.00003;0.00003;0.00001;0.000001;0.000001; " \
                                       "0.000001;0.000001;0"
        # -----| DREDGING AND DISPOSAL OPERATIONS |----------
        self.nestor = "NO"
        # -----| NUMERICAL OPERATORS |----------
        self.stationary_mode = "YES"
        self.mass_lumping = "YES"
        self.zero = 1E-12
        self.teta = 0.5
        self.reference_concentration_formula = 1
        self.type_of_advection = 14
        self.solver_for_suspension = 1
        self.solver_accuracy_for_suspension = 1E-10
        self.filename = None
        self.variable_names = []
        self.values = []

    def write(self, name):
        self.filename = name
        d = collections.OrderedDict()
        for attr, val in self.__dict__.items():
            self.variable_names.append(re.sub(r"[\W_]", r" ", attr).upper())
            self.values.append(val)
        # >> Writing
        line_break = "\n"
        l1 = "/========================================\n"
        l2 = "/---------------------------------------------------------------------\n"
        first_line = [l1, "/		SISYPHE\n", l1, "MASS-BALANCE		= YES\n"]
        headers = [["\n/ :: FILES\n", l2],
                   ["\n/ :: GENERAL INFORMATIONS\n", l2],
                   ["\n/ :: SEDIMENT PROPERTIES\n", l2],
                   ["\n/ :: SEDIMENT TRANSPORT\n", l2],
                   ["\n/ :: CONSOLIDATION\n", l2],
                   ["\n/ :: DREDGING/DISPOSAL OPERATIONS\n", l2],
                   ["\n/ :: NUMERICAL PARAMETERS\n", l2]]
        str_index = ["TITLE", "MIXED SEDIMENT", "BED LOAD", "MUD CONSOLIDATION", "NESTOR", "STATIONARY MODE"]
        # Write file
        # #####################################
        i_1 = [self.variable_names.index(x) for x in str_index]
        with open(self.filename, "w+") as f:
            for i in first_line:
                f.write(i)
            f.write(line_break)
            [f.write(x) for x in headers[0]]
            k = 1
            for i, j in zip(self.variable_names[:-3], self.values[:-3]):
                if self.variable_names.index(i) in i_1:
                    f.write(line_break)
                    [f.write(x) for x in headers[k]]
                    if isinstance(j, list):
                        f.write(("{} = ".format(i) + ";".join("{:.2f}".format(number) for number in j) + "\n"))
                        k = k+1
                    else:
                        f.write("%s = %s\n" % (i, j))
                        k = k + 1
                else:
                    if ("MASS BALANCE" in i) or ("MASS LUMPING" in i) or ("BED LOAD TRANSPORT" in i):
                        i = i.replace(" ", "-", 1)
                        f.write("%s = %s\n" % (i, j))
                    elif "NUMBER OF SIZE CLASSES" in i:
                        i = i.replace("SIZE ", "SIZE-")
                        f.write("%s = %s\n" % (i, j))
                    else:
                        if isinstance(j, list):
                            f.write(("{} = ".format(i) + ";".join("{:.6f}".format(number) for number in j) + "\n"))
                        else:
                            f.write("%s = %s\n" % (i, j))

        with open(self.filename, "r") as steering_file:
            text = steering_file.read()
            lines = text.splitlines(keepends=True)

        with open(self.filename, "w") as steering_file:
            for i in lines:
                steering_file.write("{}\n".format(textwrap.fill(i, width=100, replace_whitespace=False)))


##################################################################################
steering_t2d = Telemac2dSteeringFile()
steering_t2d.write("t2d_navtel.cas")
steering_sis = SisypheSteeringFile()
steering_sis.write("sis_navtel.cas")
