#!/home/$USER/anaconda3/envs/py27/bin python
# -*- coding: utf-8 -*-
# ***********************************************************
# Project: GIRONDE XL
# Authors: Sylvain Orseau
# history: Sylvain Orseau (Cerema)
#           11/01/2019, #V0P1
# ***********************************************************
"""
@author: Sylvain Orseau (Cerema) - sylvain.orseau@tutanota.com

"""
################################################################################
# Import public modules
import email
from email import encoders
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEBase import MIMEBase
import imaplib
import os
import re
import sys
import smtplib

# Path file
path_file = os.getcwd()
path_data = os.path.join(path_file[:-8] + "/examples/Gironde/GPMB/data")
path_data = "/home/orseausy/Git/navtel/examples/Gironde/GPMB/data"


################################################################################
class Mail:
    user = "routier.gpmb@gmail.com"
    password = "girondexl3d"
    # -- Read
    properties = imaplib.IMAP4_SSL("imap.gmail.com", "993")
    properties.login(user, password)
    properties.select()
    # -- Send
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login(user, password)

    def __init__(self):
        self.to_addr = " routier@vigiesip.eu"
        self.confirmation = None
        self.message = None
        self.navtel_version = str()
        self.report_navigation = str()
        self.report1 = str()
        self.report2 = str()
        type, data = self.properties.search(None, "ALL")
        self.id = int()
        self.ids = data[0].split()
        self.number = len(self.ids)
        self.email_date = str()
        self.email_from = str()
        self.email_subject = str()
        self.maximum_allowable_draft = str()
        self.request = list()
        self.simulation = str()
        self.status_navtel = str()
        self.status_external_forcings = str()
        self.body = list()
        # -- Variables
        self.date = str()
        self.hour = str()
        self.harbor = str()
        self.route = str()
        self.speed = str()
        self.category = str()
        self.length = str()
        self.breadth = str()
        self.draft = str()
        self.block_coefficient = float()

    def check(self):
        if not self.properties.search(None, "UnSeen")[1][0]:
            sys.exit()
        else:
            type, data = self.properties.fetch(self.ids[-1], "(RFC822)")
            raw_email = data[0][1]
            msg = email.message_from_string(raw_email.decode('utf-8', "strict"))
            if "Confirmation" in msg["subject"]:
                sys.exit()
            else:
                # Extract basic information
                self.email_subject = msg["subject"]
                self.email_from = msg["from"]
                self.email_date = msg["date"]
                self.request.append(self.email_subject)
                # self.id = str(random.randint(0, 10000))
                for part in msg.walk():
                    if part.get_content_type() == "text/plain":
                        self.body = part.get_payload(decode=True)
                    elif part.get_content_type() == "text/html":
                        continue

    def parse(self):
        for num in self.ids:
            type, data = self.properties.fetch(num, "(RFC822)")
            raw_email = data[0][1]
            msg = email.message_from_string(raw_email.decode("utf-8", "strict"))
            # Extract basic information
            self.email_subject = msg["subject"]
            self.email_from = msg["from"]
            self.email_date = msg["date"]
            self.request.append(self.email_subject)
            # self.id = str(random.randint(0, 10000))
            # Extract body of each request
            for part in msg.walk():
                if part.get_content_type() == "text/plain":
                    self.body.append(part.get_payload(decode=True))
                elif part.get_content_type() == "text/html":
                    continue

    def extract(self, body):
        self.date = str(re.search("<DATE>(.*)</DATE>", self.body).group(1))
        self.hour = str(re.search("<HOUR>(.*)</HOUR>", self.body).group(1))
        self.harbor = str(re.search("<HARBOR>(.*)</HARBOR>", self.body).group(1))
        self.route = str(re.search("<ROUTE>(.*)</ROUTE>", self.body).group(1))
        self.speed = int(re.search("<SPEED>(.*)</SPEED>", self.body).group(1))
        self.category = str(re.search("<CATEGORIE>(.*)</CATEGORIE>", self.body).group(1))
        self.length = float(re.search("<LENGTH>(.*)</LENGTH>", self.body).group(1))
        self.breadth = float(re.search("<BREADTH>(.*)</BREADTH>", self.body).group(1))
        self.draft = float(re.search("<DRAUGHT>(.*)</DRAUGHT>", self.body).group(1))
        # -- Determine block coefficient
        if "P.C." in self.category:
            self.block_coefficient = 0.6
        elif "CARGO" in self.category:
            self.block_coefficient = 0.7
        elif "CHIMIQUIER" or "PETRO" in self.category:
            self.block_coefficient = 0.8
        elif "VRAQUIER" in self.category:
            self.block_coefficient = 0.9
        else:
            self.block_coefficient = 0.75

    def send_confirmation(self, version):
        self.navtel_version = version
        # ---
        self.confirmation = MIMEMultipart()
        self.confirmation['From'] = self.user
        self.confirmation['To'] = self.email_from
        self.confirmation['Subject'] = '[' + self.email_subject + '] Receipt Confirmation'
        body = "Thank you for your request!\n" \
               "NavTEL is currently preparing ship route and the prediction of underkeel clearances " \
               "for a {} traject on {}.\n" \
               "You will notify you when it has been finished.\n\n" \
               "Best regards,\n"\
               "NavTEL v.{}"
        self.confirmation.attach(MIMEText(body.format(self.route.lower(),
                                                      self.date + " " + self.hour,
                                                      self.navtel_version), "plain"))
        text = self.confirmation.as_string()
        self.server.sendmail(self.user, self.email_from, text)

    def send(self, outputs, pdf_file1, *pdf_file2):
        self.status_navtel = outputs[0]
        self.status_external_forcings = outputs[1]
        self.simulation = outputs[2]
        self.maximum_allowable_draft = outputs[3]
        # ---
        if not pdf_file2:
            attachments = [pdf_file1]
        else:
            attachments = [pdf_file1, pdf_file2[0]]
        self.message = MIMEMultipart()
        self.message['From'] = self.user
        self.message['To'] = self.email_from
        self.message['Subject'] = '[' + self.email_subject + '] NavTEL'
        body = "You will find in attachment the output of the simulation for a {} traject " \
               "to {} on {}.\n\n" \
               "DETAILS\n" \
               "==============================\n" \
               "<DATE>{}</DATE>\n" \
               "<HOUR>{}</HOUR >\n" \
               "<HARBOR>{}</HARBOR>\n" \
               "<ROUTE>{}</ROUTE>\n" \
               "<SPEED>{}</SPEED>\n" \
               "<CATEGORIE>{}</CATEGORIE>\n" \
               "<LENGTH>{}</LENGTH>\n" \
               "<BREADTH>{}</BREADTH>\n" \
               "<MAXIMUM ALLOWABLE DRAFT>{}</MAXIMUM ALLOWABLE DRAFT>\n" \
               "<SIMULATION>{}</SIMULATION>\n" \
               "<EXTERNAL FORCINGS>{}</EXTERNAL FORCINGS>\n" \
               "<STATUS>{}</STATUS>\n\n" \
               "Best regards,\n" \
               "NavTEL v.{}"
        self.message.attach(MIMEText(body.format(self.route.lower(),
                                                 self.harbor.capitalize(),
                                                 self.date + " " + self.hour,
                                                 self.date,
                                                 self.hour,
                                                 self.harbor,
                                                 self.route,
                                                 self.speed,
                                                 self.category,
                                                 self.length,
                                                 self.breadth,
                                                 self.maximum_allowable_draft,
                                                 self.simulation,
                                                 self.status_external_forcings,
                                                 self.status_navtel,
                                                 self.navtel_version), "plain"))
        # ---
        if attachments is not None:
            for pdf_file in attachments:
                try:
                    pathname = open(path_data + "/" + pdf_file, "rb")
                    print path_data
                    part = MIMEBase("application", "octet-stream")
                    part.set_payload(pathname.read())
                    encoders.encode_base64(part)
                    part.add_header("Content-Disposition", "attachment", filename=pdf_file)
                    self.message.attach(part)
                except Exception as e:
                    print e
                    raise RuntimeError('No pdf attached.')
        # ---
        text = self.message.as_string()
        self.server.sendmail(self.user, self.email_from, text)
        self.server.quit()


################################################################################