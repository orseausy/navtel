#!/home/$USER/anaconda3/envs/py27/bin python
# -*- coding: utf-8 -*-
# ***********************************************************
# Project: GIRONDE XL
# Authors: Sylvain Orseau
# history: Sylvain Orseau (Cerema)
#           15/01/2020, #V0P1
# ***********************************************************
"""
@author: Sylvain Orseau (Cerema) - sylvain.orseau@tutanota.com
"""

################################################################################
# -- Import modules
import numpy as np


# -- Private functions
def powl(my_list, power):
    return np.array(pow(row, power) for row in my_list)


################################################################################
class MethodException(Exception):
    pass


class Squat(object):
    def __init__(self,
                 length,
                 breadth,
                 draft,
                 block_coefficient,
                 speed,
                 water_depth,
                 width,
                 velocity,
                 density_gradient=0.0006):
        # --
        self.ship_length = float(length)
        self.ship_breadth = float(breadth)
        self.ship_draft = float(draft)
        self.block_coefficient = float(block_coefficient)
        self.ship_speed_knot = float(speed)
        self.ship_speed_si = self.ship_speed_knot * 0.514444
        self.water_depth = np.asarray(water_depth, dtype=float)
        self.channel_width = width
        self.current_velocity = velocity
        if density_gradient:
            self.density_gradient = density_gradient
        # ---
        self.blockage_factor = (self.ship_breadth * self.ship_draft) / (self.channel_width * self.water_depth)
        self.channel_cross_section_area = self.channel_width * self.water_depth
        self.midship_cross_section_area = .98 * self.ship_breadth * self.ship_draft
        self.ship_displacement_volume = self.block_coefficient * self.ship_breadth * self.ship_length * self.ship_draft
        self.gravitational_acceleration = 9.806
        self.froude_number = self.ship_speed_si / np.sqrt(self.gravitational_acceleration * self.water_depth)
        self.ship_speed_relative = self.ship_speed_si - self.current_velocity
        # ---
        self.alpha_1 = 0.054
        self.alpha_2 = 0.036
        self.barrass_coefficient = float()
        self.eryuzlu_correction_factor = float()
        self.final_values = list()
        self.initial_values = list()
        self.maximum_value = float()
        self.method = int()
        self.medium_values = np.empty(np.size(self.water_depth), dtype=float)
        self.medium_values_opt = np.empty(np.size(self.water_depth), dtype=float)

    def compute(self, method, density=False):
        """
        Compute ship squat depending ship characteristics and channel configuration.

        @param method (int)
        @param density (float)
        """
        # -- Initialize
        self.method = method

        # --
        # Barrass formulation
        if self.method == 1:
            for count, value in enumerate(self.water_depth):
                if 1.1 <= value / self.ship_draft <= 1.4:
                    pass
                else:
                    print "Becareful your are outside the constraints of this method!"
                    print count, value
                if self.blockage_factor[count] < 0.1:
                    self.barrass_coefficient = 1
                elif self.blockage_factor[count] >= 0.1:
                    self.barrass_coefficient = 5.74 * np.power(self.blockage_factor[count], 0.76)
                else:
                    self.barrass_coefficient = np.nan
                print self.barrass_coefficient
                self.medium_values[count] = self.barrass_coefficient * self.block_coefficient
                self.medium_values[count] = self.medium_values[count] * np.power(self.ship_speed_knot, 2) / 100
        # ICORELS formulation
        elif self.method == 2:
            self.medium_values = 2.4 * (self.ship_displacement_volume / np.power(self.ship_length, 2))
            self.medium_values = self.medium_values * np.power(self.froude_number, 2)
            self.medium_values = self.medium_values / (np.sqrt(1 - np.power(self.froude_number, 2)))
        # Yoshimura formulation
        elif self.method == 3:
            self.medium_values = ((.7 + 1.5 * (1 / (self.water_depth / self.ship_draft)))
                                  * (self.block_coefficient / (self.ship_length / self.ship_breadth))
                                  + 15 * (1 / (self.water_depth / self.ship_draft))
                                  * np.power((self.block_coefficient / (self.ship_length / self.ship_breadth)), 3))\
                                 * (np.power(self.ship_speed_si, 2) / self.gravitational_acceleration)
        elif self.method == 4:
            # Eryuzlu
            if self.channel_width / self.ship_breadth >= 9.61:
                self.eryuzlu_correction_factor = 3.1 / np.sqrt(self.channel_width / self.ship_breadth)
            else:
                self.eryuzlu_correction_factor = 1
            self.medium_values = .298 * (np.power(self.water_depth, 2) / self.ship_draft) * \
                          np.power(self.ship_speed / np.sqrt(self.gravitational_acceleration * self.ship_draft), 2.289)\
                          * np.power(self.water_depth / self.ship_draft, -2.972) * \
                          self.eryuzlu_correction_factor
        elif self.method == 5:
            # Huuska / Guliev
            self.medium_values = 0
        elif self.method == 6:
            # Romisch
            ship_critical_speed = .58 * np.power((self.water_depth * self.ship_length)
                                                      / (self.ship_draft * self.ship_breadth), .125)\
                                       * np.sqrt(self.gravitational_acceleration * self.water_depth)
            ship_speed_correction_factor = 8 * (self.ship_speed / ship_critical_speed) *\
                                                (np.power(((self.ship_speed / ship_critical_speed) - .5), 4)
                                                 + .0625)
            body_shape_correction_factor = np.power((10 * self.block_coefficient) /
                                                         (self.ship_length / self.ship_breadth), 2)
            squat_correction_factor = .155 * np.sqrt(self.water_depth / self.ship_draft)
            self.stern_values = ship_speed_correction_factor * body_shape_correction_factor * squat_correction_factor
            self.bow_values = ship_speed_correction_factor * body_shape_correction_factor * squat_correction_factor * \
                              self.ship_draft
        elif self.method == 7:
            # Soukhomel and Zass
            if 3.5 <= (self.ship_length / self.ship_breadth) <= 9:
                sz_coefficient = .0143 * np.power((self.ship_length / self.ship_breadth), -1.11)
            else:
                sz_coefficient = np.nan
            if (self.water_depth / self.ship_draft) >= 1.4:
                self.medium_values = 12.96 * sz_coefficient * np.sqrt(self.ship_draft / self.water_depth) * \
                                     np.power(self.current_velocity, 2)
            else:
                self.medium_values = 12.96 * sz_coefficient * np.power(self.current_velocity, 2)
            # # For ship with reduced breadth:
            # if 3.5 <= (self.ship_length / self.ship_breadth) <= 5:
            #     self.stern_values = self.medium_values * 1.5
            # elif 5 <= (self.ship_length / self.ship_breadth) <= 7:
            #     self.stern_values = self.medium_values * 1.25
            # elif 7 <= (self.ship_length / self.ship_breadth) <= 9:
            #     self.stern_values = self.medium_values * 1.1
            # else:
            #     self.stern_values = self.medium_values
        elif self.method == 8:
            # Hooft
            self.medium_values = (self.ship_displacement_volume / np.power(self.ship_length, 2)) * \
                                 (np.power(self.froude_number, 2) / np.sqrt(1 - np.power(self.froude_number, 2)))
        elif self.method == 9:
            # Millward
            # A VERIFIER
            if .44 <= self.block_coefficient <= .83 and 6 <= (self.ship_length / self.water_depth) <= 12:
                self.medium_values = (15 * self.block_coefficient * (self.ship_breadth / self.ship_length) - .55) * \
                                     (np.power(self.froude_number, 2) / (1 - .9 * self.froude_number)) * \
                                     (self.ship_length / 100)
            else:
                self.medium_values = np.nan
        if density is True:
            for count, value in enumerate(self.density_gradient):
                self.medium_values_opt[count] = np.mean([25*self.alpha_1 * value, 25*self.alpha_2 * value])
                self.medium_values_opt[count] = self.medium_values_opt[count] / 1000
                self.medium_values_opt[count] = self.medium_values - self.medium_values_opt
        else:
            pass
            # raise MethodException("The method ID '%s' doesn't exist, default methid ID is  : %d "
            #                       % (self.method, self.method_id_default))
################################################################################
