#!/home/$USER/anaconda3/envs/py27/bin python
# -*- coding: utf-8 -*-
# ***********************************************************
# Project: GIRONDE XL
# Authors: Sylvain Orseau
# history: Sylvain Orseau (Cerema)
#           15/01/2020, #V0P1
# ***********************************************************
"""
@author: Sylvain Orseau (Cerema) - sylvain.orseau@tutanota.com

"""
################################################################################
# Import modules
import numpy as np


################################################################################
class ConversionLevel(object):
    def __init__(self):
        self.data = None
        self.x = list()
        self.y = list()
        self.z_ign = list()
        self.z_h = list()

    def conversion_zign(self, x, y, z):
        self.x = x
        self.y = y
        self.z_h = z
        self.z_ign = np.zeros(len(self.x), dtype=float)
        # self.data = np.zeros(shape=(len(self.x), 3))
        for i in range(len(self.x)):
            if self.y[i] > 6501999:  # Verdon
                dz = 2.871
                self.z_ign[i] = self.z_h[i] - dz
            elif 6490286 < self.y[i] <= 6501999:  # Verdon/Richard
                dz = 2.871 - (6501999 - self.y[i]) * (2.871 - 2.859) / (6501999 - 6490286)
                self.z_ign[i] = self.z_h[i] - dz
            elif 6478169 < self.y[i] <= 6490286:  # Richard/Lamena
                dz = 2.859 - (6490286 - self.y[i]) * (2.859 - 2.817) / (6490286 - 6478169)
                self.z_ign[i] = self.z_h[i] - dz
            elif 6464559 < self.y[i] <= 6478169:  # Lamena/Pauillac
                dz = 2.817 - (6478169 - self.y[i]) * (2.817 - 2.539) / (6478169 - 6464559)
                self.z_ign[i] = self.z_h[i] - dz
            elif 6452805 < self.y[i] <= 6464559:  # Pauillac/Fort Medoc
                dz = 2.539 - (6464559 - self.y[i]) * (2.539 - 2.296) / (6464559 - 6452805)
                self.z_ign[i] = self.z_h[i] - dz
            elif 6450445 < self.y[i] <= 6452805:  # Fort Medoc/Ile Verte
                dz = 2.296 - (6452805 - self.y[i]) * (2.296 - 2.218) / (6452805 - 6450445)
                self.z_ign[i] = self.z_h[i] - dz
            elif 6445438 < self.y[i] <= 6450445:  # Ile Verte/Bec Ambes
                dz = 2.218 - (6450445 - self.y[i]) * (2.218 - 2.002) / (6450445 - 6445438)
                self.z_ign[i] = self.z_h[i] - dz
            elif 6439630 < self.y[i] <= 6445438:  # Bec Ambes/Le Marquis
                dz = 2.002 - (6445438 - self.y[i]) * (2.002 - 1.878) / (6445438 - 6439630)
                self.z_ign[i] = self.z_h[i] - dz
            elif 6428749 < self.y[i] <= 6439630:  # Le Marquis/Bassens
                dz = 1.878 - (6439630 - self.y[i]) * (1.878 - 1.824) / (6439630 - 6428749)
                self.z_ign[i] = self.z_h[i] - dz
            elif 6423089 < self.y[i] <= 6428749:  # Bassens/Bordeaux
                dz = 1.824 - (6428749 - self.y[i]) * (1.824 - 1.812) / (6428749 - 6423089)
                self.z_ign[i] = self.z_h[i] - dz
            elif 6423089 >= self.y[i]:  # < Bordeaux
                self.z_ign[i] = self.z_h[i] - 1.812
            # self.data[i] = (self.x[i], self.y[i], self.z_ign[i])
        return self.z_ign

    def conversion_zh(self, x, y, z):
        self.x = x
        self.y = y
        self.z_ign = z
        self.z_h = np.zeros(len(self.x), dtype=float)
        # self.data = np.zeros(shape=(len(self.x), 3))
        for i in range(len(self.x)):
            if self.y[i] > 6501999:  # > Verdon
                dz = 2.871
                self.z_h[i] = self.z_ign[i] + dz
            elif 6490286 < self.y[i] <= 6501999:  # Verdon/Richard
                dz = 2.871 - (6501999-self.y[i]) * (2.871-2.859) / (6501999-6490286)
                self.z_h[i] = self.z_ign[i] + dz
            elif 6478169 < self.y[i] <= 6490286:  # Richard/Lamena
                dz = 2.859 - (6490286-self.y[i]) * (2.859-2.817) / (6490286-6478169)
                self.z_h[i] = self.z_ign[i] + dz
            elif 6464559 < self.y[i] <= 6478169:  # Lamena/Pauillac
                dz = 2.817 - (6478169-self.y[i]) * (2.817 - 2.539) / (6478169 - 6464559)
                self.z_h[i] = self.z_ign[i] + dz
            elif 6452805 < self.y[i] <= 6464559:  # Pauillac/Fort Medoc
                dz = 2.539 - (6464559-self.y[i]) * (2.539-2.296) / (6464559-6452805)
                self.z_h[i] = self.z_ign[i] + dz
            elif 6450445 < self.y[i] <= 6452805:  # Fort Medoc/Ile Verte
                dz = 2.296 - (6452805-self.y[i]) * (2.296-2.218) / (6452805-6450445)
                self.z_h[i] = self.z_ign[i] + dz
            elif 6445438 < self.y[i] <= 6450445:  # Ile Verte/Bec Ambes
                dz = 2.218 - (6450445-self.y[i]) * (2.218-2.002) / (6450445-6445438)
                self.z_h[i] = self.z_ign[i] + dz
            elif 6439630 < self.y[i] <= 6445438:  # Bec Ambes/Le Marquis
                dz = 2.002 - (6445438-self.y[i]) * (2.002-1.878) / (6445438-6439630)
                self.z_h[i] = self.z_ign[i] + dz
            elif 6428749 < self.y[i] <= 6439630:  # Le Marquis/Bassens
                dz = 1.878 - (6439630-self.y[i]) * (1.878-1.824) / (6439630-6428749)
                self.z_h[i] = self.z_ign[i] + dz
            elif 6423089 < self.y[i] <= 6428749:  # Bassens/Bordeaux
                dz = 1.824 - (6428749-self.y[i]) * (1.824-1.812) / (6428749-6423089)
                self.z_h[i] = self.y[i] + dz
            elif 6423089 >= self.y[i]:  # < Bordeaux
                dz = 1.812
                self.z_h[i] = self.z_ign[i] + dz
            # self.data[i] = (self.x[i], self.y[i], self.z_ign[i])
        return self.z_h

################################################################################
