#!/home/$USER/anaconda3/envs/py27/bin python
# -*- coding: utf-8 -*-
# ***********************************************************
# Project: GIRONDE XL
# Authors: Sylvain Orseau
# history: Sylvain Orseau (Cerema)
#           15/01/2020, #V0P1
# ***********************************************************
"""
@author: Sylvain Orseau (Cerema) - sylvain.orseau@tutanota.com
"""
################################################################################
# Import public modules
import csv
import numpy as np
import pandas as pd

# Import private modules
from conversion import ConversionLevel


################################################################################
class Configuration(object):
    def __init__(self, route_type):
        self.route_type = route_type
        # ---
        self.data = None
        self.distance = list()
        self.duration = list()
        self.filename_distance = str()
        self.filename_locations = str()
        self.filename_margin = str()
        self.filename_timings = str()
        self.safety_margin = list()
        self.thresholds = None
        self.tidal_range_id = int()

        # -- Route Type
        if "SEAWARD" in self.route_type:
            self.route_id = 3
        elif "FLOOD" in self.route_type:
            self.route_id = 1
        else:
            self.route_id = 2

    class Terminals(object):
        def __init__(self, csv_file):
            self.terminal_filename = csv_file
            # -- Other variables
            self.terminal_id = int()
            self.terminal_name = str()
            self.terminal_node = int()
            self.terminal_referent_node = int()

        def load(self, name):
            self.terminal_name = name
            raw_data = pd.read_csv(self.terminal_filename, sep=",")
            data_filtered = raw_data[(raw_data.TERMINAL_NAME == self.terminal_name)]
            self.terminal_id = data_filtered["ID"].values[0]
            self.terminal_node = data_filtered["NODE"].values[0] - 1
            # -- Specific to the Port of Bordeaux
            if self.terminal_id < 4:
                self.terminal_referent_node = 44180 - 1

    class CriticalLocations(object):
        dt = [("STATIONS", str), ("TYPE", str), ("ID", int), ("PK", float), ("BREADTH", float),
              ("X", float), ("Y", float), ("Z", float), ("NODES", int)]
        header = ["STATIONS", "TYPE", "ID", "PK", "BREADTH", "X", "Y", "Z", "NODES"]

        def __init__(self, csv_file):
            self.filename_distance = str()
            self.filename_locations = csv_file
            # ---
            self.conversion = None
            self.breadth = list()
            self.data = None
            self.data_filtered = None
            self.distance = list()
            self.distance_markers = list()
            self.distance_type = list()
            self.filename_output = str()
            self.id = list()
            self.method = str()
            self.pk = list()
            self.route_id = int()
            self.stations = list()
            self.terminal_id = int()
            self.type = list()
            self.x = list()
            self.y = list()
            self.z = list()
            self.z_ign = list()
            self.z_h = list()
            self.nodes = list()
            self.number = int()

        def load(self, route_id):
            self.route_id = route_id
            raw_data = pd.read_csv(self.filename_locations, sep=",", header=0, names=self.header, dtype=self.dt)
            # -- Data sorting
            if self.route_id == 3:
                self.data = raw_data.sort_values(by="PK", ascending=True)
            else:
                self.data = raw_data.sort_values(by="PK", ascending=False)

        def filter(self, terminal_id):
            self.terminal_id = terminal_id
            # -- Filtering
            if self.terminal_id == 1:
                self.data_filtered = self.data[(self.data.ID >= 1) & (self.data.ID <= 24)]
            elif self.terminal_id == 2:
                self.data_filtered = self.data[(self.data.ID >= 2) & (self.data.ID <= 24)]
            elif self.terminal_id == 3:
                self.data_filtered = self.data[(self.data.ID >= 3) & (self.data.ID <= 24)]
            elif self.terminal_id == 4:
                self.data_filtered = self.data[(self.data.ID >= 6) & (self.data.ID <= 24)]
            elif self.terminal_id == 5:
                self.data_filtered = self.data[(self.data.ID >= 8) & (self.data.ID <= 24)]
            elif self.terminal_id == 6:
                self.data_filtered = self.data[(self.data.ID >= 12) & (self.data.ID <= 24)]
            else:
                pass
            self.stations = self.data_filtered.iloc[:, 0].values.astype(str)
            self.type = self.data_filtered.iloc[:, 1].values.astype(str)
            self.id = self.data_filtered.iloc[:, 2].values.astype(int)
            self.pk = self.data_filtered.iloc[:, 3].values.astype(float)
            self.breadth = self.data_filtered.iloc[:, 4].values.astype(float)
            self.x = self.data_filtered.iloc[:, 5].values.astype(float)
            self.y = self.data_filtered.iloc[:, 6].values.astype(float)
            self.z = self.data_filtered.iloc[:, 7].values.astype(float) * -1
            self.nodes = self.data_filtered.iloc[:, 8].values.astype(int) - 1
            self.number = len(self.nodes)
            self.type_marker = np.where(self.type == "Marker")[0].tolist()
            self.type_threshold = np.where(self.type == "Threshold")[0].tolist()

        def convert_depth(self, method):
            self.method = method
            self.conversion = ConversionLevel()
            if "ign" in self.method:
                self.z_ign = self.conversion.conversion_zign(self.x, self.y, self.z)
                return self.z_ign
            else:
                self.z_h = self.conversion.conversion_zh(self.x, self.y, self.z)
                return self.z_h

        def write_file(self, name, x, y, z):
            self.filename_output = name
            self.x = x
            self.y = y
            self.z = z
            raw = [(self.stations[i], self.type[i], self.id[i], self.pk[i], self.x, self.y, self.z, self.nodes[i])
                   for i in range(len(self.stations))]
            # self.data_output = np.array(raw, dtype=self.dt)
            file_output = open(self.filename_output, "w")
            with file_output:
                writer = csv.writer(file_output)
                writer.writerows(raw)

        def load_distance(self, csv_file):
            self.filename_distance = csv_file
            # ---
            raw_data = pd.read_csv(self.filename_distance, sep=",", header=0)
            raw_data_filtered = raw_data[(raw_data.TYPE == "Marker")]
            print "rawdf", raw_data_filtered
            if self.terminal_id == 1:
                self.distance = raw_data["DISTANCE (KM)"].values
                self.distance_markers = raw_data_filtered["DISTANCE (KM)"].values
                self.distance_type = raw_data["TYPE"].values
            elif self.terminal_id == 2:
                self.distance = raw_data["DISTANCE (KM)"].values[:-1]
                self.distance_markers = raw_data_filtered["DISTANCE (KM)"].values[:-1]
                self.distance_type = raw_data["TYPE"].values[:-1]
            elif self.terminal_id == 3:
                self.distance = raw_data["DISTANCE (KM)"].values[:-2]
                self.distance_markers = raw_data_filtered["DISTANCE (KM)"].values[:-2]
                self.distance_type = raw_data["TYPE"].values[:-2]
            elif self.terminal_id == 4:
                self.distance = raw_data["DISTANCE (KM)"].values[:-2]
                self.distance_markers = raw_data_filtered["DISTANCE (KM)"].values[:-2]
                self.distance_type = raw_data["TYPE"].values[:-2]
            if self.route_id == 3:
                self.distance = self.distance[::-1]
                self.distance_type = self.distance_type[::-1]
                last_value = self.distance[0]
                for row, val in enumerate(self.distance):
                    if self.distance_type[row] == "Threshold":
                        self.distance[row] = last_value - val
                    else:
                        self.distance[row] = val
                        last_value = val
                self.distance_markers = self.distance_markers[::-1]

    class SafetyMargin(object):
        def __init__(self, csv_file):
            self.filename_margin = csv_file
            # --
            self.raw_data = None
            self.route_id = int()
            self.values = list()
            self.terminal_id = int()

        def load(self, route_id, terminal_id):
            self.route_id = route_id
            self.terminal_id = terminal_id
            self.raw_data = pd.read_csv(self.filename_margin, sep=";")
            data_filtered = self.raw_data.loc[(self.raw_data["CDE_PORT"] == self.terminal_id)]
            self.values = np.asarray(data_filtered.loc[:, "VALEUR"] / 100).astype(float)
            if self.route_id == 3:
                self.values = self.values[::-1]

                
################################################################################
