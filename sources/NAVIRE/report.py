#!/home/$USER/anaconda3/envs/py27/bin python
# -*- coding: utf-8 -*-

# -- Import modules
import datetime as dt
import numpy as np
import os
# Reportlab
from reportlab.lib import colors, styles
from reportlab.pdfgen import canvas
from reportlab.platypus import BaseDocTemplate, Frame, PageTemplate
from reportlab.platypus import Paragraph, PageBreak, Spacer
from reportlab.platypus import Flowable, Image, Table
from reportlab.lib.enums import TA_RIGHT
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import cm, mm


##################################################
class MCLine(Flowable):
    def __init__(self, width, height=0):
        Flowable.__init__(self)
        self.width = width
        self.height = height

    def __repr__(self):
        return "Line(w=%s)" % self.width

    def draw(self):
        self.canv.line(0, self.height, self.width, self.height)


class Document:
    def __init__(self, type):
        self.type = type
        self.style_sheet = getSampleStyleSheet()
        self.style_right = ParagraphStyle(name='left', parent=self.style_sheet['Normal'])
        self.margin_size = 15 * mm
        self.page_size = A4
        # --
        self.data_header = None
        self.header = None
        self.style_header = None
        self.style_table = None

    def create_pdf(self, name, story):
        self.name = name
        self.story = story
        pdf = BaseDocTemplate(self.name, page_size=self.page_size, right_margin=self.margin_size,
                              top_margin=self.margin_size, bottom_margin=self.margin_size)
        frame = Frame(self.margin_size, self.margin_size, self.page_size[0] - 2 * self.margin_size,
                      self.page_size[1] - 2 * self.margin_size, id="frame")
        template = PageTemplate(id="template", frames=[frame])
        pdf.addPageTemplates([template])
        pdf.build(story)

    def construct_pdf(self, name, header, data):
        self.line = MCLine(500)
        self.cw1 = [2 * cm] + [7 * cm] + [5 * cm] + [2 * cm]  # Column width
        self.cw2 = [1.5 * cm] + [5 * cm] + [1.5 * cm] * 7  # Column width
        self.rh = 0.7 * cm  # Row height
        self.style_header = [('FONT', (0, 1), (-1, -1), 'Helvetica'),
                             ('FONTSIZE', (0, 0), (-1, -1), 8),
                             ('ALIGN', (0, 0), (0, -1), 'CENTER'),
                             ('ALIGN', (1, 0), (1, -1), 'LEFT'),
                             ('ALIGN', (2, 0), (-1, -1), 'CENTER'),
                             ('LINEABOVE', (0, 1), (-1, 1), 1, colors.black),
                             ('LINEBELOW', (0, 0), (-1, -1), .5, colors.black)]
        self.style_table = [('FONT', (0, 1), (-1, -1), 'Helvetica'),
                            ('FONTSIZE', (0, 0), (-1, -1), 8),
                            ('ALIGN', (0, 0), (0, -1), 'CENTER'),
                            ('ALIGN', (1, 0), (1, -1), 'LEFT'),
                            ('ALIGN', (2, 0), (-1, -1), 'CENTER'),
                            ('LINEABOVE', (0, 0), (-1, 0), 1, colors.black),
                            ('LINEBELOW', (0, 0), (-1, -1), .5, colors.black),
                            ('LINEBELOW', (0, -1), (-1, -1), 1, colors.black),
                            ('BACKGROUND', (0, 0), (0, -1), colors.lightgrey),
                            ('BACKGROUND', (2, 0), (2, -1), colors.lightgrey),
                            ('BACKGROUND', (4, 0), (4, -1), colors.lightgrey),
                            ('BACKGROUND', (6, 0), (6, -1), colors.lightgrey),
                            ('BACKGROUND', (8, 0), (8, -1), colors.lightgrey)]
        self.header = header
        # -- Header
        self.data_header = [["KP", "Critical Locations", "Date", "Hour", "Z", "Water Level", "UKC", "Density", "Squat"]]
        #
        self.data = data
        self.name = name
        # -- Data loading
        t1 = Table(self.header, colWidths=self.cw1, rowHeights=self.rh/1.5, hAlign="LEFT")
        t3 = Table(self.data_header, style=self.style_header, colWidths=self.cw2, rowHeights=self.rh)
        t4 = Table(self.data, style=self.style_table, colWidths=self.cw2, rowHeights=self.rh)

        # -- Story creation
        story = []
        h1 = story.append(Paragraph("ROUTE REPORT", self.style_sheet["Title"]))
        # Float line
        story.append(Spacer(0, 0.5 * cm))
        story.append(self.line)
        story.append(Spacer(0, 0.1 * cm))
        # Table 1
        story.append(t1)
        # Float line
        story.append(Spacer(0, 0.1 * cm))
        story.append(self.line)
        story.append(Spacer(0, 0.2 * cm))
        # Data
        story.append(t3)
        story.append(t4)

        # -- PDF creation
        self.create_pdf(self.name, story)

####################################################################################################
