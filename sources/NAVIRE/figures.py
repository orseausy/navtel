#!/home/$USER/anaconda3/envs/py27/bin python
# -*- coding: utf-8 -*-
# ******************************************************************************
# Project: opentelUtils
# Authors: Sylvain Orseau
# history: Sylvain Orseau (Cerema)
#           11/15/2019, #V0P1
# ******************************************************************************
"""
@author: Sylvain Orseau (Cerema) - sylvain.orseau@tutanota.com

"""
################################################################################
# Import public modules
import datetime as dt
import numpy as np
import matplotlib.font_manager as fm
from matplotlib import pyplot as plt
import os

# Path file
path_file = os.getcwd()
path_data = os.path.join(path_file[:-8] + "/examples/Gironde/GPMB/data")


################################################################################
class Figures:
    font_path = "/usr/share/fonts/truetype/msttcorefonts/Arial.ttf"
    font = fm.FontProperties(fname=font_path, size=10)
    font2 = fm.FontProperties(fname=font_path, size=8)

    def __init__(self, config, critical_locations, mail, variables):
        self.X = None
        self.Y = None
        self.Z = None
        # --
        self.config = config
        self.critical_locations = critical_locations
        self.mail = mail
        self.variables = variables

    def fig06(self, x, y, z, idx_trajects):
        # -- Figure
        self.X, self.Y = np.meshgrid(x, y)
        self.Z = z
        self.idx_trajects = idx_trajects
        #
        xticks = [tick for tick in xrange(self.X[0][0], self.X[0][-1], (self.X[0][-1] - self.X[0][0]) / 6)]
        xticks.append(self.X[0][-1])
        tl = [(self.config.start_time + dt.timedelta(seconds=value)) for value in xticks]
        # --
        fig = plt.figure(figsize=(18 / 2.54, 12 / 2.54))
        ax = fig.add_subplot(111)
        cs = plt.contourf(self.X, self.Y, self.Z, cmap=plt.cm.RdBu, alpha=0.5, aspect=0.5, origin="lower")
        cs2 = plt.contour(cs, levels=[self.mail.draft], colors="r", linestyles="-", linewidths=.5)
        p1 = plt.plot(np.asarray(traject_safest)[~np.isnan(self.variables['ALLOWABLE_DRAFT_SAFEST'])],
                      y,
                      color="k",
                      linestyle="-",
                      linewidth=.5,
                      marker="o",
                      markersize=2)
        k = 0
        for count, value in enumerate(np.where(np.diff(self.idx_trajects) > 1)[1]):
            idx_03 = np.where(self.variables['ALLOWABLE_DRAFT_SAFEST']
                              [~np.isnan(self.variables['ALLOWABLE_DRAFT_SAFEST']).any(axis=1)] ==
                              np.nanmax(np.nanmin(
                                  self.variables['ALLOWABLE_DRAFT_SAFEST']
                                  [~np.isnan(self.variables['ALLOWABLE_DRAFT_SAFEST']).any(axis=1)], axis=0)
                                        [self.idx_trajects[0][k:value + 1]]))
            cs3 = plt.text(np.asarray(traject_safest)[~np.isnan(self.variables['ALLOWABLE_DRAFT_SAFEST'])][0] - 5000,
                           5,
                           "%s, %s" %
                           ((traject_time_safest[0].strftime("%m/%d %Hh%M")),
                            self.critical_locations.stations[self.critical_locations.type_threshold][idx_03[0]][0]))
            cs3.set_rotation("vertical")
            cs3.set_fontproperties(self.font2)
        plt.clabel(cs2, fmt="%1.2f", inline=True, inline_spacing=.1, fontsize=8)
        plt.grid(axis="y", color="k", linestyle=":", linewidth=.5)
        ax.set(xlim=(x[0], x[-1]), ylim=(0, 18))
        ax.set_xticks(xticks)
        ax.set_xticklabels([timelabel.strftime("%d/%m %Hh") for timelabel in tl])
        ax.set_yticks(y)
        ax.set_yticklabels(self.critical_locations.stations[self.critical_locations.type_threshold])
        # --
        for label in ax.get_xticklabels():
            label.set_fontproperties(self.font2)
        for label in ax.get_yticklabels():
            label.set_fontproperties(self.font2)
        # --
        cbar = plt.colorbar(cs)
        cbar.ax.set_ylabel("Allowable Draft [m]")

        fig.tight_layout()
        plt.savefig(str(os.getcwd()) + "/fig06.jpg", dpi=1200)

    def fig07(self, squat):
        self.squat = squat
        #
        fig07, axs = plt.subplots(4, 1, sharex="col", sharey="row", figsize=(18 / 2.54, 20 / 2.54))
        axs[0].plot(np.arange(0, len(self.critical_locations.type_threshold)),
                    self.variables['VELOCITY_SAFEST'][~np.isnan(self.variables['VELOCITY_SAFEST'])],
                    linestyle="none",
                    marker="o",
                    markersize=4,
                    color="k")
        axs[0].set(xlim=(0, 19), ylim=(0, 1.5))
        axs[0].set_title("a)", loc="left").set_fontproperties(self.font)
        axs[0].set_xticks(np.arange(0, len(self.critical_locations.type_threshold)))
        axs[0].set_xticklabels(self.critical_locations.stations[self.critical_locations.type_threshold])
        axs[0].set_yticks((0, .5, 1, 1.5))
        axs[0].set_ylabel("Velocity [m/s]").set_fontproperties(self.font)
        axs[0].label_outer()
        axs[1].plot(np.arange(0, len(self.critical_locations.type_threshold)),
                    self.variables['SALINITY_SAFEST'][~np.isnan(self.variables['SALINITY_SAFEST'])],
                    linestyle="none",
                    marker="o",
                    markersize=4,
                    color="k")
        axs[1].set(xlim=(0, 19), ylim=(0, 3))
        axs[1].set_title("b)", loc="left").set_fontproperties(self.font)
        axs[1].set_xticks(np.arange(0, len(self.critical_locations.type_threshold)))
        axs[1].set_xticklabels(self.critical_locations.stations[self.critical_locations.type_threshold], rotation=90)
        axs[1].set_yticks((0, 1, 2, 3))
        axs[1].set_ylabel("Salinity [psu]").set_fontproperties(self.font)
        axs[1].label_outer()
        axs[2].plot(np.arange(0, len(self.critical_locations.type_threshold)),
                    self.variables['CONC_MAS_CL2'][~np.isnan(self.variables['CONC_MAS_CL2'])],
                    linestyle="none",
                    marker="o",
                    markersize=4,
                    color="k")
        axs[2].set(xlim=(0, 19), ylim=(0, .15))
        axs[2].set_title("c)", loc="left").set_fontproperties(self.font)
        axs[2].set_xticks(np.arange(0, len(self.critical_locations.type_threshold)))
        axs[2].set_xticklabels(self.critical_locations.stations[self.critical_locations.type_threshold], rotation=90)
        axs[2].set_yticks((0, .05, .1, .15))
        axs[2].set_ylabel("SPM [g/L]").set_fontproperties(self.font)
        axs[2].label_outer()
        axs[3].plot(np.arange(0, len(self.critical_locations.type_threshold)),
                    self.squat.medium_values[~np.isnan(self.squat.medium_values)],
                    linestyle="none",
                    marker="o",
                    markersize=4,
                    color="k")
        axs[3].set(xlim=(0, 19), ylim=(0, 1.5))
        axs[3].set_title("d)", loc="left").set_fontproperties(self.font)
        axs[3].set_xticks(np.arange(0, len(self.critical_locations.type_threshold)))
        axs[3].set_xticklabels(self.critical_locations.stations[self.critical_locations.type_threshold], rotation=90)
        axs[3].set_yticks((0, .5, 1, 1.5))
        axs[3].set_ylabel("Predicted squat [m]").set_fontproperties(self.font)
        axs[3].label_outer()
        # Set font of tick and axis labels
        for ax in axs.flat:
            for label in (ax.get_xticklabels() + ax.get_yticklabels()):
                label.set_fontproperties(self.font)

        fig07.tight_layout()
        plt.savefig(str(os.getcwd()) + "/fig07.pdf", dpi=600)
