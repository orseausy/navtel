#!/home/$USER/anaconda3/envs/py27/bin python
# -*- coding: utf-8 -*-
# ***********************************************************
# Project: GIRONDE XL
# Authors: Sylvain Orseau
# history: Sylvain Orseau (Cerema)
#           15/01/2020, #V0P1
# ***********************************************************
"""
@author: Sylvain Orseau (Cerema) - sylvain.orseau@tutanota.com

"""
################################################################################
# Import public modules
import pandas as pd


################################################################################
class Timings(object):
    def __init__(self, csv_file):
        self.filename_timings = csv_file
        # ---
        self.route_id = int()
        self.speed_imposed = int()
        self.terminal_id = int()
        self.tidal_range_id = int()
        self.timings = list()
        self.timings_raw = list()
        self.values = list()

    def load(self, route_id, terminal_id, tidal_range_id, speed):
        self.route_id = route_id
        self.terminal_id = terminal_id
        self.tidal_range_id = tidal_range_id
        self.speed_imposed = speed
        # --- Reading
        raw_data = pd.read_csv(self.filename_timings, sep=",", header=0)
        data_filtered = raw_data[(raw_data.CDE_COEF == self.tidal_range_id) &
                                 (raw_data.CDE_PORT == self.terminal_id) &
                                 (raw_data.CODEPARCOURS == self.route_id) &
                                 (raw_data.VALEUR_VITESSE == self.speed_imposed)].sort_values(by="CDE_REP",
                                                                                              ascending=False)
        self.timings_raw = data_filtered["TEMPS"]
        print self.timings_raw
        timings_data_sign = [str(x)[0:1] for x in self.timings_raw]
        timings_data_value = [str(x)[1:] for x in self.timings_raw]
        self.timings = pd.to_datetime(timings_data_value, format="%H:%M")
        self.timings = self.timings.hour * 60 + self.timings.minute
        self.values = [duration * -1 if timings_data_sign[row] == "+" else duration
                       for row, duration in enumerate(self.timings)]
        print "Passing hours were loaded!"
