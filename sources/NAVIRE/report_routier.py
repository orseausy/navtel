#!/home/$USER/anaconda3/envs/py27/bin python
# -*- coding: utf-8 -*-

# -- Import modules
import datetime as dt
import numpy as np
import os
# Reportlab
from reportlab.lib import colors, styles
from reportlab.pdfgen import canvas
from reportlab.platypus import BaseDocTemplate, Frame, PageTemplate
from reportlab.platypus import Paragraph, PageBreak, Spacer
from reportlab.platypus import Flowable, Image, Table
from reportlab.lib.enums import TA_RIGHT
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import cm, mm


##################################################
class MCLine(Flowable):
    def __init__(self, width, height=0):
        Flowable.__init__(self)
        self.width = width
        self.height = height

    def __repr__(self):
        return "Line(w=%s)" % self.width

    def draw(self):
        self.canv.line(0, self.height, self.width, self.height)


class Document:
    def __init__(self, type):
        self.type = type
        self.style_sheet = getSampleStyleSheet()
        self.style_right = ParagraphStyle(name='right', parent=self.style_sheet['Normal'], alignment=TA_RIGHT)
        self.margin_size = 25 * mm
        self.page_size = A4

    def create_pdf(self, name, story):
        self.name = name
        self.story = story
        pdf = BaseDocTemplate(self.name, page_size=self.page_size, right_margin=self.margin_size,
                              top_margin=self.margin_size, bottom_margin=self.margin_size)
        frame = Frame(self.margin_size, self.margin_size, self.page_size[0] - 2 * self.margin_size,
                      self.page_size[1] - 2 * self.margin_size, id="frame")
        template = PageTemplate(id="template", frames=[frame])
        pdf.addPageTemplates([template])
        pdf.build(story)

    def construct_pdf(self, name, h1, h2, data):
        self.line = MCLine(500)
        self.cw1 = [4 * cm]  # Column width
        self.cw2 = [1.5 * cm] + [6 * cm] + [1 * cm] + [3 * cm] * 2 + [1 * cm] + [1.2 * cm] * 3  # Column width
        self.cw3 = [1.5 * cm] + [6 * cm] + [1 * cm] + [3 * cm] + [1.4 * cm] * 3 + [1.6 * cm] * 2  # Column width
        self.rh = 0.7 * cm  # Row height
        self.ts1 = [('FONT', (0, 1), (-1, -1), 'Helvetica'),
                    ('FONTSIZE', (0, 0), (-1, -1), 8),
                    ('ALIGN', (0, 0), (0, -1), 'CENTER'),
                    ('ALIGN', (1, 0), (1, -1), 'LEFT'),
                    ('ALIGN', (2, 0), (-1, -1), 'CENTER'),
                    ('LINEABOVE', (0, 1), (-1, 1), 1, colors.black),
                    ('LINEBELOW', (0, 0), (-1, -1), .5, colors.black)]
        self.ts3 = [('FONT', (0, 1), (-1, -1), 'Helvetica'),
                    ('FONTSIZE', (0, 0), (-1, -1), 8),
                    ('ALIGN', (0, 0), (0, -1), 'CENTER'),
                    ('ALIGN', (1, 0), (1, -1), 'LEFT'),
                    ('ALIGN', (2, 0), (-1, -1), 'CENTER'),
                    ('LINEABOVE', (0, 1), (-1, 1), 1, colors.black),
                    ('LINEBELOW', (0, 0), (-1, -1), .5, colors.black)]
        self.ts4 = [('FONT', (0, 1), (-1, -1), 'Helvetica'),
                    ('FONTSIZE', (0, 0), (-1, -1), 8),
                    ('ALIGN', (0, 0), (0, -1), 'CENTER'),
                    ('ALIGN', (1, 0), (1, -1), 'LEFT'),
                    ('ALIGN', (2, 0), (-1, -1), 'CENTER'),
                    ('LINEABOVE', (0, 0), (-1, 0), 1, colors.black),
                    ('LINEBELOW', (0, 0), (-1, -1), .5, colors.black),
                    ('LINEBELOW', (0, -1), (-1, -1), 1, colors.black),
                    ('BACKGROUND', (0, 0), (0, -1), colors.lightgrey),
                    ('BACKGROUND', (2, 0), (2, -1), colors.lightgrey),
                    ('BACKGROUND', (4, 0), (4, -1), colors.lightgrey),
                    ('BACKGROUND', (6, 0), (6, -1), colors.lightgrey),
                    ('BACKGROUND', (9, 0), (9, -1), colors.lightgrey)]
        self.h1 = h1
        self.h2 = h2
        # todo modifier selon paramètres de dataframe
        if self.type == "navigation":
            self.data_header = [["PK", "Repères/Seuils", "Jour", "Heure prévue", "Cote", "Hauteur", "Hs/Quille",
                                 "Densité", "Enfoncement"]]
            self.data = data
            self.name = name
        elif self.type == "route":
            self.data_header = [["PK", "Repères/Seuils", "Jour", "Heure prévue", "Heure réelle", "Delta", "Cote",
                                 "Hauteur", "Hs/Quille"]]
            self.data = data
            self.name = name
        else:
            pass
        # Data loading
        t1 = Table(self.h1, colWidths=self.cw1, rowHeights=self.rh/1.5, hAlign="RIGHT")
        t2 = Table(self.h2)
        if self.type == "navigation":
            t3 = Table(self.data_header, style=self.ts3, colWidths=self.cw3, rowHeights=self.rh)
            t4 = Table(self.data, style=self.ts4, colWidths=self.cw3, rowHeights=self.rh)
        elif self.type == "route":
            t3 = Table(self.data_header, style=self.ts3, colWidths=self.cw2, rowHeights=self.rh)
            t4 = Table(self.data, style=self.ts4, colWidths=self.cw2, rowHeights=self.rh)
        else:
            pass
        # Story creation
        story = []
        # Logo
        logo = "PilotageGironde2.png"
        im = Image(logo)
        im.hAlign = 'LEFT'
        story.append(im)
        # Title
        # if self.type == "navigation":
        #     h1 = story.append(Paragraph("Feuille de navigation", self.style_sheet["Title"]))
        # elif self.type == "route":
        #     h1 = story.append(Paragraph("Feuille de route", self.style_sheet["Title"]))
        # else:
        #     pass
        h1 = story.append(Paragraph("Feuille de route", self.style_sheet["Title"]))
        story.append(t1)
        # Float line
        story.append(Spacer(0, 0.5 * cm))
        story.append(self.line)
        story.append(Spacer(0, 0.1 * cm))
        # Header
        story.append(t2)
        # Float line
        story.append(Spacer(0, 0.1 * cm))
        story.append(self.line)
        story.append(Spacer(0, 0.2 * cm))

        # Table
        story.append(t3)
        story.append(t4)

        # PDF creation
        self.create_pdf(self.name, story)

    def encrypt(self, password):
        password = password
        with open(self.name, "rb") as in_file:
            input_pdf = PdfFileReader(in_file)
        output_pdf = PdfFileWriter()
        output_pdf.appendPagesFromReader(input_pdf)
        output_pdf.encrypt(password)
        with open(self.name, "wb") as out_file:
            output_pdf.write(out_file)
