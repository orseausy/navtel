#!/home/$USER/anaconda3/envs/py27/bin python
# -*- coding: utf-8 -*-
# ******************************************************************************
# Project: opentelUtils
# Authors: Sylvain Orseau
# history: Sylvain Orseau (Cerema)
#           11/15/2019, #V0P1
# ******************************************************************************
"""
@author: Sylvain Orseau (Cerema) - sylvain.orseau@tutanota.com

"""
################################################################################
# Import public modules
import datetime as dt
from detect_peaks import detect_peaks
import json
import re

# Import private modules
from pputils.ppmodules.selafin_io_pp import *


# Private functions
def nearest(items, pivot):
    return min(items, key=lambda x: abs(x - pivot))


################################################################################
class Selafin(object):
    def __init__(self, filename):
        self.filename = filename
        # ---
        self.raw = ppSELAFIN(self.filename)
        self.raw.readHeader()
        self.raw.readTimes()
        self.mesh = self.raw.getMesh()
        self.time = self.raw.getTimes()
        self.index_prediction = self.time.index(1310400)
        self.time_prediction = self.time[self.index_prediction:]
        self.date = [dt.datetime(*self.raw.DATE) + dt.timedelta(seconds=elapsed_time) for elapsed_time in self.time]
        self.time_step = np.arange(0, len(self.time), dtype=np.int)
        self.variable_names = self.raw.getVarNames()
        self.variable_names = [re.sub("(\W)", "_", name.strip()) for name in self.variable_names]
        self.bottom = list()
        self.bottom_friction = list()
        self.data = None
        self.filename_json = str()
        self.node = list()
        self.nodes = list()
        self.timings = list()
        self.values = list()
        self.variables = dict()
        self.variable_extracted = str()

    def extract_at_node(self, node, *time):
        self.node = node
        self.raw.readVariablesAtNode(self.node)
        self.data = self.raw.getVarValuesAtNode()
        if not time:
            self.variables = {name: self.data[:, i] for i, name in enumerate(self.variable_names)}

    def extract_at_multiple_nodes(self, nodes):
        self.nodes = nodes
        # dt = -15 + 1. / 24.
        # self.elapsed_time = [float(i) / (3600 * 24) + dt for i in self.time]
        for i, node in enumerate(self.nodes):
            self.raw.readVariablesAtNode(node)
            self.data = self.raw.getVarValuesAtNode()
            # self.variables["node_" + str(i)] = {"ELAPSED_TIME": self.elapsed_time}
            self.variables["node_" + str(i)] = ({name: self.data[:, values].tolist()
                                                 for values, name in enumerate(self.variable_names)})

    def write2json(self, filename):
        self.filename_json = filename
        # ---
        with open(self.filename_json, "w") as outfile:
            json.dump(self.variables, outfile)


class TideAnalysis(object):
    def __init__(self, filename, node):
        self.filename = filename
        self.node = node
        slf = Selafin(self.filename)
        slf.extract_at_node(self.node)
        self.water_level = slf.variables["FREE_SURFACE"]
        self.tidal_coefficient = list()
        self.date = slf.date
        self.time = slf.time
        self.high_water_location = list()
        self.high_water_time_t2d = list()
        self.high_water_level = list()
        self.low_water_location = list()
        self.low_water_time_t2d = list()
        self.low_water_level = list()
        self.tide_nb = int()
        self.tidal_range = list()
        self.water_level_reversed = list()

    def find_peaks(self):
        # 3600 sec / time step * 7 hours (estimated duration of a flood period)
        mpd_value = 3600 / (self.time[2] - self.time[1]) * 7
        self.high_water_location = detect_peaks(self.water_level, mph=1, mpd=mpd_value, show=False)
        self.high_water_time_t2d = [int(self.time[index]) for index in self.high_water_location]
        self.high_water_level = [self.water_level[index] for index in self.high_water_location]
        self.low_water_location = detect_peaks(self.water_level, mph=-3, mpd=mpd_value, valley=True, show=False)
        self.low_water_time_t2d = [int(self.time[index]) for index in self.low_water_location]
        self.low_water_level = [self.water_level[index] for index in self.low_water_location]
        # --- Filtering first values
        index_hws = [idx for idx, item in enumerate(self.high_water_time_t2d) if item < 13000]
        index_lws = [idx for idx, item in enumerate(self.low_water_time_t2d) if item < 13000]
        [self.high_water_time_t2d.__delitem__(index) for index in index_hws if index_hws]
        [self.high_water_level.__delitem__(index) for index in index_hws if index_hws]
        [self.low_water_time_t2d.__delitem__(index) for index in index_lws if index_lws]
        [self.low_water_level.__delitem__(index) for index in index_lws if index_lws]
        self.tide_nb = self.high_water_level.__len__()

    def tidal_range_computation(self):
        self.tidal_range = [self.high_water_level[i] - self.low_water_level[i] for i in range(self.tide_nb)]
        self.tidal_coefficient = [int(tidal_range / 6.10 * 100) for tidal_range in self.tidal_range]


class Density:
    def __init__(self, salinity, ssc):
        self.salinity = salinity
        self.suspended_sediment_concentration = ssc
        # --
        self.temperature = 15.0
        self.averaged_water_density = 1.05
        self.dry_sediment_density = 2.65
        # --
        self.deltar = list()
        self.values = list()
        self.values_extrapolated = list()

    def compute(self):
        rho0 = 1050.0
        rhoref = 999.972
        rhos = 2650.0
        rho = np.power(self.temperature - 4.0, 2)
        rho = rho * 7.E-6
        rho = -750.E-6 * self.salinity + rho
        rho = rho - 1.0
        rho = -rhoref * rho
        rho = rho * (1 - (self.suspended_sediment_concentration / rhos))
        rho = rho + self.suspended_sediment_concentration
        self.values = rho
        return self.values

    def extrapolate(self):
        self.values_extrapolated = (self.values - 1000) / .5 + 1000
        return self.values_extrapolated


################################################################################
