#!/home/$USER/anaconda3/envs/py27/bin python
# -*- coding: utf-8 -*-
# ******************************************************************************
# Project: opentelUtils
# Authors: Sylvain Orseau
# history: Sylvain Orseau (Cerema)
#           11/15/2019, #V0P1
# ******************************************************************************
"""
@author: Sylvain Orseau (Cerema) - sylvain.orseau@tutanota.com

"""
################################################################################
# Import public modules
import os

# Path file
path_file = os.getcwd()
path_data = os.path.join(path_file[:-8] + "/examples/Gironde/GPMB/data")


################################################################################
class HTMLTable(object):
    def __init__(self, html):
        self.html = html
        # --
        self.header = "<!DOCTYPE html>\n<html>\n<head>\n"
        self.css_link = "<link rel=""stylesheet"" type=""text/css"" href=mystyle.css"">\n"

    def generate(self):
        """ Generate an html file containing possible trajects """
        hs = open(path_data + "/Trajects.html", "w+")
        hs.write(self.header)
        hs.write(self.css_link)
        hs.write("</head>\n")
        hs.write(self.html)
        return hs
