---
title: NavTEL - Installation
subtitle: Installation
author: CEREMA - Sylvain Orseau
---

# Installation process
This article provides information for the manual installation of NavTEL and its pre-requisites. In its current version, NavTEL is only available for Ubuntu operating system. Installation can be performed for other Linux operating systems, but it implies to install Opentelemac manually without the bash scripts available in the folder navtel/dev.

## Prerequisites
Before installing the system, you will find below a list of softwares needed to run TELEMAC-MASCARET and NavTEL.

### Conda: to handle Python packages and your virtual environement
In the current version of NavTEL, we recommend to use conda as packaging tool and installer, but also for the creation of the virtual environment. In the near future, we plan to use pip for a simplified installation.
~~~~
apt install conda
~~~~

To perform simulation, NavTEL lies on the TELEMAC-MASCARET suite which used Python 2.7 scripts to run its modules. 
~~~~
conda create -n py27 python=2.7 anaconda
source activate py27
~~~~

### Jupyter: to read or to edit notebooks
To explain each operation realized by NavTEL, a notebook is available in navtel/notebooks. If you want to read this file, install Jupyter with the following code. 
~~~~
apt install jupyter-notebook
~~~~

### Python packages
We will find below the Python packages required to run NavTEL. They can be installed with conda.
- calendar
- csv
- datetime
- email
- imaplib
- matplotlib
- numpy
- os
- pandas
- re
- reportlab
- scipy
- settings
- smtplib
- sys
- urllib2
- xmltodict

## NavTEL installation
To download NavTEL, ensure that you have Git installed in you system with the following command:
~~~~
git --version
~~~~

If the command returns an error, install git with the following command for Ubuntu system:
~~~~
apt install git
~~~~

Finally, clone the Gitlab deposit.
~~~~
git clone https://gitlab.com/orseausy/navtel
~~~~


### Compile Opentelemac
Bash scripts allowing to automate the installation of Opentelemac are available in the folder navtel/dev. First, execute make_metis.sh in order to compile metis, a set of programs for partitioning. To complete the installation, execute make_telemac.sh which will compile Opentelemac suite (version v7p2r2) with parallel computing.  
~~~~
$ cd <ROOT OF NAVTEL>/resources/dev
$ bash make_metis.sh
    [...]
$ bash make_telemac.sh
    [...]
    My work is done
~~~~

### Automate simulations
Simulations can be easily automated by using cron. Cron is a time-based job scheduler for linux operating system.
~~~~
apt install cron
~~~~

Open a terminal and enter the command ''cron -e'' which allows to edit a crontab file. With your favorite text editor (for example with Vim), edit the file like this:
~~~~
#        m  h           dom mon dow   command
         0  6            *   *   *    bash ~/navtel/dev/model_launcher.sh
#        |  |            |   |   |
# 0 min -+  |            |   |   |
# at 6h ----+            |   |   |
# each day of the month -+   |   |
# each month ----------------+   |
# each day of the week ----------+
~~~~


## Usage

### Brief presentation of the NavTEL framework
The NavTEL decision support tool determines near real-time ship route and underkeel clearance by predicting water levels, current velocities and water density along an estuarine navigation channel. These predictions are obtained by the TELEMAC-MASCARET modelling system, of which the v7p2r2 version is included in the present deposit. Because of its modular structure, NaVTEL can be easily adjusted to a different port configuration. Its kernel is composed by 2 modules named NAVIRE and TELBOT. The TELBOT module prepares and launches simulations, while the NAVIRE module post-processes simulation results to determine the route and predict the squat. To ensure portability, NavTEL can be steered from the command line and does not include a graphical user interface.

### First step - Set a numerical model
As explained above, NavTEL requires simulation output to extract hydrological parameters such as water level and salinity at selected nodes. To set a numerical model of an estuarine channel, ensure to have following files:
- 2 steering files for TELEMAC-2D (t2d_navtel.cas) and SISYPHE (sis_navtel.cas) modules. These files containing numerical and physical information such as the time-step, the friction coefficient and the eddy viscosity parameterization, as well as the input/output filenames.
- 1 geometry file containing the mesh (geom.slf).
- 1 boundary conditions file containing (BoundaryConditions.cli).
- Optional: A file containing harmonic constants (HarmonicConstants.CL). At the maritime boundary, astronomical tides can be reconstructed from tidal atlases including harmonic constituents.
- Optional: Formatted data files providing measured river discharges and storm surge forecasts.

### Second step - Prepare required files for NavTEL
Before to run NavTEL, you need to provide information on the terminals from where ship coming or leaving, on the stations where variables will be extracted as well as passing hours at selected locations depending hydrological conditions. All of these information should be stored in the following files: 

- A file containing information on the selected locations (SelectedLocations.csv). These informations are (i) the location name, (ii) the type if sub-stations exist (Marker or Threshold), (iii) an ID number, (iv) the kilometric point, (v) geographic coordinates and (vi) the node number.
- A file providing the distance between selected locations (Distance.csv). The name of locations (location_1/location_2) and the type of the second location are also required.
- A file with information on terminals of the port (Terminals.csv). It contains the terminal name, an ID number as well as the node number.
- A file providing passing hours at selected locations (Timings.csv). In its current version, NavTEL compute passing hours for the Gironde Estuary and from a table depending the destination, the tidal range, the ship speed and the type of route (seaward or landward). The table gives time lags (in minutes) with the hour of high tide at the referent terminal. These time lags are then converted into the time at which variables will be extracted by finding the hour of high tide. Passing hours can also be determined by computing the mean longitudinal variation of the ship speed for a predefined range of river discharges. The most appropriate way to do this is to used Automatic Identification System dataset for a large panel of ships.

### Third step - Launch NavTEL and examine the reliability of predictions
For a manual execution of NavTEL, enter the following command:
~~~~
python <path>/navtel/sources/navtel.py
~~~~
